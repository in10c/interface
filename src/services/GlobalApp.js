let callback = null
let firstRunCompleted = null
export function saveLocalUser(usr){
    localStorage.setItem("us3r_arbitr4j5_cli3nt", JSON.stringify(usr))
}
export function getLocalUser(){
    let usr = localStorage.getItem("us3r_arbitr4j5_cli3nt")
    return JSON.parse(usr)
}
export function deleteUser(){
    localStorage.removeItem("us3r_arbitr4j5_cli3nt")
}
export function setCallback(cb){
    callback = cb
}
export function executeCallback(){
    callback()
}
export function setFirstRunDone(cb){
    firstRunCompleted = cb
}
export function executeFirstRunDone(){
    firstRunCompleted()
}