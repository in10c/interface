import * as axios from 'axios';
import Constants from './Constants';


const Requests = {
    post : async (url, payload) =>{
        const headers = localStorage.getItem("access-token") && localStorage.getItem("access-token") !== "null" ? {"access-token" : localStorage.getItem("access-token")} : {}
        const response = await axios.post(Constants.configServerURL()+"/"+url, payload, {headers})
        localStorage.setItem("access-token", response.headers["access-token"] || localStorage.getItem("access-token"))
        return response
    },
    postTradingServer : async (url, payload) =>{
        const headers = localStorage.getItem("access-token") && localStorage.getItem("access-token") !== "null" ? {"access-token" : localStorage.getItem("access-token")} : {}
        const response = await axios.post(Constants.tradingServer()+"/"+url, payload, {headers})
        localStorage.setItem("access-token", response.headers["access-token"] || localStorage.getItem("access-token"))
        return response
    },
    get: async (url) =>{
        const headers = localStorage.getItem("access-token") && localStorage.getItem("access-token") !== "null" ? {"access-token" : localStorage.getItem("access-token")} : {}
        const response = await axios.get(Constants.configServerURL()+"/"+url, {headers})
        localStorage.setItem("access-token", response.headers["access-token"] || localStorage.getItem("access-token"))
        return response
    },
    getTradingServer: async (url) =>{
        const headers = localStorage.getItem("access-token") && localStorage.getItem("access-token") !== "null" ? {"access-token" : localStorage.getItem("access-token")} : {}
        const response = await axios.get(Constants.tradingServer()+"/"+url, {headers})
        localStorage.setItem("access-token", response.headers["access-token"] || localStorage.getItem("access-token"))
        return response
    },
    put: async (url, payload) => {
        const headers = localStorage.getItem("access-token") && localStorage.getItem("access-token") !== "null" ? {"access-token" : localStorage.getItem("access-token")} : {}
        const response = await axios.put(Constants.configServerURL()+"/"+url, payload, {headers})
        localStorage.setItem("access-token", response.headers["access-token"] || localStorage.getItem("access-token"))
        return response
    },
    delete: async (url) => {
        const headers = localStorage.getItem("access-token") ? {"access-token" : localStorage.getItem("access-token")} : {}
        const response = await axios.delete(Constants.configServerURL()+"/"+url, {headers})
        localStorage.setItem("access-token", response.headers["access-token"] || localStorage.getItem("access-token"))
        return response
    },
    deleteWPayload: async (url, payload) => {
        const headers = localStorage.getItem("access-token") && localStorage.getItem("access-token") !== "null" ? {"access-token" : localStorage.getItem("access-token")} : {}
        const response = await axios.delete(Constants.configServerURL()+"/"+url, payload, {headers})
        localStorage.setItem("access-token", response.headers["access-token"] || localStorage.getItem("access-token"))
        return response
    }
}
export default Requests