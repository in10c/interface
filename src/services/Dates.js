
const meses = [
    'Enero',
   'Febrero',
    'Marzo',
   'Abril',
   'Mayo',
   'Junio',
    'Julio',
   'Agosto',
    'Septiembre',
    'Octubre',
   'Noviembre',
   'Diciembre',
]
const dias = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"]
export const dateWithFormat = (param) => {
   //let fecha = new Date(param)
   var fecha = new Date(0); // The 0 there is the key, which sets the date to the epoch
   fecha.setUTCSeconds(param);    
   //console.warn("fecha", fecha, fecha.getDay())
   return dias[fecha.getDay()]+" "+fecha.getDate()+" de "+meses[fecha.getMonth()]+" del "+fecha.getFullYear()
}
export const dateWithFormatFromTime = (param) => {
   let fecha = new Date(param)
   //console.warn("fecha", fecha, fecha.getDay())
   return dias[fecha.getDay()]+" "+fecha.getDate()+"/"+meses[fecha.getMonth()]+"/"+fecha.getFullYear()+", "+fecha.getHours()+":"+fecha.getMinutes()+" horas."
}
export const dateWithNumberFormatFromTime = (param) => {
   let fecha = new Date(param)
   //console.warn("fecha", fecha, fecha.getDay())
   return dias[fecha.getDay()]+", "+fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+fecha.getFullYear()
}