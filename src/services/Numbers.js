export const fixedToPattern = (number, pattern) => {
    let decimals= countDecimals(pattern)
    let multiplierString = "1"
    for (let index = 1; index <= decimals; index++) {
        multiplierString += "0"
    }
    let multiplier = parseInt(multiplierString)
    return parseInt('' + (number * multiplier)) / multiplier;
}

export const fixedToMultiplier = (number, multiplier) => {
    return parseInt('' + (number * multiplier)) / multiplier;
}

export const getMultiplier = (pattern) => {
    //console.log("llego el pattern: ", pattern)
    let decimals= countDecimals(pattern)
    let multiplierString = "1"
    for (let index = 1; index <= decimals; index++) {
        multiplierString += "0"
    }
    let multiplier = parseInt(multiplierString)
    return multiplier
}

export const countDecimals = (value) => {
    if(Math.floor(value) === value) return 0;
    return value.toString().split(".")[1].length || 0; 
}