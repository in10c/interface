const Constants = {
    production: false,
    activationsOpen: true,
    developerMode: false,
    tradingIsActive: true,
    termAndConditionsActive: true,
    queueAmount : 0.01,
    packgs : {
        1: {name:"Bronce", price: 250, half: 125, reservPrice: 62.5, halfReservPrice: 31.25, path: "/images/bronce.png"}, 
        2: {name: "Plata", price: 500, half: 250, reservPrice: 125, halfReservPrice: 62.5, path: "/images/plata.png"}, 
        3: {name:"Oro", price: 1250, half: 625, reservPrice: 312.5, halfReservPrice: 156.25, path: "/images/oro.png"}, 
        4: {name:"Diamante", price: 3000, half: 1500, reservPrice: 750, halfReservPrice: 375, path: "/images/diamante.png"}, 
        5: {name:"Premium", price: 6000, half: 3000, reservPrice: 1500, halfReservPrice: 750, path: "/images/premium.png"}, 
        6: {name:"Premium Plus", price: 12000, half:6000, reservPrice: 3000, halfReservPrice: 1500, path: "/images/premiumplus.png"}},
    configServerURL: ()=>{
        if(Constants.production){
            return Constants.developerMode ? 'https://bitwabi.com:3002' : 'https://sweb.bitwabi.com:3001'
        } else {
            return 'http://127.0.0.1:3001'
        }
    },
    tradingServer: () => Constants.production ? 'https://bitwabi.com:3005' : 'http://127.0.0.1:3005',
    appURL: ()=>{
        if(Constants.production){
            return 'https://app.bitwabi.com'
        } else {
            return 'http://localhost:3000'
        }
    },
    btcExplorer: ()=>{
        if(Constants.production){
            return 'https://blockstream.info/tx'
        } else {
            return 'https://blockstream.info/testnet/tx'
        }
    },
    pckgInfo : (packNumber) =>{
        return Constants.packgs[packNumber]
    }
};

export default Constants