import React from 'react';
import { Link } from 'react-router-dom';
import { useToasts } from "react-toast-notifications";
import Constants from "../../services/Constants"
import Requests from "../../services/Requests"
import Loader from "react-loader-spinner";
import OrdersActiveTable from './OrdersActiveTable'
import './Home.css';

export default function Dashboard() {
    const { addToast } = useToasts();
    const user = JSON.parse(localStorage.getItem("user_bitwabi"))
    const [ appStatus, setAppStatus ] = React.useState(undefined)
    const [ btcBalance, setBTCBalance ] = React.useState(0)
    const [ btcInOrder, setBTCInOrder ] = React.useState(0)

    const changeAppStatus = async (ev) => {
        let result = await Requests.post("changeAppStatus")
        console.log("regreso de change status", result)
        if(result.data.success == 1){
            if(!appStatus === true) getBTCBalance()
            setAppStatus(appStatus=> !appStatus)
            addToast("El estado de la aplicación ha cambiado correctamente", { appearance: "success", autoDismiss: true });
        } else {
            addToast(result.data.message, { appearance: "error", autoDismiss: true });
        }
    }
    const getBTCBalance = async () => {
        let result = await Requests.get("accountBalance")
        console.log("regreso de accountBalance", result)
        if(result.data.success == 1){
            setBTCBalance(parseFloat(result.data.balance))
        } else {
            addToast(result.data.message, { appearance: "error", autoDismiss: true });
        }
    }
    const getBotStatus = async () => {
        let response = await Requests.get("appStatus")
        if(response.data.success == 1) setAppStatus(response.data.status)
        if(response.data.status) getBTCBalance()
        console.log("respuesta de bot status", response)
    }
    const updateInOrder = (sum) => {
        setBTCInOrder(sum)
    }
    React.useEffect(()=>{
        getBotStatus()
    }, [])
    return(
        <div id="home">
            <h1>Panel de control Bitwabi</h1>
            {user && (user.reservation && !user.developer) && <div className="box">
                <div className="logotype">
                    <img src="/images/logotipo.png"/>
                </div>
                <div className="welcome">
                    <p>Te damos la <b>bienvenida</b></p>
                </div>
                {!Constants.activationsOpen && <div className="text">
                    <p>Gracias por reservar tu licencia, por ahora los menus han sido deshabilitados hasta el lanzamiento del producto el <b>20 de septiembre del 2020</b>.</p>
                </div>}
                {Constants.activationsOpen && <div className="text">
                    <p>
                        Gracias por reservar tu licencia, da clic en el botón de <strong>Activar cuenta</strong> para hacer el pago del 75% restante y
                        activar completamente tu cuenta.
                    </p>
                    <p className="buttnActivate">
                        <Link to="/activateAccount" className="buttn buttn-primary">Activar cuenta</Link>
                    </p>
                </div>}  
            </div>}
            
            {user && !user.reservation && user.fullAccount && !Constants.tradingIsActive && <div className="box">
                <div className="logotype">
                    <img src="/images/logotipo.png"/>
                </div>
                <div className="text">
                    <p>¡Gracias por activar su licencia!. <br/><br/> El <b>panel de control</b> estará disponible a partir de que <b>inicie la aplicación de copy-trade</b>.</p>
                </div>      
            </div>}
            {user && ( (!user.reservation && user.fullAccount && Constants.tradingIsActive) || user.developer ) && <div>

                <div className="box" style={{paddingTop:70, paddingBottom: 70}}>
                    <div className="logotype" style={{marginBottom: 30}}>
                        <img src="/images/logotipo.png"/>
                    </div>
                    {appStatus === undefined ? <div style={{marginBottom: 30}}><Loader type="Oval" color="#00BFFF" height={40} width={40} visible={true} /> </div>: 
                    <div style={{marginBottom: 0}}>
                        {!appStatus ? <img style={{width: 300, cursor: "pointer"}} src="/images/activarBot.png" onClick={changeAppStatus} /> :
                        <img style={{width: 300, cursor: "pointer"}} src="/images/desactivarBot.png" onClick={changeAppStatus} />}<br/>
                        {appStatus && <React.Fragment><span className="balanceDash">Balance: {btcBalance.toFixed(8)} BTC | En orden: {btcInOrder.toFixed(8)} BTC</span></React.Fragment>}
                    </div>}
                    <OrdersActiveTable updateInOrder={updateInOrder.bind(this)}/>
                </div>
               
            </div>}
        </div>
    )

}