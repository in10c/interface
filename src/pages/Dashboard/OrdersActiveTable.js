import React, { useEffect, useState } from 'react'
import { Redirect } from 'react-dom'
import Requests from "../../services/Requests"
import {dateWithNumberFormatFromTime} from "../../services/Dates"
import "./OrdersActiveTable.css"

export default function OrdersActiveTable({updateInOrder}) {

    const [orders, setOrders] = useState([])
    const [redirect, setRedirect] = useState(false)

    useEffect(() => {
        async function getInfo() {
            const response = await Requests.get("myOrdersActive")
            if (!!response.data.failAuth) {
                localStorage.removeItem("user_bitwabi")
                localStorage.removeItem("access-token")
                setRedirect(true)
            } else {
                let sum = 0
                for (let index = 0; index < response.data.orders.length; index++) {
                    let order = response.data.orders[index];
                    if(order.pretendedToBuy){
                        sum += order.pretendedToBuy
                    }
                }
                updateInOrder(sum)
                setOrders(response.data.orders)
            }
        }
        getInfo()
    }, [])

    return (
        <div className="ordersActiveTable">
            {redirect && <Redirect to="/signIn" />}
            <h4>ORDENES ABIERTAS:</h4>
            <table>
                <thead>
                    <tr>
                        <th>Simbolo</th>
                        <th>Balance asignado</th>
                        <th>Fecha</th>
                    </tr>
                </thead>
                <tbody>
                    {orders.map((order, i) => {
                        return (
                            <tr key={i}>
                                <td>{order.symbol}</td>
                                <td>{order.pretendedToBuy && !isNaN(order.pretendedToBuy) ? order.pretendedToBuy.toFixed(8) : order.pretendedToBuy} BTC</td>
                                <td>{dateWithNumberFormatFromTime(order.transactTime)}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
