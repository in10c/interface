import React, { useContext } from "react";
import DeviceDetector from "device-detector-js";
import { useToasts } from "react-toast-notifications";
import axios from "axios";
import Requests from "../../services/Requests";
import Constants from "../../services/Constants";
import { Redirect } from "react-router-dom";
import { LayoutContext } from "../../layout/LayoutContext/LayoutContext";

const deviceDetector = new DeviceDetector();
const device = deviceDetector.parse(window.navigator.userAgent);

export default function Activate() {
    const { setModalVisible } = useContext(LayoutContext);
    const user = JSON.parse(localStorage.getItem("user_bitwabi"))
    React.useEffect(() => {
        if(user && user.termsAndConditionsAgreement)
            setModalVisible("activate");
    }, []);
    return (
        <div>
            <h1>Activar cuenta</h1>
            <div className="box">
            </div>
        </div>
    );
}
