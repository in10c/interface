import React from 'react';
import Requests from "../../services/Requests"
import { useToasts } from 'react-toast-notifications'
import FormPassword from "../../formsComponents/password/formPassword"
import "./Apis.css"

export default function Apis() {
    const { addToast } = useToasts()
    const user = JSON.parse(localStorage.getItem("user_bitwabi"))
    //console.log("usr", user)
    const [inputs, setInputs] = React.useState({});
    const handleInputChange = (event) => {
        event.persist();
        setInputs(inputs => ({...inputs, [event.target.name]: event.target.value}));
    }
    const saveApis = () =>{
        let aux = JSON.parse(JSON.stringify(inputs))
        aux.userID = user.id
        Requests.post("setAPIS", aux).then((result)=>{
            console.log("regreso de enviar", result)
            if(result.data.success === 1){
                addToast("Los datos han sido guardados correctamente.", { appearance: 'success', autoDismiss: true })
            }else{
                addToast("Tus API's son invalidas.", { appearance: 'error', autoDismiss: true })
            }
            
        })
    } 
    React.useEffect(()=>{
        Requests.post("getAPIS", {userID: user.id}).then((result)=>{
            console.log("regreso de enviar", result)
            if(result.data.success == 1){                
                setInputs({apiKey: result.data.apis.apiKey, apiSecret: result.data.apis.apiSecret})
            }
        })
    }, [])
    return(
        <div id="apis">
            <h1>Enviar Trade</h1>
            <div className="box">
                <div className="form">
                    <h2>API´s Binance Spot</h2>
                    <div className="formControl">
                        <input type="text" placeholder="API KEY" className="formControl" required name="apiKey"
                        value={inputs.apiKey} onChange={handleInputChange}/>
                    </div>
                    <div className="formControl">
                        <FormPassword placeholder="API SECRET" className="formControl" name="apiSecret"
                        value={inputs.apiSecret} onChange={handleInputChange} />
                    </div>
                    <button className="buttn buttn-primary" onClick={saveApis} >Enviar </button>
                </div>
            </div>
        </div>
    )

}