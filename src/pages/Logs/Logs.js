import React from 'react';
import { useToasts } from 'react-toast-notifications'
import Requests from "../../services/Requests"
import {fixedToMultiplier} from "../../services/Numbers"
import {dateWithNumberFormatFromTime, dateWithFormatFromTime} from "../../services/Dates"
import "./Logs.css"

export default function Logs() {
    const [logs, setLogs ] = React.useState([])
    const { addToast } = useToasts()

    React.useEffect(()=>{
        Requests.get("getAppLogs").then((result)=>{
            console.log("regreso de mis ordnes", result)
            if(result.data.success == 1){
                setLogs(result.data.logs)
            } else {
                addToast(result.data.message, { appearance: 'error', autoDismiss: true })
            }
        })
    }, [])
    return(
        <div id="ordersHistory">
            <h1>Historial de Registros</h1>
            <div className="box">
                <div className="logotype">
                    <img src="/images/logotipo.png"/>
                </div>
                <div className="text">
                    <p>Este es tu historial de acciones dentro de Bitwabi:</p>
                </div> 
                <div className="ordersTable">
                    <table>
                        <thead>
                            <tr>
                                <th>Descripción</th>
                                <th>Balance</th>
                                <th>Fecha</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                logs.slice(0).reverse().map(lg=><tr>
                                    <td>{lg.description}</td>
                                    <td>{lg.balance ? lg.balance.toFixed(8)+" "+(!lg.description.includes("BNB") ? "BTC" : "BNB") : "Sin datos"}</td>
                                    <td>{dateWithFormatFromTime(lg.time)}</td>
                                </tr>)
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )

}