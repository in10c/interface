import React from 'react';
import { useToasts } from 'react-toast-notifications'
import Requests from "../../services/Requests"
import {dateWithNumberFormatFromTime} from "../../services/Dates"
import Constants from "../../services/Constants"
import numeral from "numeral" 
import "./MyPayments.css"

export default function MyPayments() {
    const { addToast } = useToasts()
    const [payments, setPayments] = React.useState([])
    const [stats, setStats] = React.useState({totalEuros: 0, totalBTC: 0})
    React.useEffect(()=>{
        Requests.get("payments").then((result)=>{
            console.log("regreso de mis pagos", result)
            if(result.data.success == 1 && Array.isArray(result.data.payments) && result.data.payments.length > 0){                
                setPayments(result.data.payments)
                let totalEuros = 0
                let totalBTC = 0
                result.data.payments.map(val=>{
                    totalEuros += val.quantity
                    totalBTC += val.quantityBTC
                })
                setStats({totalEuros, totalBTC})
            } else {
                addToast(result.data.message, { appearance: 'error', autoDismiss: true })
            }
        })
    }, [])
    return(
        <div id="myPayments">
            <h1>Mis pagos</h1>
            <div className="box">
                <div className="logotype">
                    <img src="/images/logotipo.png"/>
                </div>
                <div className="text">
                    <p>Estos han sido tus pagos a Bitwabi:</p>
                </div>
                <div className="myPaymentsTable">
                    <table>
                        <thead>
                            <tr>
                                <th>Descripción</th>
                                <th>Cantidad en Euros</th>
                                <th>Cantidad en BTC</th>
                                <th>Paquete seleccionado</th>
                                <th>Precio BTC en euros</th>
                                <th>Fecha de registro</th>
                            </tr>
                        </thead>
                        <tbody>
                            {payments.map(pay=><tr>
                                <td>{pay.description}</td>
                                <td>€{numeral(pay.quantity).format("0,0")}</td>
                                <td>{numeral(pay.quantityBTC).format("0.00000000")} BTC</td>
                                <td>
                                    <img src={Constants.pckgInfo(pay.selectedPackage).path} />
                                    <span className="packageName"> {Constants.pckgInfo(pay.selectedPackage).name.toUpperCase()}</span>
                                </td>
                                <td>€{numeral(pay.priceBTCinEuros).format("0,0")}</td>
                                <td>{dateWithNumberFormatFromTime(pay.createdAt)}</td>
                            </tr>)}
                        </tbody>
                    </table>
                </div>
                <p style={{textAlign: "right"}}>
                    Total en Euros: <strong>€{numeral(stats.totalEuros).format("0,0")}</strong><br/>
                    Total en BTC: <strong>{numeral(stats.totalBTC).format("0.00000000")} BTC</strong>
                </p>
            </div>
        </div>
    )

}