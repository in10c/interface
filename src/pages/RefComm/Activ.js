import React from 'react';
import { useToasts } from 'react-toast-notifications'
import Constants from '../../services/Constants';
import Requests from "../../services/Requests"

export default function RefComm({activ}) {
    const { addToast } = useToasts()
    const [log, setLog] = React.useState([])
    const [activation, setActivation] = React.useState({ pendingDispersion : [] })

    const verify = (idActiv) => {
        Requests.post("verifyDispersion", {idActiv}).then((result)=>{
            console.log("regreso de actv", result)
            if(result.data.success==1){
                setLog(result.data.log)
            }  else {
                addToast(result.data.message, { appearance: 'error', autoDismiss: true })
            }
        })
    }
    const sendDispersion = (idActiv) => {
        Requests.post("sendDispersion", {idActiv}).then((result)=>{
            console.log("regreso de send ", result)
            if(result.data.success==1){
                addToast("Se ha dispersado correctamente.", { appearance: 'success', autoDismiss: true })
                //setLog(result.data.log)
            }  else {
                addToast(result.data.message, { appearance: 'error', autoDismiss: true })
            }
        })
    }
    const regenerate = (idActiv) => {
        Requests.post("regenerateDispersionArray", { activationID: idActiv }).then((result)=>{
            console.log("regreso de send ", result)
            if(result.data.success==1){
                addToast("Se ha regenerado correctamente.", { appearance: 'success', autoDismiss: true })
            }  else {
                addToast(result.data.message, { appearance: 'error', autoDismiss: true })
            }
        })
    }
    const forceDistribution = (idActiv) => {
        Requests.post("forceDispersate", { idActiv }).then((result)=>{
            console.log("regreso de send ", result)
            if(result.data.success==1){
                addToast("Se ha distribuido correctamente.", { appearance: 'success', autoDismiss: true })
            }  else {
                addToast(result.data.message, { appearance: 'error', autoDismiss: true })
            }
        })
    }
    React.useEffect(()=>{
        setActivation(JSON.parse(JSON.stringify(activ)))
    }, [])
    return( <tr>
        <td>
            Usuario: {activation.userID}<br/>
            Pago BTC: {activation.quantity}<br/>
            Pago Euro: {activation.remainPayment}<br/>
            Paquete: {activation.selectedPackage}<br/>
            Descuento: {activation.halfDiscount? "Si": "No"}
        </td>
        <td>{activation.remainInWallet}</td>
        <td>
            {
                activation.pendingDispersion.map(disp=>{
                let btc = parseFloat(disp.quantity / 100000000)
                return <p>
                    Dispersa: {disp.address}<br/>
                    Satoshis: {disp.quantity}<br/>
                    BTC: {btc}<br/>
                    Aprx Euro: €{ parseInt(btc * activation.priceBTCinEuros) }
                </p>})
            }
        </td>
        <td>
            {activation.status}<br/>
            {activation.status === "Dispersada correctamente" && <span><a target="_blank" href={Constants.btcExplorer()+"/"+activation.commissionTxID}>TXID</a></span>}
        </td>
        <td><pre>{log}</pre></td>
        <td>
            {activation.status != "Dispersada correctamente" && <button onClick={ev=>verify(activation._id)}>Verificar</button>}
            {activation.status != "Dispersada correctamente" && <button onClick={ev=>sendDispersion(activation._id)}>Enviar</button>}
            <button onClick={ev=>regenerate(activation._id)}>Regenerar dispersion</button>
            <button onClick={ev=>forceDistribution(activation._id)}>Forzar distribución</button>
        </td>
    </tr> )

}