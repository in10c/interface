import React, {useState} from "react";
import { useToasts } from 'react-toast-notifications'
import {Redirect, useParams} from "react-router-dom"
import Loader from 'react-loader-spinner'
import * as md5 from "md5";
import axios from "axios"
import DeviceDetector from "device-detector-js";
import Requests from "../../services/Requests"
import "./Reservation.css"
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"

const deviceDetector = new DeviceDetector();
const device = deviceDetector.parse(window.navigator.userAgent);

export default function Reservation() {
    const { refererID } = useParams()
    const [inputs, setInputs] = useState({});
    const [typeInput, setType] = useState("password");
    const [sended, setSended] = useState(false);
    const [open, setOpen] = useState(true);
    const [loading, setLoading] = useState(true);
    const [infoClient, setInfo] = useState({});
    const [pckg, setPackg] = useState(false);
    
    const { addToast } = useToasts()
    const handleInputChange = (event) => {
        event.persist();
        setInputs(inputs => ({...inputs, [event.target.name]: event.target.value}));
    }
    const getclientdata = ()=>{
        axios.get('http://api.ipstack.com/check?access_key=512192071c40a3165c5d296a6edf6ea3').then(data=>{
            console.log("data", data, "el device", device)
            setInfo({
                ip: data.data.ip,
                country_name: data.data.country_name,
                browser: device.client.name,
                device: device.device.type,
                os: device.os.name
            })
        }, err=>{
            console.log("hubo error")
        })
    }
    const getStatus = () =>{
        Requests.get("statusInscriptions").then((result)=>{
            console.log("status ", result)
            if(result.data.success===1){
                setOpen(result.data.value)
                setLoading(false)
            } 
        })
    }
    const handleSubmit = (event) => {
        event.preventDefault()
        if(!inputs.email || !inputs.password || !inputs.name || !inputs.phone){
            addToast("Todos los campos son requeridos.", { appearance: 'error', autoDismiss: true })
            return false
        }
        if(!pckg){
            addToast("Debes elegir un paquete para continuar.", { appearance: 'error', autoDismiss: true })
            return false
        }
        let aux = JSON.parse(JSON.stringify(inputs))
        aux.password = md5(aux.password)
        aux.selectedPackage = pckg
        aux.refererID = refererID
        aux.infoClient = infoClient
        Requests.post("doSignUp", aux).then((result)=>{
            console.log("regreso de enviar", result)
            if(result.data.success == 1){
                setSended(result.data.token)
            } else {
                addToast(result.data.message, { appearance: 'error', autoDismiss: true })
            }
        })
        
    }
    React.useEffect(()=>{
        getclientdata()
        getStatus()
    }, [])
    return <div id="reservation">
        {sended && <Redirect to={"/token/"+sended} />}
        <img src="/images/wallreservation.png" />
        {open && !loading && <form className="layer" onSubmit={handleSubmit} autocomplete="off">
            <div className="form">
                <img src="/images/logotipo.png" width="200" />
                <p className="desc">Completa el registro para adquirir tu licencia.</p>
                <div className="structure">
                    <div className="formControl">
                        <input type="text" placeholder="Nombre" className="formControl" required name="name"
                            value={inputs.name} onChange={handleInputChange}/>
                    </div>
                    <div className="formControl">
                        <input type="email" placeholder="Correo electrónico" className="formControl" required name="email"
                            value={inputs.email} onChange={handleInputChange}/>
                    </div>
                    <div className="formControl">
                        <input type="phone" placeholder="Teléfono (código de país y número, solo números)" className="formControl" required name="phone"
                            value={inputs.phone} onChange={handleInputChange}/>
                    </div>
                    <div className="passInpt link formControl">
                        <input type={typeInput} placeholder="Contraseña" className="formControl" required name="password"
                            value={inputs.password} onChange={handleInputChange}/>
                        <div onClick={ev=>setType( typeInput === "password" ? "text" : "password")}><i class="fa fa-eye" aria-hidden="true"></i></div>
                    </div>
                </div>
                
                <h2>LICENCIAS DISPONIBLES</h2>
            </div>
            <div className="packages">
                <div>
                    <img src="/images/bronce.png" />
                    <div className="title">
                        <h3>BRONCE</h3>Límite 0.042 BTC
                    </div>
                    <div className="price">
                        €250
                    </div>
                    <div className="check" onClick={ev=>setPackg(1)}>
                        <div className="chkbox link" />
                        {pckg === 1 && <i class="fa fa-check" aria-hidden="true"></i>}
                    </div>
                </div>
                <div>
                    <img src="/images/plata.png" />
                    <div className="title">
                        <h3>PLATA</h3>Límite 0.084 BTC
                    </div>
                    <div className="price">
                        €500
                    </div>
                    <div className="check" onClick={ev=>setPackg(2)}>
                        <div className="chkbox link" />
                        {pckg === 2 && <i class="fa fa-check" aria-hidden="true"></i>}
                    </div>
                </div>
                <div>
                    <img src="/images/oro.png" />
                    <div className="title">
                        <h3>ORO</h3>Límite 0.212 BTC
                    </div>
                    <div className="price">
                        €1250
                    </div>
                    <div className="check" onClick={ev=>setPackg(3)}>
                        <div className="chkbox link" />
                        {pckg === 3 && <i class="fa fa-check" aria-hidden="true"></i>}
                    </div>
                </div>
                <div>
                    <img src="/images/diamante.png" />
                    <div className="title">
                        <h3>DIAMANTE</h3>Límite 0.509 BTC
                    </div>
                    <div className="price">
                        €3000
                    </div>
                    <div className="check" onClick={ev=>setPackg(4)}>
                        <div className="chkbox link" />
                        {pckg === 4 && <i class="fa fa-check" aria-hidden="true"></i>}
                    </div>
                </div>
                <div>
                    <img src="/images/premium.png" />
                    <div className="title">
                        <h3>PREMIUM</h3>Límite 1.018 BTC
                    </div>
                    <div className="price">
                        €6000
                    </div>
                    <div className="check" onClick={ev=>setPackg(5)}>
                        <div className="chkbox link" />
                        {pckg === 5 && <i class="fa fa-check" aria-hidden="true"></i>}
                    </div>
                </div>
                <div>
                    <img src="/images/premiumplus.png" />
                    <div className="title">
                        <h3>PREMIUM PLUS</h3>Límite 2.036 BTC
                    </div>
                    <div className="price">
                        €12000
                    </div>
                    <div className="check" onClick={ev=>setPackg(6)}>
                        <div className="chkbox link" />
                        {pckg === 6 && <i class="fa fa-check" aria-hidden="true"></i>}
                    </div>
                </div>
            </div>
            <div className="footer">
                <button href="" className="buttn buttn-primary">RESERVAR LICENCIA</button>
                <p>EL PRECIO POR CONCEPTO DE RESERVA ES DEL <strong>25%</strong></p>
            </div>
        </form>}
        {!open && !loading &&  <form className="layer" onSubmit={handleSubmit}>
        <p className="textLoading">Los registros están cerrados por el momento.</p>
        </form>}
        { loading && <form className="layer" onSubmit={handleSubmit}>
            <Loader
                type="BallTriangle"
                color="#1ABFD3"
                height={100}
                width={100}
            />
            <p className="textLoading">Procesando, por favor espere...</p>
        </form>}
    </div>
}