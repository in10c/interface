import React from "react"
import Requests from "../../services/Requests"
import "./Buy.css"

export default function Buy({match}){
    const [generated, setGenerated] = React.useState(false)
    const buyPackage = (pckgNumber) =>{
        Requests.post("generatePaymentGate", {pckgNumber, tkn: match.params.tkn}).then((result)=>{
            console.log("regreso de enviar", result)
            if(result.data.success === 1){
                setGenerated(result.data)
            }
        })
    }
    console.log(match.params.tkn)
    return generated ? <div>
        La dirección de pago ha sido generada, por favor envíe la cantidad de: {generated.quantity} BTC a la siguiente 
        dirección para hacer el pago de su licencia: <br/>
        Wallet BTC: {generated.address}
        </div> : <div>
        Elige tu paquete:
        <div className="packages">
            <div>
                Bronce <br/>
                €250<br/>
                <button className="buttn buttn-primary" onClick={ev=>buyPackage(1)}>Elegir</button>
            </div>
            <div>
                Plata <br/>
                €500<br/>
                <button className="buttn buttn-primary" onClick={ev=>buyPackage(2)}>Elegir</button>
            </div>
            <div>
                Oro <br/>
                €1000<br/>
                <button className="buttn buttn-primary" onClick={ev=>buyPackage(3)}>Elegir</button>
            </div>
            <div>
                Diamante <br/>
                €2000<br/>
                <button className="buttn buttn-primary" onClick={ev=>buyPackage(4)}>Elegir</button>
            </div>
            <div>
                Premium <br/>
                €3000<br/>
                <button className="buttn buttn-primary" onClick={ev=>buyPackage(5)}>Elegir</button>
            </div>
            <div>
                Premium PLUS <br/>
                €12000<br/>
                <button className="buttn buttn-primary" onClick={ev=>buyPackage(6)}>Elegir</button>
            </div>
        </div>
    </div>
}