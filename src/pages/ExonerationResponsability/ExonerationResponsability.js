import React from "react";
import LayoutVisitors from "../../layout/LayoutVisitors"
import "../LegalWarning/TermsStyles.css"

export default function ExonerationResponsability()  {

    return (
        <LayoutVisitors className="terms">
            <h1>EXONERACIÓN DE RESPONSABILIDAD</h1>
            <pre>
                {`
Esta presentación es solo a efectos informativos y puede estar sujeta a cambios. No podemos garantizar la exactitud de las declaraciones hechas o las conclusiones 
alcanzadas en este resumen y renunciamos expresamente a cualquier tipo de responsabilidad en relación a lo siguiente:


●	Cualquier responsabilidad relacionada con la comercialización o idoneidad del producto.
●	Que el contenido de este documento sea exacto y libre de cualquier error.
●	Que dichos contenidos no infrinjan ningún derecho de terceros. No seremos responsables por daños de ningún tipo que surjan del uso o referencia al contenido 
de esta presentación. Esta presentación puede contener referencias a datos de terceros y publicaciones de la industria. Por lo que sabemos, la información 
reproducida en esta presentación es precisa y que las estimaciones y suposiciones contenidas en este documento son razonables. Sin embargo, no ofrecemos garantías 
en cuanto a la exactitud o integridad de estos datos. Si bien se cree que la información y los datos reproducidos en esta presentación se obtuvieron de fuentes fiables, 
no hemos verificado de manera independiente ninguna información o datos de fuentes de terceros a los que se hace referencia en esta presentación, ni hemos comprobado 
los supuestos subyacentes en los que se basan.

No se harán ni se hacen promesas en relación a la apreciación o el valor futuro de los servicios y productos de BITWABI OÜ y ninguna garantía de que el servicio de 
BITWABI OÜ tenga un valor en particular. De igual forma no se hacen promesas respecto a los beneficios esperados respecto a la plataforma que ofrece BITWABI OÜ.

Cualquier cifra que se recoge en la presentación son meras estimaciones. A menos que los posibles participantes comprendan y acepten completamente la naturaleza 
del negocio de BITWABI OÜ y los riesgos potenciales asociados con la actividad de trading, no deben adquirir los servicios ofrecidos en BITWABI OÜ. 
La rentabilidad por inversión en BITWABI OÜ no se estructura o venden como valores o inversiones. La inversión en BITWABI OÜ no otorga derechos ni confiere intereses 
en el patrimonio de la empresa. Esta presentación no constituye un prospecto y no es una oferta de venta, ni la solicitud de ninguna oferta para adquirir un 
título valor, security, acción o instrumento financiero en cualquier jurisdicción. La inversión en BITWABI OÜ no debe ejecutarse con fines especulativos o de inversión 
con la expectativa de obtener un retorno de la inversión. 

Ninguna autoridad reguladora ha examinado o aprobado ninguna de la información establecida en esta presentación. Ninguna acción de este tipo ha sido o será tomada 
bajo las leyes, requisitos reglamentarios o reglas de ninguna jurisdicción. La publicación, distribución o difusión de esta presentación no implica que se hayan 
cumplido las leyes o los requisitos reglamentarios aplicables. La participación en la Plataforma BITWABI OÜ conlleva un riesgo sustancial y puede implicar riesgos 
especiales que podrían llevar a una pérdida total o una parte sustancial de su contribución.

Asegúrese de haber leído, comprendido y ser responsable como para aceptar los riesgos de participar en la Plataforma BITWABI OÜ antes de enviarnos una contribución. 
La plataforma BITWABI OÜ podría verse afectada por una acción reguladora. Los reguladores u otras autoridades competentes pueden exigir que revisemos los mecanismos 
de la Plataforma BITWABI OÜ para cumplir con los requisitos reglamentarios u otras obligaciones gubernamentales o comerciales. 

Sin embargo, creemos que estamos tomando medidas comercialmente razonables para garantizar que la mecánica de BITWABI OÜ no infrinja las leyes y regulaciones aplicables.

 

PRECAUCIÓN CON RESPECTO A LAS DECLARACIONES A FUTURO

Esta presentación y su web/plataforma asociada, https://www.bitwabi.com/ contienen declaraciones o información a futuro (en conjunto, "declaraciones a futuro") que 
se relacionan con nuestras expectativas actuales de eventos futuros. En algunos casos, estas declaraciones prospectivas pueden identificarse con palabras o frases como 
"publicar", "usuarios", "asumir", "puede", "deseará", "esperar", "anticipar", "apuntar", "Estimar", "intentar", "planear", "buscar", "creer", "potencial", "continuar", 
"es / es probable que" o el negativo de estos términos, u otras expresiones similares destinadas a identificar escenarios futuros. 
Hemos basado estas declaraciones a futuro en proyecciones actuales sobre eventos futuros y tendencias financieras y tecnológicas que creemos pueden afectar nuestra 
condición financiera, resultados de operaciones, estrategia comercial, necesidades financieras o los resultados de la venta de tokens. Además de las declaraciones 
relacionadas con los asuntos que se detallan aquí, esta presentación contiene declaraciones a futuro relacionadas con el modelo operativo propuesto BITWABI OÜ. 
El modelo habla solo de nuestros objetivos y no es un pronóstico, proyección o predicción de resultados futuros de operaciones. Las declaraciones a futuro están basadas en 
ciertas suposiciones y análisis realizados por BITWABI OÜ a la luz de su experiencia, condiciones actuales y desarrollos futuros esperados y otros factores que considere 
apropiados y están sujetos a riesgos e incertidumbres. Si bien las declaraciones a futuro contenidas en esta presentación se basan en lo que creemos son suposiciones razonables, 
existen riesgos, incertidumbres, suposiciones y otros factores que podrían causar que los resultados reales, resultados, logros y / o experiencias de BITWABI OÜ difieran 
materialmente de las expectativas expresadas, implícitas o percibidas en declaraciones a futuro. Dados estos riesgos, los posibles participantes en BITWABI OÜ no deben depositar 
una confianza indebida en estas declaraciones a futuro.

                `}
            </pre>
        </LayoutVisitors>
    )
}
