import React from "react";
import { useToasts } from 'react-toast-notifications'
import Loader from 'react-loader-spinner'
import Requests from "../../services/Requests"
import { useParams, Link  } from 'react-router-dom'
import Constants from "../../services/Constants"
import "./Token.css"
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"

export default function Reservation() {
    let { tkn } = useParams()
    const { addToast } = useToasts()
    const [token, setToken] = React.useState(false)
    const [smsCode, setCode] = React.useState("")

    const updateToken = () => {
        return new Promise((resolve, reject)=>{
            Requests.post("getToken", {token: tkn}).then((result)=>{
                console.log("regreso actualizar de nuevo token >>>>> ", result)
                let tkn = result.data.token
                setToken(tkn)
                // interval if is searching for a payment
                if(tkn.active && tkn.stepPhonePassed ){
                    setTimeout(() => {
                        updateToken()
                    }, 5000);
                }
                resolve(result.data.token)
            })
        })
        
    }
    const getElapsedTime = (time) =>{
        let now = new Date().getTime()
        let created = new Date(time).getTime()
        let elapsed = now - created
        var minutes = Math.floor(elapsed / 60000);
        var seconds = ((elapsed % 60000) / 1000).toFixed(0);
        console.log("elapsed", elapsed)
        return minutes+":"+seconds
    }
    const generateAdd = (ev)=>{
        ev.preventDefault()
        if(!smsCode){
            addToast("El código de confirmación es requerido.", { appearance: 'error', autoDismiss: true })
        }
        Requests.post("confirmSMSCode", {token: tkn, smsCode}).then((result)=>{
            console.log("regreso de enviar", result)
            if(result.data.success === 1){
                updateToken()
            } else {
                addToast(result.data.message, { appearance: 'error', autoDismiss: true })
            }
            
        })
    }
    React.useEffect(()=>{
        updateToken()
        
    }, [])  
    return <div id="token">
        <img src="/images/wallreservation.png" />
        <div className="layer">
            {token && token.active && token.stepPhonePassed && <div className="form">
                <img src="/images/logotipo.png" width="200" />
                {/*<img width="100" height="100" alt="qrcode" src={"https://chart.googleapis.com/chart?cht=qr&chl=bitcoin:"+token.address+"?amount="+token.quantity+"&choe=UTF-8&chs=100x100"} />*/}
                <p>
                    Envia la cantidad de: 
                </p>
                <div className="bubble" style={{backgroundColor: "rgb(99, 239, 169, .3)"}}>
                    {token.quantity}
                </div>
                <p>
                    a la billetera:
                </p>
                <div className="bubble" style={{backgroundColor: "rgb(41, 199, 208, .3)"}}>
                    {token.address} 
                </div>
                <p>
                    Tiempo transcurrido:
                </p>
                <div className="bubble" style={{borderColor: "gray", borderWidth: 1, borderStyle: "solid"}}>
                    {getElapsedTime(token.generatedTime)}
                </div>

                <p className="waiting">Esperando pago...</p>
                
            </div>}
            {token && token.active && !token.stepPhonePassed && <div className="form closed">
                <h2 style={{textAlign: "center"}}>Aplicación del código de descuento</h2>
                {!token.halfDiscount && <p className="desc" style={{marginBottom: 10}}>Lamentablemente <span style={{color: "red"}}>su número telefónico +{token.mobile} no cuenta con un código de descuento</span>. 
                Usted obtendrá la licencia {Constants.pckgInfo(token.selectedPackage).name} por un total de <strong style={{color: "black", fontSize: 20}}>€{Constants.pckgInfo(token.selectedPackage).price}</strong> y pagará 
                <strong style={{color: "black", fontSize: 20}}> €{Constants.pckgInfo(token.selectedPackage).reservPrice}</strong> por concepto de reserva. <br/><br/>Ingrese el código de confirmación que le hemos enviado mediante SMS a su número telefónico para continuar. </p>}
                {token.halfDiscount && <p className="desc" style={{marginBottom: 10}}>¡Felicidades!,  <span style={{color: "green"}}>su número telefónico +{token.mobile} sí cuenta con un código de descuento</span>. 
                Usted obtendrá la licencia {Constants.pckgInfo(token.selectedPackage).name} por un total de <strong style={{color: "black", fontSize: 20}}>€{Constants.pckgInfo(token.selectedPackage).half}</strong> y pagará 
                <strong style={{color: "black", fontSize: 20}}> €{Constants.pckgInfo(token.selectedPackage).halfReservPrice}</strong> por concepto de reserva. <br/><br/>Ingrese el código de confirmación que le hemos enviado mediante SMS a su número telefónico para continuar. </p>}
                <div className="formControl">
                    <input type="text" placeholder="Código de confirmación SMS" className="formControl" required name="name"
                       value={smsCode} onChange={ev=>setCode(ev.target.value)} />
                </div>
                <button className="buttn buttn-primary" onClick={generateAdd}>Revisar código y continuar</button>
                <img src="/images/logotipo.png" width="200" />
            </div>}
            {token && !token.active && !token.payed && <div className="form closed">
                <h2>Token caducado</h2>
                <p className="desc">El token de pago ha caducado debido a que no se ha recibido su pago dentro del rango de 1 hora permitido, por favor vuelva a realizar el proceso de compra.</p>
                <img src="/images/logotipo.png" width="200" />
            </div>}
            {token && !token.active && token.payed && <div className="form closed">
                <h2>Reservación completa</h2>
                <p className="desc" style={{marginBottom:0}}>Muchas gracias, su pago ha sido procesado y su reserva de licencia ha sido registrada correctamente.<br/><br/> Ahora su cuenta esta activa  
                y ya puede acceder al panel de control.</p>
                <Link to="/" className="buttn buttn-primary">Ir al panel de control</Link>
                <img src="/images/logotipo.png" width="200" />
            </div>}
        </div>
    </div>
}