import React from 'react';
import Binance from 'binance-api-node'
import { useToasts } from 'react-toast-notifications'
import Requests from "../../services/Requests"
import { useParams, Link } from 'react-router-dom';
import {dateWithFormatFromTime} from "../../services/Dates"
import Modal from '../../layout/Modal/Modal'
import "./Signal.css"
const clientBinance = Binance()

export default function Signal() {
    const {signalID} = useParams()
    const { addToast } = useToasts()
    //console.log("el signal id es", signalID)
    
    //list of all trades
    const [tradList, setTradList] = React.useState([])
    const [filtrdList, setFiltrdList] = React.useState([])

    const [ticker, setTicker] = React.useState({});
    const [signal, setSignal] = React.useState({});
    const [logs, setLogs] = React.useState({});
    //to show hidden forms
    const [edit, setEdit] = React.useState(false);
    const [editEnter, setEditEntr] = React.useState(false);

    //filter options
    const [selectAll, setSelectAll] = React.useState(false);
    const [includeDied, setIncludeDied] = React.useState(true);

    const [inputs, setInputs] = React.useState({ });
    const handleInputChange = (event) => {
        event.persist();
        setInputs(inputs => ({...inputs, [event.target.name]: event.target.value}));
    }
    const getPercentChange = () => {
        let takedPrice = ticker.askPrice 
        let pricediff =  takedPrice - signal.entryPrice
        //console.log("doy porcentaje", signal.entryPrice, takedPrice, pricediff, signal.side)
        return fixedToMultiplier(pricediff * 100 / signal.entryPrice, 100)
    }
    const fixedToMultiplier = (number, multiplier) => {
        return parseInt('' + (number * multiplier)) / multiplier;
    }
    const updateTicker = (_signal) =>{
        clientBinance.dailyStats({ symbol: _signal.inPair+""+_signal.outPair }).then(resp=>{
            console.log("resp de ticker", resp)
            setTicker(resp)
            //setTimeout(()=>updateTicker(_signal), 20000)
        })
    }
    const getInfo = async () => {
        let respSignal = await Requests.post("getSignal", {signalID})
        console.log("la resp de signal", respSignal)
        if(respSignal.data.success== 1){
            setSignal(respSignal.data.signal)
            updateTicker(respSignal.data.signal)
            setInputs({
                enterprice: respSignal.data.signal.entryPrice,
                gain: respSignal.data.signal.takeProfit,
                sl: respSignal.data.signal.stopLoss,
            })
        }
        let result = await Requests.post("getSignalTrades", {signalID})
        console.log("la resp de signaltrades", result)
        if(result.data.success== 1){
            setTradList(result.data.trades)
            setFiltrdList(result.data.trades)
        }
    }
    const cancelOrders = (ev) => {
        ev.preventDefault()
        console.log("llego cancel todo")
        if(window.confirm("¿Está seguro que desea cancelar las ordenes?, si la orden se ha procesado se generará una venta a mercado")){
            let payload = getSelected(["userID", "lotSize", "_id", "tradedPair"])
            if(payload.length == 0){
                addToast("No se han seleccionado clientes para ejecutar la acción", { appearance: 'error', autoDismiss: true })
                return false
            }
            Requests.postTradingServer("deleteOrders", { signalID, symbol: signal.inPair+""+signal.outPair, tradList: payload, asset: signal.inPair }).then((resp)=>{
                console.log("de delete", resp)
                if(resp.data.success==1){
                    getInfo()
                    addToast("Se han cancelado las ordenes y eliminado los saldos correctamente.", { appearance: 'success', autoDismiss: true })
                } else {
                    addToast(resp.data.message, { appearance: 'error', autoDismiss: true })
                }
            })
        }
    }
    const cancelOrder = (trad) => {
        console.log("llego trade", trad)
        Requests.post("deleteOrder", {processID: trad.processID}).then((resp)=>{
            console.log("de delete", resp)
        })
    }
    const editOCO = (ev) =>{
        ev.preventDefault()
        let payload = getSelected(["userID", "lotSize", "_id", "tradedPair"])
        if(payload.length == 0){
            addToast("No se han seleccionado clientes para ejecutar la acción", { appearance: 'error', autoDismiss: true })
            return false
        }
        Requests.postTradingServer("editOCO", {...inputs, symbol: signal.inPair+""+signal.outPair, signalID, tradList: payload}).then((resp)=>{
            console.log("de edit oco", resp)
            if(resp.data.success==1){
                setEdit(false)
                getInfo()
                addToast("Se han editado los puntos de venta correctamente.", { appearance: 'success', autoDismiss: true })
            } else {
                addToast(resp.data.message, { appearance: 'error', autoDismiss: true })
            }
        })
    }
    const editBuyOrders = (ev) =>{
        ev.preventDefault()
        let payload = getSelected(["userID", "lotSize", "_id"])
        if(payload.length == 0){
            addToast("No se han seleccionado clientes para ejecutar la acción", { appearance: 'error', autoDismiss: true })
            return false
        }
        Requests.postTradingServer("editBuyOrders", {...inputs, symbol: signal.inPair+""+signal.outPair, signalID, tradList: payload}).then((resp)=>{
            console.log("de edit oco", resp)
            if(resp.data.success==1){
                setEditEntr(false)
                getInfo()
                addToast("Se ha editado el punto de compra correctamente.", { appearance: 'success', autoDismiss: true })
            } else {
                addToast(resp.data.message, { appearance: 'error', autoDismiss: true })
            }
        })
    }
    const verificateSymbolBalance = async () => {
        let payload = getSelected(["userID", "tradedPair"])
        console.log("voy a enviar a aux", payload)
        let resp = await Requests.postTradingServer("verificateSymbolBalance", {tradList: payload})
        console.log("de verificateSymbolBalance", resp)
        if(resp.data.success==1){
            setLogs(logs=>({ ...logs, ...resp.data.logs}))
        } else {
            addToast(resp.data.message, { appearance: 'error', autoDismiss: true })
        }
    }
    const verificateTrades = async (account, inPair, initialOrderID, orderID, orderType) => {
        let resp = await Requests.postTradingServer("regenerateQuantities", {account, inPair, initialOrderID, orderID, orderType})
        console.log("de verificateTrades", resp)
        if(resp.data.success==1){
            getInfo()
            addToast("Se han regenerado correctamente los resultados del trade, se recargaran los datos...", { appearance: 'success', autoDismiss: true })
        } else {
            addToast(resp.data.message, { appearance: 'error', autoDismiss: true })
        }
    }
    const toHistoric = async (orderID) =>{
        if(!window.confirm("¿Está seguro que desea mover a histórico la orden?")) return false
        let resp = await Requests.postTradingServer("moveToHistoric", { orderID, signalID })
        console.log("de moveToHistoric", resp)
        if(resp.data.success==1){
            getInfo()
            addToast("Se han movido correctamente a histórico", { appearance: 'success', autoDismiss: true })
        } else {
            addToast(resp.data.message, { appearance: 'error', autoDismiss: true })
        }
    }
    const getSelected = (wichFieldsSInclud) => {
        let auxArray = []
        for (let index = 0; index < filtrdList.length; index++) {
            let element = {} //JSON.parse(JSON.stringify(filtrdList[index]));
            if(filtrdList[index].include === true){
                for (let idxB = 0; idxB < wichFieldsSInclud.length; idxB++) {
                    let column = wichFieldsSInclud[idxB]
                    element[column] = filtrdList[index][column];
                }
                element.index = index
                auxArray.push(element)
            }
        }
        return auxArray
    }
    const toggleHideHist = async () => {
        let contrary = !includeDied
        let aux = [...tradList]
        //visible el historico
        if(contrary){
            setFiltrdList(aux)
        } else {
            let filtered = []
            for (let index = 0; index < aux.length; index++) {
                if(aux[index].type != "Historial"){
                    filtered.push(aux[index])
                }
            }
            setFiltrdList(filtered)
        }
        setIncludeDied(contrary)
    }
    const toggleSelect = async (ev)=> {
        let contrary = !selectAll
        let aux = []
        for (let index = 0; index < tradList.length; index++) {
            if( tradList[index].type != "Historial" || ( includeDied && tradList[index].type === "Historial" )){
                let item = JSON.parse(JSON.stringify(tradList[index]))
                item.include = contrary
                aux.push(item)
            }
        }
        setFiltrdList(aux)
        setSelectAll(contrary)
    }
    React.useEffect(()=>{
        getInfo()
    }, [])
    return(
        <div>
            <h1>Detalles de la señal</h1>
            <div className="box">
                <div id="infoSignal">
                    <div>
                        Pares <strong>{signal.inPair+""+signal.outPair}</strong> <br/><br/>
                        Tipo <strong>{signal.type}</strong> <br/><br/>
                        Estado <strong  style={{color: signal.active ? "green" : "red"}}>{signal.active ? "Activa": "Inactiva"}</strong> <br/><br/>
                        Creada <strong>{dateWithFormatFromTime(signal.registeredTime)}</strong>
                    </div>
                    <div>
                        Precio de entrada <strong>{signal.entryPrice}</strong> <br/><br/>
                        Tomar beneficios <strong>{signal.takeProfit}</strong> <br/><br/>
                        Pare de perder <strong>{signal.stopLoss}</strong> <br/><br/>
                    </div>
                    <div>
                        Precio de compra <strong>{ticker.askPrice}</strong> <br/><br/>
                        Precio de venta <strong>{ticker.bidPrice}</strong> <br/><br/>
                        Tipo Cierre: <strong>{signal.kindOfClose ? signal.kindOfClose : null}</strong> <br/><br/>
                        Porcentaje de cambio <strong style={{color: getPercentChange(signal) >= 0 ? "green" : "red"}}>{getPercentChange(signal)}%</strong>
                    </div>
                </div>
                <div className="buttonsBox">
                    <button style={{fontSize: 15}} onClick={ev=>setEditEntr(edit=>!edit)}>Editar Precio de entrada</button>
                    <button style={{fontSize: 15}} onClick={ev=>setEdit(edit=>!edit)}>Editar TP y SL</button>
                    <button style={{fontSize: 15}} onClick={verificateSymbolBalance}>Verificar saldo de símbolo</button>
                    <button style={{fontSize: 15}} onClick={toggleSelect}>{selectAll ? "Des-seleccionar todos" : "Seleccionar todos"}</button>
                    <button style={{fontSize: 15}} onClick={toggleHideHist}>{ !includeDied ? "Des-ocultar cerradas" : "Ocultar cerradas"}</button>
                    <button style={{fontSize: 15,backgroundColor: "red", color: "white"}} onClick={cancelOrders}>Cierre de emergencia</button>
                </div>
                <table className="signalsTable" style={{marginTop: 20}}>
                    <thead>
                        <tr>
                            <th>*</th>
                            <th>Usuario</th>
                            <th>Detalles</th>
                            <th>Estados</th>
                            <th>Benf./Perd.</th>
                            <th>Salida</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {filtrdList.map((trad, index)=>{    
                            //console.log("paso x trad", trad)                     
                            return <tr>
                                <td><input type="checkbox" checked={trad.include} onChange={ev=> {
                                    let newval = ev.target.checked
                                    //console.log("el nuevo val es", newval)
                                    let aux = {...filtrdList[index], include: newval}
                                    let auxList = [...filtrdList]
                                    auxList[index] = aux
                                    setFiltrdList(auxList)   
                                }}  style={{transform: "scale(2)", "-webkit-transform": "scale(2)"}} /></td>
                                <td><Link to={"/account/"+trad.userID} target="_blank">{trad.userID.substring(0,5)}...</Link></td>
                                <td>
                                    Pretende {signal.inPair}: {trad.buyOrderQuantity}<br/>
                                    Pretende {signal.outPair}: {trad.pretendedToBuy && !isNaN(trad.pretendedToBuy) && trad.pretendedToBuy.toFixed(8)}<br/>
                                </td>
                                <td>
                                    OC: {trad.buyOrderStatus} <abbr title={trad.buyOrderID}>ID</abbr> <abbr title={dateWithFormatFromTime(trad.buyOrderExecutionTime)}>T.</abbr> <br/>
                                    OV: {trad.sellOrderStatus} { trad.sellOrderID && <React.Fragment><abbr title={trad.sellOrderID}>ID</abbr> <abbr title={dateWithFormatFromTime(trad.sellOrderExecution)}>T.</abbr></React.Fragment> } <br/> 
                                    GLOBAL: {trad.globalStatus}<br/>
                                    TIPO: <strong>{trad.type}</strong>
                                </td>
                                <td>
                                    Comprado: {trad.spendedQuantity}<br/>
                                    Vendido: {trad.receivedQuantity} <br/>
                                    Satoshis: <span style={{color: isNaN(trad.resultantQuantity) ? "black" : trad.resultantQuantity >= 0 ? "green" : "red"}}>{trad.resultantQuantity}</span><br/>
                                    Porcent.: <span style={{color: isNaN(trad.resultantPercentage) ? "black" : trad.resultantPercentage >= 0 ? "green" : "red"}}>{trad.resultantPercentage}%</span>
                                </td>
                                <td>
                                    <pre>{logs[index]}</pre>
                                </td>
                                <td>
                                    <button style={{transform: "scale(1.2)"}} onClick={ev=>verificateTrades(trad.userID, signal.inPair, trad.initialOrderID, trad._id, trad.type)}>Recuperar Comp.</button><br/>
                                    <button style={{transform: "scale(1.2)"}} onClick={ev=>toHistoric(trad._id)}>A Historico</button>
                                </td>
                            </tr>
                        })}
                    </tbody>
                </table>
                <p>
                    Total de personas en la señal: {tradList.length}
                </p>
            </div>
            <div className={`modalContainer ${ ( edit || editEnter ) ? "" : "hidden"}`} style={{top: 0,left: 0}}>
            <div className="modalBox">
                <div className="closeModalIcon">
                        <svg
                            version="1.1"
                            xmlns="http://www.w3.org/2000/svg"
                            xlink="http://www.w3.org/1999/xlink"
                            x="0px"
                            y="0px"
                            viewBox="0 0 1000 1000"
                            enable-background="new 0 0 1000 1000"
                            space="preserve"
                            onClick={ev=>{setEdit(false); setEditEntr(false)}}
                        >
                            <g>
                                <path
                                    d="M964.7,157.6L622.3,500l342.4,342.4l0,0c15.7,15.7,25.3,37.3,25.3,61.1c0,47.7-38.7,86.5-86.5,86.5c-23.9,0-45.5-9.7-61.1-25.3l0,0L500,622.3L157.6,964.7l0,0C142,980.3,120.3,990,96.5,990C48.7,990,10,951.3,10,903.5c0-23.9,9.7-45.5,25.3-61.1l0,0L377.7,500L35.3,157.6l0,0C19.7,142,10,120.3,10,96.5C10,48.7,48.7,10,96.5,10c23.9,0,45.5,9.7,61.1,25.3l0,0L500,377.7L842.4,35.3l0,0C858,19.7,879.7,10,903.5,10c47.8,0,86.5,38.7,86.5,86.5C990,120.3,980.3,142,964.7,157.6L964.7,157.6z"
                                    fill="#BCBCCB"
                                />
                            </g>
                        </svg>
                </div>
                <div className="logotype">
                    <img src="/images/logotipo.png" />
                </div>
                <div style={{position: "relative", marginTop: 30}}>
                    <p className="formUpgradeTitle">
                        EDITAR ORDEN <b>{signal.inPair + signal.outPair}</b>
                    </p>
                    {editEnter && <form  id="formBuyAssets" onSubmit={ editBuyOrders }>
                        <p>
                            <label>PRECIO DE ENTRADA: </label>
                            <input type="text"  placeholder="Precio de entrada" className="formControl"  required name="enterprice" 
                                value={inputs.enterprice} onChange={handleInputChange}/>
                        </p>
                        <p>
                            <input type="submit" className="buttn buttn-primary" value="Modificar precio de entrada" />
                        </p>
                    </form>}
                    {edit && <form  id="formBuyAssets" onSubmit={ editOCO }>
                            <p>
                                TOMAR BENEFICIOS:
                                <input type="text" placeholder="Tomar beneficios" className="formControl" required name="gain" 
                                value={inputs.gain} onChange={handleInputChange}/>
                            </p>    
                            <p>
                                PARE DE PERDER:
                                <input type="text" placeholder="Precio SL" className="formControl" required name="sl" 
                                value={inputs.sl} onChange={handleInputChange}/>
                            </p>
                        <p>
                            <input type="submit" className="buttn buttn-primary" value="Modificar TP y SL" />
                        </p>
                    </form>}
                </div>
            </div>
        </div>
        </div>
    )

}