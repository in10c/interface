import React from 'react';
import { useToasts } from 'react-toast-notifications'
import Requests from "../../services/Requests"

export default function Debug() {
    const [log, setLog] = React.useState(false)
    const { addToast } = useToasts()

    const getVPSWOOrders = async (ev) => {
        let resp = await Requests.getTradingServer("getVPSWOOrders")
        console.log("de getVPSWOOrders", resp)
        if(resp.data.success==1){
            setLog(resp.data.log)
        } else {
            addToast(resp.data.message, { appearance: 'error', autoDismiss: true })
        }
    }
    return(
        <div>
            <h1>Detalles de la señal</h1>
            <div className="box">
                <div id="infoSignal">
                    <button className="buttn buttn-primary" onClick={getVPSWOOrders}>Get Log</button>
                </div>
                <div>
                    <pre>
                        {log}
                    </pre>
                </div>
            </div>
            
        </div>
    )

}