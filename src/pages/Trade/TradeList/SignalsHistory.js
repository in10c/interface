import React from 'react';
import Requests from "../../../services/Requests"
import {dateWithNumberFormatFromTime} from "../../../services/Dates"
import { Link } from 'react-router-dom';
import "./TradeList.css"

export default function SignalsHistory(){
    const [tradList, setTradList] = React.useState([]);
    React.useEffect(()=>{
        Requests.getTradingServer("signalsHistory").then((result)=>{
            console.log("de trade monit ", result)
            setTradList(result.data.signals)
        })
    }, [])
    return <table className="signalsTable">
        <thead>
            <tr>
                <th>Pares</th>
                <th>Tipo</th>
                <th>Detalles</th>
                <th>Cierre</th>
                <th>Fecha</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            {tradList.slice(0).reverse().map(signal=>{
                return <tr>
                    <td><span style={{fontWeight:"bold"}}>{signal.inPair+""+signal.outPair}</span><br/><span style={{color:"gray"}}>{signal._id}</span></td>
                    <td>{signal.type}</td>
                    <td>
                        ENTRADA: {signal.entryPrice}<br/>
                        T. PROFIT: {signal.takeProfit}<br/>
                        STOP LOSS: {signal.stopLoss}
                    </td>
                    <td>{signal.kindOfClose}</td>
                    <td>{signal.registeredTime ? dateWithNumberFormatFromTime(signal.registeredTime) : null}</td>
                    <td>
                        <Link className="buttn buttn-default" target="_blank" to={"/signal/"+signal._id}>Ver señal</Link>
                    </td>
                </tr>
            })}
        </tbody>
    </table>
}