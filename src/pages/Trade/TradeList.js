import React from 'react';
import Requests from "../../services/Requests"
import { Redirect } from 'react-router-dom';
import SignalsHistory from "./TradeList/SignalsHistory"
import ActiveSignals from "./TradeList/ActiveSignals"

export default function TradeList() {
    const [redirect, setRedirect] = React.useState(undefined);
    const [tab, setTab] = React.useState(1)
    React.useEffect(()=>{
        Requests.get("amItrader").then((result)=>{ 
            if(result.data.result === false) setRedirect(true) 
        })
    }, [])
    const tabs = () =>{
        return <div id="pestanas">
            <div className={tab===1 && "active"} onClick={ev=>setTab(1)}>Señales activas</div>
            <div className={tab===2 && "active"} onClick={ev=>setTab(2)}>Historial de señales</div>
        </div>
    }
    return(
        <div>
            {redirect ?
            <Redirect to="/" />
            :
            <React.Fragment>
                <h1>Lista de señales</h1>
                <div className="box">
                    {tabs()}
                    {tab=== 1 && <ActiveSignals/>}
                    {tab=== 2 && <SignalsHistory/>}
                </div>
            </React.Fragment>
            }
        </div>
    )

}