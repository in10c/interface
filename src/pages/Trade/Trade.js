import React from 'react';
import { useToasts } from 'react-toast-notifications'
import Loader from 'react-loader-spinner'
import { Link, Redirect } from 'react-router-dom'
import Requests from "../../services/Requests"
import "./Trade.css"
import Constants from '../../services/Constants';
import { LayoutContext } from "../../layout/LayoutContext/LayoutContext";

export default function Trade() {
    const { setModalVisible } = React.useContext(LayoutContext);
    const { addToast } = useToasts()
    const [inputs, setInputs] = React.useState({
        type: "BUY",
        outPair: "BTC",
        queueAmount: Constants.queueAmount
    });
    //Variables de cargando
    const [loading, setLoad] = React.useState(false)
    //variables de listas
    const [loadingList, setLoadList] = React.useState(true)
    const [clientsList, setClients] = React.useState([])
    const [orderedList, setOrdered] = React.useState([])
    const [queueList, setQueue] = React.useState([])
    const [bnbPrice, setBNBPrice] = React.useState(0)
    //sumatorias btc
    const [BTCToExecute, setBTCToExecute] = React.useState(0)
    const [totalInBTC, setTotalInBTC] = React.useState(0)
    //auxiliares
    const [errosList, setErrors] = React.useState([])
    const [tab, setTab] = React.useState(1)
    const [signalID, setSignalID] = React.useState(false)
    const [clusterOcup, setClusterOcup] = React.useState(false)

    const [redirect, setRedirect] = React.useState(undefined);
    React.useEffect(()=>{
        Requests.get("amItrader").then((result)=>{
            console.log("de amItrader ", result)
            setRedirect(result.data.result !== true)
            if(result.data.result){ //getQueued
                getParticipants().then(()=>{
                    setLoadList(false)
                })
                Requests.getTradingServer("clusterOcuppation").then((response)=>{
                    console.log("resultado de clusterOcuppation", response)
                    if(response.data.success == 1){
                        setClusterOcup(response.data.clusterOcupation)
                    }
                })
            }
        })
    }, [])
    const handleInputChange = (event) => {
        event.persist();
        setInputs(inputs => ({...inputs, [event.target.name]: event.target.value}));
    }
    const replicate = (ev)=>{
        ev.preventDefault()
        if(!inputs.inPair) {
            addToast("El par a comprar es requerido", { appearance: 'error', autoDismiss: true })
            return false
        }
        setLoad(true)
        let filteredQueue = queueList.filter(val=> val.skiped === false)
        Requests.postTradingServer("disperseOrder", {...inputs, clientsQueue: filteredQueue }).then((result)=>{
            setLoad(false)
            console.log("status ", result)
            if(result.data.success==1){
                addToast("Se ha enviado la compra correctamente.", { appearance: 'success', autoDismiss: true })
                setSignalID(result.data.signalID)
            } else {
                addToast(result.data.message, { appearance: 'error', autoDismiss: true })
            }
        })
    }
    const getMainQueue = (ev)=>{
        setLoadList(true)
        Requests.postTradingServer("getMainQueue", { queueAmount: inputs.queueAmount, inPair: inputs.inPair }).then((result)=>{
            console.log("de getMainQueue ", result)
            setLoadList(false)
            if(result.data.success==1){
                let sum = 0
                result.data.queue.map(val=>sum+=val.btcPortion)
                setBTCToExecute(sum)
                setQueue(result.data.queue)
                addToast("Se ha traido la cola correctamente.", { appearance: 'success', autoDismiss: true })
            } else {
                addToast(result.data.message, { appearance: 'error', autoDismiss: true })
            }
        })
    }
    const getParticipants = async () => {
        let result = await Requests.getTradingServer("getParticipants")
        console.log("de obtener participantes ", result)
        if(result.data.success==1){
            setOrdered(result.data.sessions)
        } else {
            addToast(result.data.message, { appearance: 'error', autoDismiss: true })
        }
    }
    const selectParticipantsForAnalysis = async (ev) => {
        setLoadList(true)
        let result = await Requests.postTradingServer("generateAnalytics", { clientsParticipating: inputs.clientsParticipating })
        setLoadList(false)
        console.log("de obtener generateAnalytics ", result)
        if(result.data.success === 1){
            addToast("Se han actualizado correctamente las estadísticas y balances de los usuarios.", { appearance: 'info', autoDismiss: true })
            const {ready, notReady, totalInBTC, bnbPrice} = result.data
            setTotalInBTC(totalInBTC)
            setClients(ready)
            setErrors(notReady)
            setBNBPrice(bnbPrice)
        } else {
            addToast(result.data.message, { appearance: 'error', autoDismiss: true })
        }
    }
    const tabs = () =>{
        return <div id="pestanas">
            <div className={tab===1 && "active"} onClick={ev=>setTab(1)}>Nueva orden</div>
            <div className={tab===2 && "active"} onClick={ev=>setTab(2)}>Cola a ejecutar</div>
            <div className={tab===3 && "active"} onClick={ev=>setTab(3)}>Clientes con análisis</div>
            <div className={tab===4 && "active"} onClick={ev=>setTab(4)}>Clientes no añadidos</div>
            <div className={tab===5 && "active"} onClick={ev=>setTab(5)}>Todos los clientes</div>
        </div>
    }
    const resetQueue = (ev) => { setQueue([]); setBTCToExecute(0); }

    const handlePointChange = (event) => {
        event.persist();
        let aux = event.target.value
        if(aux.includes(",")) aux = aux.replace(/,/g, ".") 
        setInputs(inputs => ({...inputs, [event.target.name]: aux}));
    }
    const powerOffApp = async (account, comments) => {
        let result = await Requests.postTradingServer("powerOffAccountApp", { account, comments })
        console.log("powerOffAccountApp ", result)
        if(result.data.success==1){
            addToast("Se ha apagado la app correctamente.", { appearance: 'success', autoDismiss: true })
        } else {
            addToast(result.data.message, { appearance: 'error', autoDismiss: true })
        }
    }
    return(
            <div>
            
            {signalID && <Redirect to={"/signal/"+signalID} />}
            {redirect === undefined ? 
            <div></div>
            :
            redirect ?
            <Redirect to="/" />
            :
            <React.Fragment>
            <h1>Enviar Trade (SPOT)</h1>
            {clusterOcup && <div className="clusterMapOcup">
                {Object.keys(clusterOcup).map(clusterNumber=><div style={{backgroundColor: clusterOcup[clusterNumber].used > 25 ? "red": "#067684"}}>
                    <span>Cluster #{clusterNumber}</span><br/>
                    <span>{clusterOcup[clusterNumber].used}/30 = {30-clusterOcup[clusterNumber].used}</span>
                </div>)}
            </div>}
            {tab === 1 && <form className="box" style={{marginTop:50}} onSubmit={replicate}>
                {tabs()}
                {loadingList && <div style={{
                    display: "flex",
                    flexDirection: "row",
                    borderWidth: 1,
                    borderColor: "#1ABFD3",
                    borderStyle: "solid",
                    padding: 10,
                    borderRadius: 10
                }}>
                    <Loader
                        type="Grid"
                        color="#1ABFD3"
                        height={30}
                        width={30}
                    />
                    <div style={{marginLeft: 10, display: "flex", alignItems: "center", alignContent: "center"}}>Se están actualizando los balances de los clientes, por favor espere...</div>
                </div>}
                { clientsList.length === 0 && <div className="form-control">
                <h2>Paso 1 - Elige el número de clientes a analizar</h2>
                    <input type="text" placeholder="Ejemplo 200" className="formControl" required name="clientsParticipating" 
                    value={inputs.clientsParticipating} onChange={handleInputChange}/>
                    <button type="button" disabled={loadingList} className="buttn buttn-primary" onClick={selectParticipantsForAnalysis}>Clientes a analizar</button>
                </div>}
                { clientsList.length > 0 && queueList.length === 0 && <div className="formControl" style={{padding: 20}}>
                    <h2>Paso 2 - De los clientes analizados, elige que cantidad de BTC se enviará a la señal</h2>
                    <div className="formControl">
                        Par a comprar:
                        <input type="text" placeholder="Ejemplo BEAM" className="formControl" required name="inPair" 
                        value={inputs.inPair} onChange={handleInputChange}/>
                    </div>
                    <div className="formControl">
                        Cantidad de BTC para la cola:
                        <input type="text" placeholder="Ejemplo 2.5" className="formControl" required name="queueAmount" 
                        value={inputs.queueAmount} onChange={handleInputChange}/>
                    </div>
                    <button type="button" disabled={loadingList} className="buttn buttn-primary" onClick={getMainQueue}>Generar cola de clientes</button>
                </div> } 
            
                { queueList.length > 0 && <React.Fragment>
                    <h2>Paso 3 - Envía la señal de Trading</h2>
                    <div className="formControl" style={{padding: 20}}>
                        Par a comprar:
                        <strong style={{fontSize:25, marginLeft: 10}}>{inputs.inPair}</strong>
                    </div>
                    
                    <div className="prices">
                        <div className="formControl">
                            Precio de entrada:
                            <input type="text" className="formControl" required name="enterprice" 
                            value={inputs.enterprice} onChange={handlePointChange}/>
                        </div>
                        <div className="formControl">
                            Tomar beneficios:
                            <input type="text" className="formControl" required name="gain" 
                            value={inputs.gain} onChange={handlePointChange}/>
                        </div>
                        <div className="formControl">
                            Stop Loss:
                            <input type="text" className="formControl" required name="sl" 
                            value={inputs.sl} onChange={handlePointChange}/>
                        </div>
                    </div>
                </React.Fragment>}
                {loading ? "Cargando..." : queueList.length > 0 ? <div style={{display: "flex", justifyContent: "flex-end"}}>
                    <button className="buttn buttn-default" style={{marginLeft: 15, marginRight: 15}} onClick={resetQueue}>Resetear la cola </button>
                    <button className="buttn buttn-primary" style={{marginRight: 15}}>Enviar orden </button>
                </div> : null}
                
            </form>}
            {tab === 2 && <div className="box">
                {tabs()}
                <p>
                    * OA: Ordenes Abiertas<br/>
                    * OCH: Ordenes Cerradas Hoy
                </p>
                <table className="signalsTable">
                    <thead>
                        <tr>
                            <th>Usuario</th>
                            <th>Detalles</th>
                            <th>Extras</th>
                            <th>Salida</th>
                            <th>Proc.</th>
                        </tr>
                    </thead>
                    <tbody>
                        {queueList.map(client=><tr>
                            <td>
                                <Link to={"/account/"+client.userID} target="_blank">{client.email}</Link>
                            </td>
                            <td>
                                Porcion a usar: {client.btcPortion && !isNaN(client.btcPortion) && client.btcPortion.toFixed(8)} BTC<br/>
                                Balance: {client.btcBalance} BTC<br/>
                                En ordenes: {client.btcInOrders} BTC<br/>
                                Usando: {client.totalBalanceWithOrders && !isNaN(client.totalBalanceWithOrders) && client.totalBalanceWithOrders.toFixed(8)} BTC<br/>
                            </td>
                            <td>
                                O. Abiertas: {client.openOrders}<br/>
                                O. Cerradas: {client.historicOrders} <br/>
                                Limites: {client.limits}<br/>
                                Balance BNB: {client.bnbBalance} 
                            </td>
                            <td>
                                {client.skiped === false && <span>{client.socketOrCluster === "cluster" ? "Cluster #"+client.assignedCluster:  "Socket: "+client.assignedSocket }</span>}
                                {client.skiped === true && <span>{client.socketOrCluster === "cluster" ? "Cluster #"+client.assignedCluster:  "Socket: "+client.assignedSocket }</span>}
                            </td>
                            <td>
                                <strong style={{fontWeight: "bold", color: client.skiped === false ? "green" : "red"}}>{client.skiped === false ? "A ejecutar" : "Saltar"}</strong><br/>
                                {client.comments && client.comments.length > 0 ? client.comments : null}
                            </td>
                        </tr>)}
                    </tbody>
                </table>
                <p style={{textAlign: "right"}}>
                    Suma de BTC a enviar: {BTCToExecute && !isNaN(BTCToExecute) && BTCToExecute.toFixed(8)}BTC <br/>
                    Total de registros: {queueList.length}
                </p>
            </div>}
            {tab === 3 && <div className="box">
                {tabs()}
                <p>
                    * OA: Ordenes Abiertas<br/>
                    * OCH: Ordenes Cerradas Hoy<br/>
                    * BNB Price: {bnbPrice}
                </p>
                <table className="signalsTable">
                    <thead>
                        <tr>
                            <th>Usuario</th>
                            <th>Estadisticas</th>
                            <th>Balance en ordenes</th>
                            <th>Porcion a usar</th>
                            <th>Balance actual</th>
                            <th>Entrada + ordenes</th>
                            <th>Límites de licencias</th>
                            <th>Balance BNB</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        {clientsList.map(client=><tr>
                            <td>
                                <Link to={"/account/"+client.userID} target="_blank">{client.email}</Link>
                            </td>
                            <td>
                                # OA: {client.openOrders}<br/>
                                # OCH: {client.historicOrders}
                            </td>
                            <td>{client.btcInOrders}</td>
                            <td>{client.btcPortion && !isNaN(client.btcPortion) && client.btcPortion.toFixed(8)}BTC ({client.percentageToUse}%)</td>
                            <td>{client.btcBalance}</td>
                            <td>{ client.totalBalanceWithOrders && !isNaN(client.totalBalanceWithOrders) && client.totalBalanceWithOrders.toFixed(8) }</td>
                            <td>{client.limits}</td>
                            <td>{client.bnbBalance}</td>
                            
                        </tr>)}
                    </tbody>
                </table>
                <p style={{textAlign: "right"}}>
                   Total en porciones para usar: {totalInBTC} BTC <br/>
                   Total de registros: {clientsList.length}
                </p>
            </div>}
            {tab === 4 && <div className="box">
                {tabs()}
                <p>
                    * OA: Ordenes Abiertas<br/>
                    * OCH: Ordenes Cerradas Hoy
                </p>
                <table className="signalsTable">
                    <thead>
                        <tr>
                            <th>Usuario</th>
                            <th>Estadisticas</th>
                            <th>Error</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {errosList.map(client=><tr>
                            <td>
                                <Link to={"/account/"+client.userID} target="_blank">{client.email}</Link>
                            </td>
                            <td>
                                # OA: {client.openOrders}<br/>
                                # OCH: {client.historicOrders}
                            </td>
                            <td>{client.comments}</td>
                            <td>
                                <button onClick={ev=>powerOffApp(client.userID, client.comments)}>Apagar Bot</button>
                            </td>
                        </tr>)}
                    </tbody>
                </table>
                <p>
                    Total de registros: {errosList.length}
                </p>
            </div>}
            {tab === 5 && <div className="box">
                {tabs()}
                <p>
                    Todos los clientes
                </p>
                <table >
                    <thead>
                        <tr>
                            <th>Usuario</th>
                            <th>Ordenes abiertas</th>
                            <th>Ordenes cerradas</th>
                        </tr>
                    </thead>
                    <tbody>
                        {orderedList.map(client=><tr>
                            <td>
                                <Link to={"/account/"+client.userID} target="_blank">{client.email}</Link>
                            </td>
                            <td>
                                {client.openOrders}
                            </td>
                            <td>
                                {client.historicOrders}
                            </td>
                        </tr>)}
                    </tbody>
                </table>
                <p>
                    Total de registros: {orderedList.length}
                </p>
            </div>}
            </React.Fragment>
}
        </div>
    )

}