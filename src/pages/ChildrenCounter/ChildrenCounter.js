import React, { useEffect, useState } from "react";
import Requests from "../../services/Requests";
import "./ChildrenCounter.css"

export default function ChildrenCounter() {
    const [children, setChildren] = useState(0);

    useEffect(() => {
        async function getInfo() {
            const response = await Requests.get("getMyChildren");
            if (!!response.data.failAuth) {
                localStorage.removeItem("user_bitwabi");
                localStorage.removeItem("access-token");
            } else setChildren(response.data.children);
        }
        getInfo();
    }, []);

    return (
        <div className={`childrenCounter ${!children ? "noChildren" : ""}`}>
            <span className="number">{children}</span>
            <span className="label">REFERIDOS</span>
        </div>
    );
}
