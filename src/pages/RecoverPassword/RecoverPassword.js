import React, {useState, useEffect} from "react";
import { useToasts } from 'react-toast-notifications'
import * as md5 from "md5";
import {Redirect, Link, NavLink, useParams} from "react-router-dom"
import Requests from "../../services/Requests"
import LayoutVisitors from "../../layout/LayoutVisitors"
import FormPassword from "../../formsComponents/password/formPassword"
import "../Signin/Signin.css"

export default function SignIn({newAccount})  {
    let { token } = useParams()
    const [password, setPass] = useState();
    const [confirmPass, setConfirmPass] = useState();
    const [redirect, setRed] = useState(false);
    const { addToast } = useToasts()

    useEffect(() => {
        Requests.get('recoverPassword/'+token)
        .then( (response) =>{
            console.log("respuesta de check", response)
            if(response.data.success === 0){
                setRed(true)
                addToast(response.data.message, { appearance: 'error', autoDismiss: true })
            }
        })
    }, [])

    const handleSubmit = (event) => {
        event.preventDefault()
        if(password !== confirmPass){
            addToast("Las contraseñas no coinciden", { appearance: 'error', autoDismiss: true })
            return
        }
        Requests.post('recoverPassword/'+token, { password: md5(password) })
        .then( (response) =>{
            console.log("respuesta de check", response)
            if(response.data.success == 1){
                addToast(response.data.message, { appearance: 'success', autoDismiss: true })
                setRed(true)
            } else {
                addToast(response.data.message, { appearance: 'error', autoDismiss: true })
                setRed(true)
            }
        })
    }

    return (
        <LayoutVisitors>
            {redirect && <Redirect to='/signIn' />}
            <form onSubmit={handleSubmit} id="formSignIn">
                <p className="desc">Elija y confirme su nueva contraseña Bitwabi.</p>
                <div>
                    <FormPassword placeholder="Contraseña" value={password} onChange={ev=>setPass(ev.target.value)}/>
                    <FormPassword placeholder="Confirmar contraseña" value={confirmPass} onChange={ev=>setConfirmPass(ev.target.value)}/>
                </div>
                <div>
                    <button className="buttn buttn-primary">Guardar</button>
                </div>
            </form>
        </LayoutVisitors>
    )
}
