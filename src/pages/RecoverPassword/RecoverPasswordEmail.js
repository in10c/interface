import React, { useState } from "react";
import { useToasts } from "react-toast-notifications";
import Requests from "../../services/Requests";
import { NavLink} from "react-router-dom"
import LayoutVisitors from "../../layout/LayoutVisitors";
import Loading from "../../layout/Loading/Loading";
import "../Signin/Signin.css";

export default function SignIn() {
    const [email, setEmail] = useState();
    const [phone, setPhone] = useState();
    const [byEmail, setByEmail] = useState(false);
    const [byPhone, setByPhone] = useState(false);
    const { addToast } = useToasts();
    const [loading, setLoading] = useState(false);
    const [counter, setCounter] = useState(false);
    const [time, setTime] = useState({ minutes: 15, seconds: 0 });
    const [timer, setTimer] = useState()

    const count = (limit) => {
        const now = (new Date(Date.now())).getTime()
        console.log(now)
        const timeLimit = limit.getTime()
        console.log(Math.floor((timeLimit - now)/60000))
        const minutes = Math.floor((timeLimit - now)/60000)
        console.log(parseInt(((timeLimit - now)%60000)/1000))
        const seconds = parseInt(((timeLimit - now)%60000)/1000)
        if(minutes === 0 && seconds === 0){
            setCounter(false)
        }
        if(minutes >= 0 && seconds >= 0){
            setTime({minutes, seconds})
        }
    };

    const handleSubmit = (event) => {
        setLoading(true);
        event.preventDefault();
        Requests.post("recoverPassword", { email }).then((response) => {
            console.log("respuesta de check", response);
            if (response.data.success == 1) {
                addToast(response.data.message, { appearance: "success", autoDismiss: true });
                const timeLimit = new Date(Date.now())
                timeLimit.setSeconds(960)
                clearInterval(timer)
                setTimer(setInterval(()=>{count(timeLimit)}, 1000))
                setCounter(true)
                setEmail("");
            } else {
                addToast(response.data.message, { appearance: "error", autoDismiss: true });
            }
            setLoading(false);
        });
    };

    const handleSubmitBySms = (event) => {
        setLoading(true);
        event.preventDefault();
        Requests.post("recoverPasswordBySms", { phone }).then((response) => {
            console.log("respuesta de check", response);
            if (response.data.success == 1) {
                addToast(response.data.message, { appearance: "success", autoDismiss: true });
                const timeLimit = new Date(Date.now())
                timeLimit.setSeconds(960)
                clearInterval(timer)
                setTimer(setInterval(()=>{count(timeLimit)}, 1000))
                setCounter(true)
                setPhone("");
            } else {
                addToast(response.data.message, { appearance: "error", autoDismiss: true });
            }
            setLoading(false);
        });
    };
    return (
        <LayoutVisitors>
            <Loading visible={loading} />
            {
                byEmail ? 
                    <form onSubmit={handleSubmit} id="formSignIn">
                        <p className="desc">
                            Por favor introduce tu <b>email</b>
                        </p>
                        <div>
                            <div>
                                <input
                                    type="email"
                                    placeholder="Correo electrónico"
                                    className="formControl"
                                    required
                                    value={email}
                                    disabled={counter}
                                    onChange={(ev) => setEmail(ev.target.value)}
                                />
                            </div>
                        </div>
                        {counter && <div>
                            <p>Puedes volver a enviar un código en:</p>
                            <div className="bubble" style={{ borderColor: "gray", borderWidth: 1, borderStyle: "solid" }}>
                                {time.minutes + ":" + time.seconds}
                            </div>
                        </div>}
                        {!counter && 
                        <div>
                            <button className="buttn buttn-primary">Enviar</button>
                        </div>}
                        <div className="legal" style={{marginTop: 20}}>
                            <NavLink to="/signin">Regresar al acceso</NavLink>
                        </div>
                    </form>
                : byPhone ?
                    <form onSubmit={handleSubmitBySms} id="formSignIn">
                        <p className="desc">
                            Por favor introduce tu <b>número de teléfono</b>
                        </p>
                        <div>
                            <div>
                                <input
                                    type="phone"
                                    placeholder="Número de teléfono"
                                    className="formControl"
                                    required
                                    value={phone}
                                    disabled={counter}
                                    onChange={(ev) => setPhone(ev.target.value)}
                                />
                            </div>
                        </div>
                        {counter && <div>
                            <p>Puedes volver a enviar un código en:</p>
                            <div className="bubble" style={{ borderColor: "gray", borderWidth: 1, borderStyle: "solid" }}>
                                {time.minutes + ":" + time.seconds}
                            </div>
                        </div>}
                        {!counter && 
                        <div>
                            <button className="buttn buttn-primary">Enviar</button>
                        </div>}
                        <div className="legal" style={{marginTop: 20}}>
                            <NavLink to="/signin">Regresar al acceso</NavLink>
                        </div>
                    </form>
                :
                    <form>
                        <p className="desc">
                            Por favor elija una opción para recuperar su contraseña
                        </p>
                        <button className="buttn buttn-primary bySms" onClick={() => {setByPhone(true)}}>SMS</button>
                        <button className="buttn buttn-primary byEmail" onClick={() => {setByEmail(true)}}>Email</button>
                        <div className="legal" style={{marginTop: 20}}>
                            <NavLink to="/signin">Regresar al acceso</NavLink>
                        </div>
                    </form>
            }
        </LayoutVisitors>
    );
}
