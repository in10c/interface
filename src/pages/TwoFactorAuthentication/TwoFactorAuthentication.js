import React, { useEffect, useState } from 'react'
import ReactCodeInput from 'react-verification-code-input'
import { Redirect } from "react-router-dom"
import { useToasts } from "react-toast-notifications"
import "./TwoFactorAuthentication.css"
import Requests from "../../services/Requests"

//////////////////////////////////////
///////////// Deprecated /////////////
//////////////////////////////////////

export default function TwoFactorAuthentication() {

    const { addToast } = useToasts()
    const [enabled, setEnabled] = useState(false)
    const [tempVerification, setTempVerification] = useState(false)
    const [desactivatingVerification, setDesactivatingVerification] = useState(false)
    const [desactivateVerificationCode, setDesactivateVerificationCode] = useState("")
    const [qrCode, setQrCode] = useState()
    const [tempVerificationCode, setTempVerificationCode] = useState("")
    const [redirect, setRedirect] = useState(false)

    useEffect(() => {
        async function getInfo() {
            const response = await Requests.get("verifyTwoFactorActive")
            if (!!response.data.failAuth) {
                addToast(response.data.message, { appearance: "error", autoDismiss: true })
                localStorage.removeItem("user_bitwabi")
                localStorage.removeItem("access-token")
                setRedirect(true)
            }
            setEnabled(response.data.result)
        }
        getInfo();
    }, [])

    const OnClick = async (event) => {
        const response = await Requests.get("getTwoFactVerQRCode")
        if (!!response.data.failAuth) {
            addToast(response.data.message, { appearance: "error", autoDismiss: true })
            localStorage.removeItem("user_bitwabi")
            localStorage.removeItem("access-token")
            setRedirect(true)
        }
        if (response.data.success === 1) {
            setQrCode(response.data.qrcode)
            setTempVerification(true)
            setDesactivatingVerification(false)
        }else{
            addToast(response.data.message, { appearance: "error", autoDismiss: true })
        }
    }

    const OnSubmit = async (event) => {
        event.preventDefault()
        const response = await Requests.post("verifyTwoFactorTEMPSecret", {code: tempVerificationCode})
        if (!!response.data.failAuth) {
            addToast(response.data.message, { appearance: "error", autoDismiss: true })
            localStorage.removeItem("user_bitwabi")
            localStorage.removeItem("access-token")
            setRedirect(true)
        }
        if (response.data.success === 1) {
            setEnabled(true)
            setTempVerification(false)
            setDesactivatingVerification(false)
            addToast("Activada autenticación en dos pasos", { appearance: "success", autoDismiss: true})
        }else{
            addToast(response.data.message, { appearance: response.data.success === 1 ? "success" : "error", autoDismiss: true })
        }
    }

    const OnSubmitDesactivate = async (event) => {
        event.preventDefault()
        const response = await Requests.post("desactivateTwoFactorAuth", {code: desactivateVerificationCode})
        if (!!response.data.failAuth) {
            addToast(response.data.message, { appearance: "error", autoDismiss: true })
            localStorage.removeItem("user_bitwabi")
            localStorage.removeItem("access-token")
            setRedirect(true)
        }
        if (response.data.success === 1) {
            setEnabled(false)
            setTempVerification(false)
            setDesactivatingVerification(false)
            addToast("Desctivada autenticación en dos pasos", { appearance: "success", autoDismiss: true})
        }else{
            addToast(response.data.message, { appearance: response.data.success === 1 ? "success" : "error", autoDismiss: true })
        }
    }

    return (
        <div id="twoFactorAuthentication">
            {redirect && <Redirect to="signIn" />}
            <h1>Autenticación en dos pasos</h1>
            <div className="box">
                {!enabled ?
                    !tempVerification ? 
                        <button className="buttn buttn-primary" onClick={OnClick}>Activar autenticación en dos pasos</button>
                    :
                        <div>
                            <p>Escanee el siguiente código QR en su aplicación de Autenticador de Google e ingrese el código numérico que le aparezca: </p>
                            <img src={qrCode.qrcode}/>
                            <p>Tu clave para recuperar tus códigos de autenticación en dos pasos es:</p>
                            <p><b>{qrCode.key}</b></p>
                            <form onSubmit={OnSubmit}>
                                <ReactCodeInput 
                                    title="Código de verificación"
                                    placeholder={["-","-","-","-","-","-"]}
                                    id="tempVerificationCode" 
                                    onChange={(event)=>{
                                        setTempVerificationCode(event)
                                    }} 
                                    autoFocus={true}
                                    fieldWidth={50}
                                    fieldHeight={50}
                                />
                                <input style={{marginTop: "20px"}} type="submit" className="buttn buttn-primary" value="Verificar"/>
                            </form>
                        </div>
                :
                    !desactivatingVerification ?
                        <div>
                            <p>Ha activado la autenticación en dos pasos</p>
                            <button className="buttn buttn-primary" onClick={()=>{setDesactivatingVerification(true)}}>Desactivar autenticación en dos pasos</button>
                        </div>
                    :
                        <div>
                            <p>Introduzca su código de verificación de dos pasos: </p>
                            <form onSubmit={OnSubmitDesactivate}>
                                <ReactCodeInput 
                                    title="Código de verificación"
                                    placeholder={["-","-","-","-","-","-"]}
                                    id="desactivateVerificationCodeError" 
                                    onChange={(event)=>{
                                        setDesactivateVerificationCode(event)
                                    }} 
                                    autoFocus={true}
                                    fieldWidth={50}
                                    fieldHeight={50}
                                />
                                <input style={{marginTop: "20px"}} type="submit" className="buttn buttn-primary" value="Verificar"/>
                            </form>
                        </div>
                }
            </div>
        </div>
    )
}
