import React from 'react';
import { useToasts } from 'react-toast-notifications'
import Requests from "../../services/Requests"
import {fixedToMultiplier} from "../../services/Numbers"
import {dateWithNumberFormatFromTime} from "../../services/Dates"
import "./OrdersHistory.css"

export default function OrdersHistory() {
    const [orders, setOrders ] = React.useState([])
    const { addToast } = useToasts()

    React.useEffect(()=>{
        Requests.get("myOrdersHistory").then((result)=>{
            console.log("regreso de mis ordnes", result)
            if(result.data.success == 1){
                setOrders(result.data.orders)
            } else {
                addToast(result.data.message, { appearance: 'error', autoDismiss: true })
            }
        })
    }, [])
    return(
        <div id="ordersHistory">
            <h1>Historial de Ordenes</h1>
            <div className="box">
                <div className="logotype">
                    <img src="/images/logotipo.png"/>
                </div>
                <div className="text">
                    <p>Este es tu historial de ordenes cerradas:</p>
                </div> 
                <div className="ordersTable">
                    <table>
                        <thead>
                            <tr>
                                <th>Par</th>
                                <th>Balance asignado</th>
                                <th>Beneficios/Pérdidas</th>
                                <th>Benef/Pérd %</th>
                                <th>Fecha</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                orders.slice(0).reverse().map(ordr=><tr>
                                    <td>{ordr.symbol}</td>
                                    <td>{ordr.pretendedToBuy ? ordr.pretendedToBuy.toFixed(8)+" BTC" : "Sin datos"}</td>
                                    <td style={{color: ordr.resultantQuantity > 0 ? "green": "red"}}>{ordr.resultantQuantity ? ordr.resultantQuantity.toFixed(8)+" BTC" : "Sin datos"}</td>
                                    <td style={{color: ordr.resultantPercentage > 0 ? "green": "red"}}>{ordr.resultantPercentage ? ordr.resultantPercentage.toFixed(2)+" %" : "Sin datos"}</td>
                                    <td>{dateWithNumberFormatFromTime(ordr.transactTime)}</td>
                                </tr>)
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )

}