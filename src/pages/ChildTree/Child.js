import React, { useState } from "react";
const packs = {
    1: { name: " Bronce", path: "/images/bronce.png", price: 250, half: 125 },
    2: { name: " Plata", path: "/images/plata.png", price: 500, half: 250 },
    3: { name: " Oro", path: "/images/oro.png", price: 1250, half: 625 },
    4: { name: " Diamante", path: "/images/diamante.png", price: 3000, half: 1500 },
    5: { name: " Premium", path: "/images/premium.png", price: 6000, half: 3000 },
    6: { name: " Premium+", path: "/images/premiumplus.png", price: 12000, half: 6000 },
};
export default function Hijo({ data, nivel, fatherSelectedPackage }) {
    const [visible, setVisible] = useState(!nivel);
    const keys = data.childs ? Object.keys(data.childs) : null;
    const renderHijos = () => {
        if (data.childs) {
            return keys.map((index, i) => (
                <Hijo
                    key={i}
                    data={data.childs[index]}
                    nivel={nivel + 1}
                    fatherSelectedPackage={fatherSelectedPackage}
                />
            ));
        } else {
            return null;
        }
    };
    return (
        <React.Fragment>
            {!!nivel && (
                <tr>
                    <td style={{ paddingLeft: nivel * 10 + 20, textAlign: "left" }}>
                        {!!(nivel - 1) && (
                            <svg height="30" width="10">
                                <line x1="0" y1="-16" x2="0" y2="30" style={{ stroke: "#000", strokeWidth: 1 }} />
                                <line x1="0" y1="30" x2="7" y2="30" style={{ stroke: "#000", strokeWidth: 1 }} />
                            </svg>
                        )}
                        {nivel}
                        {data.childs && keys.length > 0 && (
                            <div className="toggle" onClick={(ev) => setVisible(!visible)}>
                                {!visible ? (
                                    <i className="fa fa-chevron-down" aria-hidden="true"></i>
                                ) : (
                                    <i className="fa fa-chevron-up" aria-hidden="true"></i>
                                )}
                            </div>
                        )}
                    </td>
                    <td style={{ paddingLeft: (nivel - 1) * 10 + 20, textAlign: "left" }}>
                        {data.nickName || data.name}
                        &nbsp;&nbsp;&nbsp;
                        <span className={`${data.fullAccount ? "active" : "inactive"}`}>
                            {`${data.fullAccount ? "ACTIVO" : "INACTIVO"}`}
                        </span>
                    </td>
                    <td style={{ paddingLeft: (nivel - 1) * 10 + 20, textAlign: "left" }}>
                        <img src={packs[data.selectedPackage].path} />
                        <span className="packageName">{packs[data.selectedPackage].name.toUpperCase()}</span>
                    </td>
                    <td style={{ paddingLeft: (nivel - 1) * 10 + 20, textAlign: "left" }}>
                        {nivel === 1
                            ? `€ ${
                                  0.3 *
                                  (fatherSelectedPackage < data.selectedPackage ?
                                    !!data.halfDiscount ?
                                          packs[fatherSelectedPackage].half
                                        : packs[fatherSelectedPackage].price
                                    : !!data.halfDiscount ?
                                          packs[data.selectedPackage].half
                                        : packs[data.selectedPackage].price)
                              }  (30%)`
                            : nivel <= 10
                            ? `€ ${
                                  0.02 *
                                  (fatherSelectedPackage < data.selectedPackage ?
                                    !!data.halfDiscount ?
                                          packs[fatherSelectedPackage].half
                                        : packs[fatherSelectedPackage].price
                                    : !!data.halfDiscount ?
                                          packs[data.selectedPackage].half
                                        : packs[data.selectedPackage].price)
                              }  (2%)`
                            : `€ 0  (0%)`}
                    </td>
                    <td>
                        <span className={`numeroHijos ${keys.length === 0 ? "noChildren" : ""}`}>{keys.length}</span>
                    </td>
                </tr>
            )}
            {visible && renderHijos()}
        </React.Fragment>
    );
}
