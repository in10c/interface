import React, { useContext } from "react";
import Requests from "../../services/Requests";
import Child from "./Child";
import ChildrenCounter from "../ChildrenCounter/ChildrenCounter";
import { useToasts } from "react-toast-notifications";
import "./ChildTree.css";
import Constants from "../../services/Constants";

export default function ChildTree() {
    const { addToast } = useToasts();
    const user = JSON.parse(localStorage.getItem("user_bitwabi"));
    const [fam, setFam] = React.useState(false);
    React.useEffect(() => {
        const user = JSON.parse(localStorage.getItem("user_bitwabi"));
        Requests.post("getFamily", { userID: user.id }).then((resp) => {
            console.log("la resp es", resp);
            if (!!resp.data.failAuth) {
                addToast(resp.data.message, { appearance: "error", autoDismiss: true });
                localStorage.removeItem("user_bitwabi");
                localStorage.removeItem("access-token");
            }
            setFam(resp.data.family);
        });
    }, []);
    return (
        <div id="referidos">
            <h1>Mis referidos</h1>
            <div className="box">
                <div className="avatar">
                    <img src="/images/avatar.png" />
                </div>
                <div className="userInfo">
                    {user && (
                        <p>
                            {user.name} <b>{Constants.pckgInfo(user.selectedPackage).name}</b>
                        </p>
                    )}
                </div>
                <div className="numberOfChildrenComponent">
                    <ChildrenCounter />
                </div>
                <div className="childrenList">
                    <table>
                        <thead>
                            <tr>
                                <th>NIVEL</th>
                                <th>NOMBRE</th>
                                <th>LICENCIA</th>
                                <th>GANANCIA</th>
                                <th>REFERIDOS</th>
                            </tr>
                        </thead>
                        <tbody>{fam && <Child data={fam} nivel={0} fatherSelectedPackage={fam.selectedPackage} />}</tbody>
                    </table>
                    <ul></ul>
                </div>
            </div>
        </div>
    );
}
