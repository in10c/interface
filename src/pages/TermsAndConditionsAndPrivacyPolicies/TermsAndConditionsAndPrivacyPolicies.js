import React from "react";
import LayoutVisitors from "../../layout/LayoutVisitors"
import "../LegalWarning/TermsStyles.css"

export default function TermsAndConditionsAndPrivacyPolicies()  {

    return (
        <LayoutVisitors className="terms">
            <h1>TÉRMINOS DE USO Y POLÍTICA DE PRIVACIDAD</h1>
            <pre>
                {`
En cumplimiento con el deber de información recogido en artículo 10 de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y del 
Comercio Electrónico, a continuación, se hace constar:

DATOS IDENTIFICATIVOS. 

El titular del/los dominios/s web es BITWABI OÜ, con domicilio en Harju maakond, Tallinn, Kesklinna linnaosa, Roosikrantsi tn 2-1127, 10119; NIF: 16072289;
teléfono +34722136015; correo electrónico info@bitwabi.com y registro mercantil en Estonia como BITWABI OÜ.

USUARIOS.

El acceso y/o uso de este sitio web de https://www.bitwabi.com/ atribuye la condición de USUARIO, que acepta, desde dicho acceso y/o uso, los presentes términos de uso.

USO DEL SITIO WEB.

https://www.bitwabi.com/ proporciona el acceso a artículos, informaciones y datos (en adelante, “los contenidos”) propiedad de BITWABI OÜ El USUARIO asume la 
responsabilidad del uso de la web.

El USUARIO se compromete a hacer un uso adecuado de los contenidos que BITWABI OÜ ofrece a través de su web y con carácter enunciativo, pero no limitativo, 
a no emplearlos para:

●	Incurrir en actividades ilícitas, ilegales o contrarias a la buena fe y al orden público
●	Difundir contenidos o propaganda de carácter racista, xenófobo, pornográfico-ilegal, de apología del terrorismo o atentatorio contra los derechos humanos.
●	Provocar daños en los sistemas físicos y lógicos de https://www.bitwabi.com/ de sus proveedores o de terceras personas, introducir o difundir en la red 
        virus informáticos o cualesquiera otros sistemas físicos o lógicos que sean susceptibles de provocar los daños anteriormente mencionados.
●	Intentar acceder y, en su caso, utilizar las cuentas de correo electrónico de otros usuarios y modificar o manipular sus mensajes.

BITWABI OÜ se reserva el derecho de retirar todos aquellos comentarios y aportaciones que vulneren el respeto a la dignidad de la persona, que sean discriminatorios, 
xenófobos, racistas, pornográficos, que atenten contra la juventud o la infancia, el orden o la seguridad pública o que, a su juicio, no resultaran adecuados para 
su publicación.

En cualquier caso, BITWABI OÜ no será responsable de las opiniones vertidas por los usuarios a través del blog u otras herramientas de participación que puedan 
crearse, conforme a lo previsto en la normativa de aplicación.

POLÍTICA DE PRIVACIDAD. PROTECCIÓN DE DATOS.

Finalidad de los datos recabados y CONSENTIMIENTO al tratamiento.

Conforme a lo previsto en el artículo 5 de la LOPD, se informa al USUARIO que, mediante los formularios de registro de la web, se recaban datos, que se almacenan 
en un fichero, con la exclusiva finalidad de envío de comunicaciones electrónicas, tales como: boletines (newsletters), nuevas entradas (posts), así como otras 
comunicaciones que BITWABI OÜ entiende interesantes para sus USUARIOS. Los campos marcados como de cumplimentación obligatoria, son imprescindibles para realizar 
la finalidad expresada.

Únicamente el titular tendrá acceso a sus datos, y bajo ningún concepto, estos datos serán cedidos, compartidos, transferidos, ni vendidos a ningún tercero.
La aceptación de la política de privacidad,  mediante el procedimiento establecido de doble opt-in,  se entenderá a todos los efectos como la prestación de 
CONSENTIMIENTO EXPRESO E INEQUÍVOCO – del artículo 6 de la LOPD– del USUARIO al tratamiento de los datos de carácter personal en los términos que se exponen en el 
presente documento, así como a la transferencia internacional de datos que se produce, exclusivamente debido a la ubicación física de las instalaciones de los 
proveedores de servicios y encargados del tratamiento de datos.

Cumplimiento de la normativa de aplicación.

BITWABI OÜ cumple con las directrices de la Ley Orgánica 15/1999 de 13 de diciembre de Protección de Datos de Carácter Personal, el Real Decreto 1720/2007 de 
21 de diciembre por el que se aprueba el Reglamento de desarrollo de dicha Ley Orgánica y demás normativa vigente y de aplicación en cada momento, velando por 
garantizar un correcto uso y tratamiento de los datos personales del usuario. 

Asimismo, BITWABI OÜ informa que da cumplimiento a la Ley 34/2002 de 11 de julio, de Servicios de la Sociedad de la Información y el Comercio Electrónico y 
le solicitará su consentimiento al USUARIO para el tratamiento de su correo electrónico con fines comerciales en cada momento.

En cumplimiento de lo establecido en la LOPD, le informamos que los datos suministrados, así como aquellos datos derivados de su navegación, podrán ser almacenados 
en los ficheros de BITWABI OÜ y tratados para la finalidad de atender su solicitud y el mantenimiento de la relación que se establezca en los formularios que suscriba.

Adicionalmente, el USUARIO consiente el tratamiento de sus datos con la finalidad de informarles, por cualquier medio, incluido el correo electrónico, de productos 
y servicios de BITWABI OÜ.


En caso de no autorizar el tratamiento de sus datos con la finalidad señalada anteriormente, el USUARIO podrá ejercer su derecho de oposición al tratamiento de sus 
datos en los términos y condiciones previstos más adelante en el apartado “Ejercicio de Derechos ARCO”.

Medidas de Seguridad.

BITWABI OÜ le informa que tiene implantadas las medidas de seguridad de índole técnica y organizativas necesarias para garantizar la seguridad de sus datos de 
carácter personal y evitar su alteración, pérdida y tratamiento y/o acceso no autorizado, habida cuenta del estado de la tecnología, la naturaleza de los datos 
almacenados y los riesgos a que están expuestos, ya provengan de la acción humana o del medio físico o natural. Todo ello de conformidad con lo previsto en el 
Art. 9 de la LOPD y el Título VIII del RLOPD.

Asimismo, BITWABI OÜ ha establecido medidas adicionales en orden a reforzar la confidencialidad e integridad de la información en su organización. 
Manteniendo continuamente la supervisión, control y evaluación de los procesos para asegurar el respeto a la privacidad de los datos.

Ejercicio de Derechos ARCO: Acceso, Rectificación, Cancelación y Oposición.

Aquellas personas físicas que hayan facilitado sus datos a través de la web https://www.bitwabi.com/, podrán dirigirse al titular de la misma con el fin de poder 
ejercitar gratuitamente sus derechos de acceso, rectificación, cancelación y oposición respecto de los datos incorporados en sus ficheros.
El interesado podrá ejercitar sus derechos mediante comunicación por escrito dirigida a BITWABI OÜ con la referencia “Protección de datos/bitwabi”, especificando 
sus datos, acreditando su identidad y los motivos de su solicitud en la siguiente dirección:

También podrá ejercitar los derechos ARCO, a través del correo electrónico: info@bitwabi.com

Links.

Como un servicio a nuestros visitantes, nuestro sitio web puede incluir hipervínculos a otros sitios que no son operados o controlados por BITWABI OÜ Por ello 
BITWABI OÜ no garantiza, ni se hace responsable de la licitud, fiabilidad, utilidad, veracidad y actualidad de los contenidos de tales sitios web o de sus prácticas 
de privacidad. Por favor, antes de proporcionar su información personal a estos sitios web ajenos a BITWABI OÜ tenga en cuenta que sus prácticas de privacidad 
pueden diferir de las nuestras.

Política de “Cookies”.

Una cookie es un archivo de información que el servidor de este sitio web envía al dispositivo (ordenador, smartphone, tablet, etc.) de quien accede a la página 
para almacenar y recuperar información sobre la navegación que se efectúa desde dicho equipo.
BITWABI OÜ utiliza diversos tipos de cookies (técnicas, analíticas y sociales) únicamente con la finalidad de mejorar la navegación del usuario en el sitio web, 
sin ningún tipo de objeto publicitario o similar, para el análisis y elaboración de estadísticas de la navegación que el USUARIO realiza en el sitio web, así como 
para compartir los contenidos en redes sociales (Google+, Twitter, Linkedin, Disqus)

BITWABI OÜ utiliza en este sitio web las cookies que se detallan a continuación:

Cookies técnicas: Son aquéllas que permiten al USUARIO la navegación a través de la página web y la utilización de las diferentes opciones o servicios que en ella 
existen como, por ejemplo, controlar el tráfico y la comunicación de datos, identificar la sesión, acceder a partes de acceso restringido, recordar los elementos 
que integran un pedido, realizar el proceso de compra de un pedido, realizar la solicitud de inscripción o participación en un evento, utilizar elementos de seguridad 
durante la navegación, almacenar contenidos para la difusión de vídeos o sonido o compartir contenidos a través de redes sociales.

Cookies de Google Analytics: Son cookies de terceros (Google Inc.) de análisis que permiten el seguimiento y análisis del comportamiento de los USUARIOS de los sitios 
web a los que están vinculadas. La información recogida mediante este tipo de cookies se utiliza en la medición de la actividad de los sitios web, aplicación o 
plataforma y para la elaboración de perfiles de navegación de los USUARIOS de dichos sitios, aplicaciones y plataformas, con el fin de introducir mejoras en función 
del análisis de los datos de uso que hacen los USUARIOS del servicio.

Google Analytics, almacena las cookies en servidores ubicados en Estados Unidos y se compromete a no compartirla con terceros, excepto en los casos en los que sea 
necesario para el funcionamiento del sistema o cuando la ley obligue a tal efecto. Según Google no guarda la dirección IP del USUARIO.
Más información sobre Google Analytics en los siguientes enlaces:
www.google.com/analytics/ y http://www.google.com/intl/es/policies/privacy/

Si deseas información sobre el uso que Google da a las cookies adjuntamos este otro enlace: https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage?hl=es&csw=1).
Cookies sociales: Google+, Facebook, YouTube, Twitter: Cookies de terceros, es decir, redes sociales externas y de terceros, cuya temporalidad y finalidad depende 
de cada red social.
El usuario podrá -en cualquier momento- elegir qué cookies quiere que funcionen en este sitio web mediante:
– La configuración del navegador; por ejemplo:
Chrome, desde: http://support.google.com/chrome/bin/answer.py?hl=es&answer=95647
Explorer, desde: http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9
Firefox, desde: http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we
Safari, desde: http://support.apple.com/kb/ph5042
Opera, desde: http://help.opera.com/Windows/11.50/es-ES/cookies.html

– Existen herramientas de terceros, disponibles online, que permiten a los usuarios detectar las cookies en cada sitio web que visita y gestionar su desactivación.
Ni esta web, ni sus representantes legales se hacen responsables ni del contenido ni de la veracidad de las políticas de privacidad que puedan tener los terceros 
mencionados en esta política de cookies.
Los navegadores web son las herramientas encargadas de almacenar las cookies y desde esos navegadores debes efectuar tu derecho a eliminación o desactivación de las 
mismas. Ni esta web ni sus representantes legales pueden garantizar la correcta o incorrecta manipulación de las cookies por parte de los mencionados navegadores.
En algunos casos es necesario instalar cookies para que el navegador no olvide tu decisión de no aceptación de las mismas.
La aceptación de la presente política de privacidad implica que el usuario ha sido informado de una forma clara y completa sobre el uso de dispositivos de almacenamiento 
y recuperación de datos (cookies) así como que https://www.bitwabi.com/ dispone del consentimiento del usuario para el uso de las mismas tal y como establece el artículo 
22 de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y Comercio Electrónico (LSSI-CE).
Para cualquier duda o consulta acerca de esta política de cookies no dudes en comunicarte con nosotros a través de la dirección de correo electrónico info@bitwabi.com 

Menores de edad.

La web de https://www.bitwabi.com/ no se dirige a menores de edad. El titular de la web declina cualquier responsabilidad por el incumplimiento de este requisito.

Modificación de la Política de Privacidad.

BITWABI OÜ se reserva el derecho a modificar su Política de Privacidad, de acuerdo a su propio criterio, motivado por un cambio legislativo, jurisprudencial o 
doctrinal de la Agencia Española de Protección de Datos.

Cualquier modificación de la Política de Privacidad será publicada al menos diez días antes de su efectiva aplicación. El uso de la Web después de dichos cambios, 
implicará la aceptación de los mismos.

Responsable del fichero, y encargados del tratamiento.

El responsable del fichero de datos es BITWABI OÜ
Como encargados de tratamiento ajenos a al citado responsable:

BITWABI OÜ  ha contratado los servicios de hosting a 1&1 IONOS España S.L.U.

Los servicios de suscripción por correo electrónico y envío de newsletters a 1&1 IONOS España S.L.U.

Se ha exigido a dichos encargados de tratamiento el cumplimiento de las disposiciones normativas de aplicación en materia de protección de datos, en el momento de su contratación.

PROPIEDAD INTELECTUAL E INDUSTRIAL. 

www.eois2convocatoria por sí o como cesionario, es titular de todos los derechos de propiedad intelectual e industrial de su página web, así como de los elementos contenidos 
en la misma (a título enunciativo, imágenes, sonido, audio, vídeo, software o textos; marcas o logotipos, combinaciones de colores, estructura y diseño, selección de materiales 
usados, programas de ordenador necesarios para su funcionamiento, acceso y uso, etc.), titularidad de BITWABI OÜ o bien de sus licenciantes. Todos los derechos reservados.
Cualquier uso no autorizado previamente por BITWABI OÜ será considerado un incumplimiento grave de los derechos de propiedad intelectual o industrial del autor.

Quedan expresamente prohibidas la reproducción, la distribución y la comunicación pública, incluida su modalidad de puesta a disposición, de la totalidad o parte de los contenidos 
de esta página web, con fines comerciales, en cualquier soporte y por cualquier medio técnico, sin la autorización de BITWABI OÜ.
El USUARIO se compromete a respetar los derechos de Propiedad Intelectual e Industrial titularidad de BITWABI OÜ podrá visualizar los elementos de la web e incluso imprimirlos, 
copiarlos y almacenarlos en el disco duro de su ordenador o en cualquier otro soporte físico siempre y cuando sea, única y exclusivamente, para su uso personal y privado. 
El USUARIO deberá abstenerse de suprimir, alterar, eludir o manipular cualquier dispositivo de protección o sistema de seguridad que estuviera instalado en las páginas de BITWABI OÜ.

EXCLUSIÓN DE GARANTÍAS Y RESPONSABILIDAD.

BITWABI no se hace responsable, en ningún caso, de los daños y perjuicios de cualquier naturaleza que pudieran ocasionar, a título enunciativo: por errores u omisiones en los contenidos, 
por falta de disponibilidad del sitio web – el cual realizará paradas periódicas por mantenimientos técnicos – así como por la transmisión de virus o programas maliciosos o lesivos en 
los contenidos, a pesar de haber adoptado todas las medidas tecnológicas necesarias para evitarlo.

MODIFICACIONES. 

BITWABI se reserva el derecho de efectuar sin previo aviso las modificaciones que considere oportunas en su web, pudiendo cambiar, suprimir o añadir tanto los contenidos y servicios que 
se presten a través de la misma como la forma en la que éstos aparezcan presentados o localizados en su web.

POLÍTICA DE ENLACES.

Las personas o entidades que pretendan realizar o realicen un hiperenlace desde una página web de otro portal de Internet a la web de BITWABI deberá someterse a las siguientes condiciones:
–	No se permite la reproducción total o parcial de ninguno de los servicios ni contenidos del sitio web sin la previa autorización expresa de BITWABI
–	No se establecerán deep-links ni enlaces IMG o de imagen, ni frames con la web de https://www.bitwabi.com/ sin su previa autorización expresa.
–	No se establecerá ninguna manifestación falsa, inexacta o incorrecta sobre la web de https://www.bitwabi.com/ ni sobre los servicios o contenidos de la misma. Salvo aquellos signos que 
    formen parte del hipervínculo, la página web en la que se establezca no contendrá ninguna marca, nombre comercial, rótulo de establecimiento, denominación, logotipo, eslogan u otros signos 
    distintivos pertenecientes a https://www.bitwabi.com/ salvo autorización expresa de éste.
–	El establecimiento del hipervínculo no implicará la existencia de relaciones entre BITWABI y el titular de la página web o del portal desde el cual se realice, ni el conocimiento y 
    aceptación de BITWABI de los servicios y contenidos ofrecidos en dicha página web o portal.
–	BITWABI no será responsable de los contenidos o servicios puestos a disposición del público en la página web o portal desde el cual se realice el hipervínculo, ni de las informaciones 
y manifestaciones incluidas en los mismos.

El sitio web https://www.bitwabi.com/ puede poner a disposición del usuario conexiones y enlaces a otros sitios web gestionados y controlados por terceros. 
Dichos enlaces tienen como exclusiva función, la de facilitar a los usuarios la búsqueda de información, contenidos y servicios en Internet, sin que en ningún caso pueda considerarse una sugerencia, 
recomendación o invitación para la visita de los mismos.

BITWABI no comercializa, ni dirige, ni controla previamente, ni hace propios los contenidos, servicios, informaciones y manifestaciones disponibles en dichos sitios web.

BITWABI no asume ningún tipo de responsabilidad, ni siquiera de forma indirecta o subsidiaria, por los daños y perjuicios de toda clase que pudieran derivarse del acceso, mantenimiento, uso, 
calidad, licitud, fiabilidad y utilidad de los contenidos, informaciones, comunicaciones, opiniones, manifestaciones, productos y servicios existentes u ofrecidos en los sitios web no gestionados 
por BITWABI y que resulten accesibles a través de https://www.bitwabi.com/

DERECHO DE EXCLUSIÓN. 

BITWABI se reserva el derecho a denegar o retirar el acceso al portal y/o los servicios ofrecidos sin necesidad de preaviso, a instancia propia o de un tercero, a aquellos usuarios que 
incumplan las presentes Condiciones Generales de Uso.

GENERALIDADES. 

BITWABI perseguirá el incumplimiento de las presentes condiciones, así como cualquier utilización indebida de su web ejerciendo todas las acciones civiles y penales que le puedan 
corresponder en derecho.

MODIFICACIÓN DE LAS PRESENTES CONDICIONES Y DURACIÓN

BITWABI podrá modificar en cualquier momento las condiciones aquí determinadas, siendo debidamente publicadas como aquí aparecen. La vigencia de las citadas condiciones irá en función 
de su exposición y estarán vigentes hasta que sean modificadas por otras debidamente publicadas.

LEGISLACIÓN APLICABLE Y JURISDICCIÓN 

La relación entre BITWABI y el USUARIO se regirá por la normativa española vigente y cualquier controversia se someterá a los Juzgados y tribunales de la ciudad de Marbella, Málaga, España, 
salvo que la Ley aplicable disponga otra cosa.




                `}
            </pre>
        </LayoutVisitors>
    )
}
