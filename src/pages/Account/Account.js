import React from "react";
import { useParams } from 'react-router-dom'
import { useToasts } from 'react-toast-notifications'
import Requests from "../../services/Requests"
import "./Account.css"

export default function Account() {
    const { userID } = useParams()
    const { addToast } = useToasts()
    const [info, setInfo] = React.useState(false)
    const [searchPair, setSearchPair] = React.useState("")
    const [visible, setVisible] = React.useState({
        balances: true
    })
    const titlesAppOpenOrders = ["symbol", "sl", "enterprice", "gain", "side", "signalID", "globalStatus", "pretendedToBuy", "buy", "buyOrderID", "buyOrderQuantity","buyOrderStatus","buyOrderExecutionTime","buyOrderPrice","buyedQuantity","spendedQuantity","sellOrderID","sellOrderQuantity","sellOrderPrice","sellOrderExecution","sellOrderStatus","sellOrderType","selledQuantity","receivedQuantity","resultantQuantity","resultantPercentage"]
    const [skip, setSkip] = React.useState(0)
    const [balanceTitles, setBalanT] = React.useState([])
    const [oOrdersTitles, setOOrdersT] = React.useState([])
    const [appOpenOrders, setAppOpenOrders] = React.useState([])
    const [newVPS, setNewVPS] = React.useState(1)
    const titlesNoVisibles = ["orderId", "orderListId", "clientOrderId", "isWorking", "timeInForce", "icebergQty", "updateTime"]
    React.useEffect(()=>{
        console.log("el user id es ", userID)
        Requests.postTradingServer("getCriptoAccount", { account: userID}).then((result)=>{
            console.log("de data cuenta ", result)
            setInfo(result.data)
            if(result.data.nonZeroBalances && result.data.nonZeroBalances.length > 0){
                setBalanT(["asset", "free", "locked", "protected"])
            }
            if(result.data.openOrders && result.data.openOrders.length > 0){
                let auxtitles = Object.keys(result.data.openOrders[0]).filter(val=>!titlesNoVisibles.includes(val))
                setOOrdersT(auxtitles)
            }
            if(result.data.appOpenOrders && result.data.appOpenOrders.length > 0){
                setAppOpenOrders(result.data.appOpenOrders)
            }
        })
    }, [])
    const cancelOrder = (item) => {
        let payload = { account: userID, origClientOrderId: item.clientOrderId, symbol: item.symbol }
        payload.skip = skip
        Requests.postTradingServer("deleteExchgOrder", payload).then((result)=>{
            console.log("de data cuenta ", result)
            if(result.data.success === 1){
                addToast("Se ha eliminado correctamente la orden.", { appearance: 'success', autoDismiss: true })
            } else {
                addToast(result.data.message, { appearance: 'error', autoDismiss: true })
            }
        })
    }
    const sellQuantity = async (quantity, asset) => {
        let payload = {quantity, asset, account: userID}
        payload.skip = skip
        const result = await Requests.postTradingServer("sellQuantity", payload)
        console.log("de data cuenta ", result)
        if(result.data.success === 1){
            addToast("Se ha eliminado correctamente la orden.", { appearance: 'success', autoDismiss: true })
        } else {
            addToast(result.data.message, { appearance: 'error', autoDismiss: true })
        }
    }
    const refreshOrders = async(ev) => {
        const result = await Requests.postTradingServer("refreshSocketOrders", {account: userID})
        console.log("de data cuenta ", result)
        if(result.data.success === 1){
            addToast("Se ha enviado correctamente la orden de refresco al socket.", { appearance: 'success', autoDismiss: true })
        } 
    }
    const reBornVPS = async (ev)=>{
        const result = await Requests.postTradingServer("reBornVPS", {account: userID, clusterNumber: newVPS})
        console.log("de revivir vps ", result)
        if(result.data.success === 1){
            addToast("Se ha generado correctamente el VPS para el cliente.", { appearance: 'success', autoDismiss: true })
        } 
    } 
    const deleteVPS = async (ev) => {
        if(window.confirm("¿Está seguro que desea matar el VPS del cliente?")){
            const result = await Requests.postTradingServer("deleteVPS", {account: userID})
            console.log("de deleteVPS ", result)
            if(result.data.success === 1){
                addToast("Se ha eliminado el VPS.", { appearance: 'success', autoDismiss: true })
            } else {
                addToast(result.data.message, { appearance: 'error', autoDismiss: true })
            }
        }
    }
    const deleteSlot = async (ev) => {
        if(window.confirm("¿Está seguro que desea matar el VPS del cliente?")){
            const result = await Requests.postTradingServer("deleteSlot", {account: userID})
            console.log("de delete slot ", result)
            if(result.data.success === 1){
                addToast("Se ha eliminado el SLOT.", { appearance: 'success', autoDismiss: true })
            } else {
                addToast(result.data.message, { appearance: 'error', autoDismiss: true })
            }
        }
    }
    const regenerateSellOrder = async (sellable, asset) => {
        if(window.confirm("¿Está seguro que desea regenerar la orden de venta del cliente?")){
            const result = await Requests.postTradingServer("regenerateSellOrder", {account: userID, sellable, asset})
            console.log("de regenerateSellOrder ", result)
            // if(result.data.success === 1){
            //     addToast("Se ha generado correctamente el VPS para el cliente.", { appearance: 'success', autoDismiss: true })
            // }
        }
    }
    const recoverySocketID = async () => {
        if(window.confirm("¿Está seguro que desea regenerar el socket del cliente?")){
            const result = await Requests.postTradingServer("recoverySocketID", {account: userID})
            console.log("de recoverySocketID ", result)
            if(result.data.success === 1){
                addToast("Se ha generado correctamente el SOCKET para el cliente.", { appearance: 'success', autoDismiss: true })
            } else {
                addToast(result.data.message, { appearance: 'error', autoDismiss: true })
            }
        }
    }
    const sellDust = async () => {
        const result = await Requests.postTradingServer("sellDust", {account: userID})
            console.log("de recoverySocketID ", result)
            if(result.data.success === 1){
                addToast("Se vendió el polvo para el cliente.", { appearance: 'success', autoDismiss: true })
            } else {
                addToast(result.data.message, { appearance: 'error', autoDismiss: true })
            }
    }
    return (
        <div>
            <h1>Control de cuenta de usuario</h1>
            <div className="box">
                <h2>Usuario</h2>
                <p>
                    Name: {info.user && info.user.name}<br/>
                    Email: {info.user && info.user.email}
                </p>
                <h2>Detalles del VPS</h2>
                <p>
                    {!info.slot && <div>VPS no encendido <br/>
                    <input type="number" min="1" value={newVPS} onChange={ev=>setNewVPS(ev.target.value)} />
                    <button onClick={reBornVPS} className="buttn buttn-default">Revivir VPS</button></div>}
                    {info.slot && <React.Fragment>
                        Encendido<br/>
                        Cluster #: {info.slot.clusterNumber}<br/>
                        Socket ID: {info.slot.socketID}<br/>
                        {info.slot.reBorned && "Renacido"} <br/>
                        {info.isSocketConnected ? "Socket conectado" : "Socket no conectado"} <br/>
                        <div className="buttonsOrdsr">
                            <button onClick={refreshOrders} className="buttn buttn-default">Refrescar ordenes</button> <br/>
                            <button onClick={recoverySocketID} className="buttn buttn-default">Recuperar SocketID</button>
                            <button onClick={sellDust} className="buttn buttn-default">Vender polvo</button>
                            <button onClick={deleteVPS} className="buttn" style={{backgroundColor:"red", color: "white"}}>Borrar VPS (sin venta mercado, socket)</button>
                            <button onClick={deleteSlot} className="buttn" style={{backgroundColor:"red", color: "white"}}>Borrar SLOT</button>
                        </div>
                    </React.Fragment>}
                </p>
                <h2>Balances <button onClick={ev=>setVisible(vis=> ({...vis, balances: !vis.balances}))}>{visible.balances ? "Ocultar" : "Mostrar"}</button></h2>
                { balanceTitles.length > 0 && <p> 
                    
                    <table className="balancesTable" style={{display: visible.balances ? "block" :"none"}}>
                        <thead>
                            <tr>
                                {balanceTitles.map(title=><th>{title}</th>)}
                                <th>Vendible</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {info.nonZeroBalances.map(val=>{
                                let sellable = val.free - (val.protected ? val.protected : 0)
                                return <tr>
                                    {balanceTitles.map(title=><td>{val[title]}</td>)}
                                    <td>{ val.asset != "BTC" && sellable }</td>
                                    <td>
                                        { val.asset != "BTC" && sellable > 0 && <React.Fragment>
                                            <button onClick={ev=>sellQuantity(sellable, val.asset)}>Vender cantidad vendible</button><br/><br/>
                                            <button onClick={ev=>regenerateSellOrder(sellable, val.asset)}>Re-generar orden de venta</button>
                                        </React.Fragment>}
                                    </td>
                                </tr>
                            })}
                        </tbody>
                    </table>
                </p>}
                <h2>Ordenes abiertas (exchange)</h2>
                { oOrdersTitles.length > 0 && <p>
                    <table>
                        <thead>
                            <tr>
                                {oOrdersTitles.map(title=><th>{title}</th>)}
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {info.openOrders.map(val=><tr>
                                {oOrdersTitles.map(title=><td>{val[title]}</td>)}
                                <td>
                                    <button onClick={ev=>cancelOrder(val)}>Cancelar orden</button>
                                </td>
                            </tr>)}
                        </tbody>
                    </table>
                </p>}
                <h2>Ordenes abiertas (bitwabi)</h2>
                { appOpenOrders.length > 0 && <p>
                    <table>
                        <thead>
                            <tr>
                                {titlesAppOpenOrders.map(val=><th>{val}</th>)}
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {appOpenOrders.map(val=><tr>
                                {titlesAppOpenOrders.map(key=><td>{ typeof val[key] != "object" ? val[key] : "ND"}</td>)}
                                <td>
                                    <button onClick={ev=>cancelOrder(val)}>Cancelar orden</button>
                                </td>
                            </tr>)}
                        </tbody>
                    </table>
                </p>}
                <h2>Buscar transacciones</h2>
                <input type="text" value />
            </div>
        </div>
    );
}
