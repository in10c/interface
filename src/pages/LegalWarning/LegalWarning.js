import React from "react";
import LayoutVisitors from "../../layout/LayoutVisitors"
import "../LegalWarning/TermsStyles.css"

export default function LegalWarning()  {

    return (
        <LayoutVisitors className="terms">
            <h1>Aviso legal</h1>
            <pre>
                {`
Datos del propietario:
Conforme a lo establecido en la ley de Servicios de la Sociedad de la Información (LSSI), se informa a los usuarios de https://www.bitwabi.com/ (en adelante, LA WEB) 
que estos son los datos del propietario de este sitio web:

Nombre: BITWABI OÜ
Dirección: Harju maakond, Tallinn, Kesklinna linnaosa, Roosikrantsi tn 2-1127, 10119
NIF: 16072289
Teléfono de Contacto: 34 722 13 60 15
E-mail de Contacto: info@bitwabi.com
Registro Mercantil: BITWABI OÜ está inscrita en el Registro Mercantil de Estonia, eBusiness Registry

El acceso a LA WEB, propiedad de BITWABI OÜ y el uso de los servicios que en este sitio web se ofrecen, implica la aceptación de las siguientes condiciones generales:

Disponibilidad y actualizaciones:
BITWABI se reserva el derecho a interrumpir en cualquier momento y sin previo aviso el acceso a LA WEB, así como a modificar o eliminar la configuración del sitio, 
los contenidos que en él se muestran o la prestación de cualquiera o de todos los servicios que se prestan a través del mismo, ya sea por motivos técnicos, de seguridad, 
de mantenimiento o por cualquier otra causa. 

Dicha interrupción, edición o eliminación podrá tener carácter temporal o definitivo, sin que de ello se derive ninguna responsabilidad para BITWABI OÜ.

Uso del sitio web, sus contenidos y servicios:
El acceso a los contenidos y servicios que se ofrecen en LA WEB, y la utilización de los mismos por parte de los usuarios, se lleva a cabo por su propia cuenta y riesgo.
BITWABI OÜ no se responsabiliza de posibles daños o perjuicios que puedan sufrir los usuarios, ni de posibles problemas técnicos o fallos en los equipos informáticos de 
los mismos, que se produzcan debido a:

●	Un mal uso de los contenidos o servicios de LA WEB por parte de los usuarios.
●	Errores, defectos u omisiones en la información facilitada cuando proceda de fuentes ajenas a BITWABI OÜ.
●	Intromisiones ilegítimas fuera del control de BITWABI OÜ por parte de terceras personas.

Enlaces:
BITWABI OÜ no se hace responsable de aquellos otros datos, sitios web u archivos a los que sea posible acceder a través de enlaces (links) disponibles entre los contenidos 
de LA WEB, dado que dichas páginas o archivos objeto de enlace son responsabilidad de sus respectivos titulares. 
BITWABI OÜ, por tanto, ni aprueba, ni hace suyos los productos, servicios, contenidos, información, datos, archivos y cualquier clase de material existente en tales 
páginas web o archivos y no controla ni, de conformidad con lo dispuesto en la LSSICE, se hace responsable de la calidad, licitud, fiabilidad y utilidad de la información,
 contenidos, datos y servicios existentes en los sitios enlazados y que son ajenos a LA WEB. En el caso de que un órgano competente declare la ilicitud de los datos, 
 ordenando su retirada o que se imposibilite el acceso a los mismos, o se hubiera declarado la existencia de la lesión, los enlaces que se indiquen serían inmediatamente retirados, 
 en cuanto así se le notifique expresamente a BITWABI OÜ.

Logos:
Los propietarios de los logos que aparecen en LA WEB han autorizado a BITWABI OÜ a hacer uso de ellos, junto con el nombre y posibles opiniones que puedan tener sobre sus servicios. 
Igualmente, BITWABI OÜ se compromete a no utilizar el nombre y/o logos para finalidad distinta de la indicada y a respetar todos los derechos de propiedad industrial e intelectual 
que el propietario tenga sobre los mismos. Dicha autorización quedará sin efecto desde el momento en que el propietario lo notifique.

Comentarios y opiniones:
BITWABI OÜ no se hace responsable de los comentarios ni de las opiniones que puedan hacer los usuarios en los contenidos de la WEB.


                `}
            </pre>
        </LayoutVisitors>
    )
}
