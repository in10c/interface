import React, { useEffect, useState, useContext } from "react"
import Requests from "../../services/Requests"
import { LayoutContext } from "../../layout/LayoutContext/LayoutContext"
import "./TwoFactorAuthAlert.css"

export default function TwoFactorAuthAlert() {

    const { modalVisible, setModalVisible } = useContext(LayoutContext)
    const [active, setActive] = useState(false)

    useEffect(() => {
        async function getInfo() {
            const response = await Requests.get("verifyTwoFactorActive");
            if (!!response.data.failAuth) {
                localStorage.removeItem("user_bitwabi");
                localStorage.removeItem("access-token");
            } else setActive(response.data.result);
        }
        getInfo();
    }, [modalVisible]);

    return (
        <div className={`twoFactorAuthAlert ${!active ? "inactive" : ""}`} onClick={() => {setModalVisible("twoFactorAuthentication")}}>
            <span className="logo">
                {!active ? 
                    <svg xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><path d="M11.492 21.771c.294.157.663.157.957-.001c8.012-4.304 8.581-12.713 8.574-15.104a.988.988 0 0 0-.596-.903l-8.051-3.565a1.005 1.005 0 0 0-.813.001L3.57 5.765a.988.988 0 0 0-.592.891c-.034 2.379.445 10.806 8.514 15.115zM8.293 9.707l1.414-1.414L12 10.586l2.293-2.293l1.414 1.414L13.414 12l2.293 2.293l-1.414 1.414L12 13.414l-2.293 2.293l-1.414-1.414L10.586 12L8.293 9.707z" fill="#FF4600"/></svg>
                :
                    <svg xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16"><g fill="#4AD991"><path fill-rule="evenodd" d="M8 .5c-.662 0-1.77.249-2.813.525a61.11 61.11 0 0 0-2.772.815a1.454 1.454 0 0 0-1.003 1.184c-.573 4.197.756 7.307 2.368 9.365a11.192 11.192 0 0 0 2.417 2.3c.371.256.715.451 1.007.586c.27.124.558.225.796.225s.527-.101.796-.225c.292-.135.636-.33 1.007-.586a11.191 11.191 0 0 0 2.418-2.3c1.611-2.058 2.94-5.168 2.367-9.365a1.454 1.454 0 0 0-1.003-1.184a61.09 61.09 0 0 0-2.772-.815C9.77.749 8.663.5 8 .5zm2.854 6.354a.5.5 0 0 0-.708-.708L7.5 8.793L6.354 7.646a.5.5 0 1 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3z"/></g></svg>
                }
            </span>
            <span className="text">{!active ? <span><b>ACTIVAR</b> 2 FACTOR</span> : <span>2FACTOR <b>ACTIVADO</b></span>}</span>
        </div>
    );
}
