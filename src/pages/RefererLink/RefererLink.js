import React, {useState} from 'react';
import {  useToasts } from 'react-toast-notifications'
import Constants from "../../services/Constants"
import "./RefererLink.css"

export default function RefererLink({ match }) {
    const { addToast } = useToasts()
    const user = JSON.parse(localStorage.getItem("user_bitwabi"))
    console.log("el user", user, Constants)
    const link = Constants.appURL()+"/reservation/"+user.id
    const copiar = ()=>{
        navigator.clipboard.writeText(link).then(function() {
            addToast('El enlace se ha copiado correctamente al portapapeles.', { appearance: 'success', autoDismiss: true })
        })
    }
    return(
        <div id="registro">
            <h1>Enlace de referido</h1>
            <div className="box">
                <div className="logotype">
                    <img src="/images/logotipo.png"/>
                </div>
                <div className="text">
                    <p>Utiliza el siguiente enlace para poder registrar referidos:</p>
                </div>
                <div className="link">
                    <a href={link} target="_blank">{link}</a><i className="fa fa-clipboard" onClick={copiar}></i>
                </div>
            </div>
        </div>
    )

}