import React, {useState} from "react";
import { useToasts } from 'react-toast-notifications'
import * as md5 from "md5";
import * as GlobalApp from '../../services/GlobalApp'
import Requests from "../../services/Requests"
import LayoutVisitors from "../../layout/LayoutVisitors"
import "./FirstConfig.css"

export default function FirstConfig()  {
    const [inputs, setInputs] = useState({});
    const [typeInput, setType] = useState("password");
    const [sended, setSended] = useState(false);
    const { addToast } = useToasts()
    const handleInputChange = (event) => {
        event.persist();
        setInputs(inputs => ({...inputs, [event.target.name]: event.target.value}));
    }
    const handleSubmit = (event) => {
        event.preventDefault()
        if(!inputs.email || !inputs.password || !inputs.name){
            addToast("Todos los campos son requeridos.", { appearance: 'error', autoDismiss: true })
            return false
        }
        let aux = JSON.parse(JSON.stringify(inputs))
        aux.password = md5(aux.password)
        Requests.post("doSignUp", aux).then((result)=>{
            console.log("regreso de enviar", result)
            if(result.data.success == 1){
                setSended(true)
            }
        })
    }
    return (
        <LayoutVisitors>
            {!sended && <form onSubmit={handleSubmit} autoComplete={false}>
                <h1>Bitwabi</h1>
                <p className="desc">¡Hola!, por favor registra tu nueva cuenta.</p>
                <div>
                    <div>
                        <input type="text" placeholder="Nombre" className="formControl" required name="name"
                            value={inputs.name} onChange={handleInputChange}/>
                    </div>
                    <div>
                        <input type="email" placeholder="Correo electrónico" className="formControl" required name="email"
                            value={inputs.email} onChange={handleInputChange}/>
                    </div>
                    <div className="passInpt link">
                        <input type={typeInput} placeholder="Contraseña" className="formControl" required name="password"
                            value={inputs.password} onChange={handleInputChange}/>
                        <div onClick={ev=>setType( typeInput === "password" ? "text" : "password")}><i class="fa fa-eye" aria-hidden="true"></i></div>
                    </div>
                </div>
                <div style={{marginTop:20}}>
                    <button className="buttn buttn-primary">Registrar cuenta</button>
                </div>
            </form>}
            {sended && <div>
                <h1>Cuenta registrada</h1>
                <p className="desc">Gracias por registrar tu cuenta, por favor accede a tu correo y da clic en el enlace de confirmación
                para continuar con tu registro.</p>
            </div>}
        </LayoutVisitors>
    )
}
