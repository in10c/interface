import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { useToasts } from "react-toast-notifications";
import Constants from "../../services/Constants";
import Requests from "../../services/Requests";

//////////////////////////////////////
///////////// Deprecated /////////////
//////////////////////////////////////

export default function Upgrade() {
    const { addToast } = useToasts();
    const user = JSON.parse(localStorage.getItem("user_bitwabi"));
    const [redirect, setRedirect] = useState(user.selectedPackage == Object.entries(Constants.packgs).length || !user);
    const [selectedPackage, setSelectedPackage] = useState(undefined);
    const [halfDiscount, setHalfDiscount] = useState(undefined);

    useEffect(() => {
        async function getInfo() {
            const response = await Requests.get("verifyHalfDiscount");
            if (!!response.data.failAuth) {
                addToast(response.data.message, { appearance: "error", autoDismiss: true });
                localStorage.removeItem("user_bitwabi");
                localStorage.removeItem("access-token");
            }
            setHalfDiscount(response.data.halfDiscount);
        }
        getInfo();
    }, []);

    const OnClick = (event) => {
        const { id } = event.target;
        setSelectedPackage(id);
    };

    const OnSubmit = async (event) => {
        event.preventDefault();
        if (!selectedPackage) {
            addToast("Por favor selecciona un paquete", { appearance: "error" });
            return;
        }

        const response = await Requests.post("upgrade", { selectedPackage });
        if (!!response.data.failAuth) {
            localStorage.removeItem("user_bitwabi");
            localStorage.removeItem("access-token");
        }
        addToast(response.data.message, { appearance: response.data.success === 1 ? "success" : "error", autoDismiss: true });
        if (response.data.success === 1) {
            const userUpdated = await Requests.get("user");
            localStorage.setItem("user_bitwabi", JSON.stringify(userUpdated.data.user));
            setRedirect(true);
        }
    };

    const generateUpgradeAddress = async (ev) => {
        ev.preventDefault()
        let response = await Requests.post("upgradeAddress", { newPackage: selectedPackage });
        if (response.data.success === 1) {
            console.log("trago de upgrade address", response.data.payload)
        } else {
            addToast(response.data.message, { appearance: "error", autoDismiss: true });
        }
    }

    return (
        <div>
            {redirect && <Redirect to="/" />}
            <h1>Mejorar cuenta</h1>

            <div className="box">
                <form onSubmit={generateUpgradeAddress}>
                    {Object.entries(Constants.packgs)
                        .filter((e) => e[0] > user.selectedPackage)
                        .map((element) => {
                            const [pkgID, pkg] = element;
                            return (
                                <React.Fragment key={pkgID}>
                                    <input type="radio" id={pkgID} onClick={OnClick} checked={pkgID === selectedPackage} value={pkg.name} />
                                    <label htmlFor={pkgID}>
                                        {pkg.name} € {halfDiscount ? pkg.price * 0.6 : pkg.price}
                                    </label>
                                    <br />
                                </React.Fragment>
                            );
                        })}

                    <input type="submit" className="buttn buttn-primary" value="Activar licencia" />
                </form>
            </div>
        </div>
    );
}
