import React, {useState} from "react";
import { useToasts } from 'react-toast-notifications'
import {Redirect, Link, NavLink, useParams} from "react-router-dom"
import * as md5 from "md5"
import ReactCodeInput from 'react-verification-code-input'
import * as GlobalApp from '../../services/GlobalApp'
import Requests from "../../services/Requests"
import LayoutVisitors from "../../layout/LayoutVisitors"
import FormPassword from "../../formsComponents/password/formPassword"
import "./Signin.css"

export default function SignIn({newAccount})  {
    const {returnUrl}= useParams();
    const [password, setPass] = useState();
    const [email, setEmail] = useState();
    const [redirect, setRed] = useState(false);
    const [twoFactorAuthentication, setTwoFactorAuthentication] = useState(false);
    const [verificationCode, setVerificationCode] = useState("")
    const { addToast } = useToasts();

    const handleSubmit = (event) => {
        event.preventDefault()
        Requests.post('loginUser', { password: md5(password), email})
        .then( (response) =>{
            console.log("respuesta de check", response)
            if(response.data.success == 1){
                if(response.data.twoFactorAuthentication){
                    setTwoFactorAuthentication(true)
                }else{
                    localStorage.setItem("user_bitwabi", JSON.stringify(response.data.user))
                    setRed(true)
                }
            } else {
                addToast(response.data.message, { appearance: 'error', autoDismiss: true })
            }
        })
    }

    const OnSubmitVerificationCode = (event) => {
        event.preventDefault()
        Requests.post('verifyTwoFactorSecret', { password: md5(password), email, code: verificationCode})
        .then( (response) =>{
            console.log("respuesta de verification", response)
            if(response.data.success == 1){
                localStorage.setItem("user_bitwabi", JSON.stringify(response.data.user))
                setRed(true)
            } else {
                addToast(response.data.message, { appearance: 'error', autoDismiss: true })
            }
        })
    }

    const OnCompleteVerificationCode = (code) => {
        Requests.post('verifyTwoFactorSecret', { password: md5(password), email, code})
        .then( (response) =>{
            console.log("respuesta de verification", response)
            if(response.data.success == 1){
                localStorage.setItem("user_bitwabi", JSON.stringify(response.data.user))
                setRed(true)
            } else {
                addToast(response.data.message, { appearance: 'error', autoDismiss: true })
            }
        })
    }

    return (
        <LayoutVisitors>
            {redirect && <Redirect to={`/${returnUrl || ""}`} />}
            {
                !twoFactorAuthentication ? 
                    <form onSubmit={handleSubmit} id="formSignIn">
                        <p className="desc"><b>¡Bienvenido!</b> Identifícate para acceder a tu cuenta.</p>
                        <div>
                            <div>
                                <input type="email" placeholder="Correo electrónico" className="formControl" required 
                                    value={email} onChange={ev=>setEmail(ev.target.value)}/>
                            </div>
                            <FormPassword placeholder="Contraseña" value={password} onChange={ev=>setPass(ev.target.value)}/>
                        </div>
                        <div>
                            <button className="buttn buttn-primary">Acceder</button>
                        </div>
                        <div className="legal" style={{marginTop: 20}}>
                            <span>¿Olvidaste tu contraseña?,</span> <span><NavLink to="/recoverPassword">Recupérala aquí</NavLink></span>
                        </div>
                    </form>
                :
                    <form onSubmit={OnSubmitVerificationCode}>
                        <p className="desc">Autenticación en dos pasos.</p>
                        <p className="desc">Por favor ingresa tu codigo de autenticación de dos pasos:</p>
                        <ReactCodeInput 
                            title="Código de verificación"
                            placeholder={["-","-","-","-","-","-"]}
                            id="verificationCode" 
                            onChange={(event)=>{
                                setVerificationCode(event)
                            }} 
                            onComplete={(event) => {OnCompleteVerificationCode(event)}}
                            autoFocus={true}
                        />
                        <input style={{marginTop: "20px"}} type="submit" className="buttn buttn-primary" value="Verificar"/>
                    </form>
            }
        </LayoutVisitors>
    )
}
