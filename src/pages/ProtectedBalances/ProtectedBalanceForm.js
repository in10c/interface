import React, { useState, useEffect, useContext } from 'react';
import { ProtectedBalancesContext } from './ProtectedBalancesContext/ProtectedBalancesContext'

//////////////////////////////////////
///////////// Deprecated /////////////
//////////////////////////////////////

export default function AddProtectedBalanceForm(props) {

    const { addProtectedBalance, getProtectedBalance, updateProtectedBalance, addToast, setCurrentUpdate } = useContext(ProtectedBalancesContext);
    const [ protectedBalance, setProtectedBalance ] = useState({currency: "", quantity: ""});
    const [isUpdate, setIsUpdate] = useState(false);
    const [errorFields, setErrorFields] = useState({theresIsCurrencyError: false, theresIsQuantityError: false})

    useEffect(() => {
        async function get(id){ setProtectedBalance(await getProtectedBalance(id)); }
        if (props.type === "update"){
            get(props.id);
            setIsUpdate(true);
        }
    }, []);
    
    const OnChangeText = (event) => {
        const {id, value} = event.target;
        setProtectedBalance({...protectedBalance, ...{
            [id] : value
        }});
        setErrorFields({theresIsCurrencyError: false, theresIsQuantityError: false});
    };
    
    const OnSumbmit = async (event) => {
        event.preventDefault();
        const {currency, quantity} = protectedBalance;
        
        if(!currency.trim() || !quantity.trim()){
            addToast('Por favor llena todos los campos', { appearance: 'error', autoDismiss: true});
            setErrorFields({
                theresIsCurrencyError: !currency.trim(),
                theresIsQuantityError: !quantity.trim()
            });
            return;
        }

        if(!(/^\d+(\.\d{1,8})?$/.test(quantity.trim()))){
            addToast('La cantidad debe ser un numero positivo de a lo mas ocho decimales', { appearance: 'error', autoDismiss: true});
            setErrorFields({
                theresIsQuantityError: true
            });
            return;
        }

        if (props.type === "update"){
            await updateProtectedBalance(props.id, currency, quantity);
            setCurrentUpdate(null);
        }else{
            await addProtectedBalance(currency, quantity);
            setProtectedBalance({currency: "", quantity: ""});
        }

    };

    return (
        <div className={`${isUpdate ? "update": "add"}`} >
            <h2>{props.type === "update" ? "Editar" : "Agregar"} balance protegido</h2>
            <form onSubmit={OnSumbmit}>
                <div className="formControl">
                    <input 
                        type="text" 
                        id="currency" 
                        placeholder="Moneda"
                        onChange={OnChangeText} 
                        value={protectedBalance.currency} 
                        className={`formControl ${errorFields.theresIsCurrencyError ? "error" : ""}`}
                    />
                </div>
                <div className="formControl">
                    <input 
                        type="text" 
                        id="quantity"
                        placeholder="Cantidad"
                        onChange={OnChangeText} 
                        value={protectedBalance.quantity} 
                        className={`formControl ${errorFields.theresIsQuantityError ? "error" : ""}`}
                    />
                </div>
                <input type="submit" value="Guardar" className="buttn buttn-primary"/>
            </form>
        </div>
    );
};
