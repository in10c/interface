import React, { useState, useContext, useEffect } from "react";
import ProtectedBalanceForm from "./ProtectedBalanceForm";
import { ProtectedBalancesContext } from "./ProtectedBalancesContext/ProtectedBalancesContext";

export default function ProtectedBalanceTableRow(props) {
    const [balance, setBalance] = useState({ ...props.balance, ...{ quantityToProtect: props.balance.free } });
    const { balances, protectedBalances, setProtectedBalances } = useContext(ProtectedBalancesContext);

    useEffect(() => {
        setBalance({ ...props.balance, ...{ quantityToProtect: props.balance.free } });
        if (balance.isProtected) {
            setProtectedBalances({ ...protectedBalances, ...{ [balance.asset]: balance.quantityToProtect } });
        }
    }, [balances]);

    const OnClickCheck = (event) => {
        if (!!event.target.checked) {
            setProtectedBalances({ ...protectedBalances, ...{ [balance.asset]: balance.quantityToProtect } });
        } else {
            const toDelete = protectedBalances;
            delete toDelete[balance.asset];
            setProtectedBalances(toDelete);
        }
        setBalance({ ...balance, ...{ isProtected: !!event.target.checked } });
    };

    const OnChangeQuantity = (event) => {
        setBalance({ ...balance, ...{ quantityToProtect: event.target.value } });
        if (balance.isProtected) {
            setProtectedBalances({ ...protectedBalances, ...{ [balance.asset]: event.target.value } });
        }
    };

    return (
        <tr>
            <td>
                <input type="checkbox" checked={balance.isProtected} onChange={OnClickCheck} />
            </td>
            <td>{balance.asset}</td>
            <td>
                {balance.isProtected ? (
                    <input
                        type="number"
                        min="0"
                        step="0.00000001"
                        value={balance.quantityToProtect}
                        max={balance.free}
                        onChange={OnChangeQuantity}
                    />
                ) : (
                    balance.free
                )}
            </td>
            <td>{balance.protectedQuantity}</td>
        </tr>
    );
}
