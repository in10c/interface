import React, { useState, useEffect, useContext } from "react";
import ProtectedBalanceTableRow from "./ProtectedBalanceTableRow";
import { ProtectedBalancesContext } from "./ProtectedBalancesContext/ProtectedBalancesContext";

export default function ProtectedBalanceList() {
    const { balances, updateProtectedBalances } = useContext(ProtectedBalancesContext);
    const [isEmpty, setIsEmpty] = useState(true);

    useEffect(() => {
        setIsEmpty(!balances.length);
    }, []);

    return (
        <div id="protectedBalanceList" className={`${isEmpty ? "hidden" : ""}`}>
            <table>
                <thead>
                    <tr>
                        <th>Protegido</th>
                        <th>Moneda</th>
                        <th>Cantidad</th>
                        <th>Cantidad protegida</th>
                    </tr>
                </thead>
                <tbody>
                    {balances.map((balance, i) => balance.asset != "BTC" ? <ProtectedBalanceTableRow key={i} balance={balance} /> : null )}
                </tbody>
            </table>
            <button
                className="buttn buttn-primary savebutton"
                onClick={async () => {
                    await updateProtectedBalances();
                }}
            >
                Guardar
            </button>
        </div>
    );
}
