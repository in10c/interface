import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import Request from "../../../services/Requests";
import { useToasts } from "react-toast-notifications";

const ProtectedBalancesContext = React.createContext();
const { Provider, Consumer } = ProtectedBalancesContext;

const ProtectedBalancesProvider = ({ children }) => {
    const { addToast } = useToasts();
    const [redirect, setRedirect] = useState(false);
    const [redirectRoute, setRedirectRoute] = useState("/");
    const [balances, setBalances] = useState([]);
    const [protectedBalances, setProtectedBalances] = useState({});
    const [refresh, setRefresh] = useState(false);
    const [currentUpdate, setCurrentUpdate] = useState(null);

    useEffect(() => {
        async function getProtectedBalances() {
            const response = await Request.get("protectedBalances");
            if (!!response.data.failAuth) {
                addToast(response.data.message, { appearance: "error", autoDismiss: true });
                localStorage.removeItem("user_bitwabi");
                localStorage.removeItem("access-token");
                setRedirectRoute("/signIn");
                setRedirect(true);
            }
            if (response.data.success === 0) {
                addToast(response.data.message, { appearance: "error", autoDismiss: true });
                setRedirect(true);
            }
            setBalances(response.data.balances);
        }
        getProtectedBalances();
        setRefresh(false);
    }, [refresh]);

    const addProtectedBalance = async (currency, quantity) => {
        const response = await Request.post("protectedBalances", { currency, quantity });
        if (!!response.data.success) {
            setRefresh(true);
        }
        addToast(response.data.message, { appearance: !!response.data.success ? "success" : "error", autoDismiss: true });
    };

    const getProtectedBalance = async (id) => {
        const response = await Request.get(`protectedBalances/${id}`);
        if (!!response.data.success) {
            return response.data.balance;
        }
        return {};
    };

    const updateProtectedBalances = async () => {
        const data = Object.keys(protectedBalances).map((key) => {return {asset: key, quantity: protectedBalances[key]}});
        const response = await Request.post("protectedBalances", { balances: data });
        if (!!response.data.success) {
            setRefresh(true);
        }
        addToast(response.data.message, { appearance: !!response.data.success ? "success" : "error", autoDismiss: true });
    };

    const updateProtectedBalance = async (id, currency, quantity) => {
        const response = await Request.put(`protectedBalances/${id}`, { currency, quantity });
        if (!!response.data.success) {
            setRefresh(true);
        }
        addToast(response.data.message, { appearance: !!response.data.success ? "success" : "error", autoDismiss: true });
    };

    const deleteProtectedBalance = async (id) => {
        const response = await Request.delete(`protectedBalances/${id}`);
        if (!!response.data.success) {
            setRefresh(true);
        }
        addToast(response.data.message, { appearance: !!response.data.success ? "success" : "error", autoDismiss: true });
    };

    return (
        <Provider value={{ protectedBalances, balances, setProtectedBalances, updateProtectedBalances, addToast, refresh }}>
            {redirect && <Redirect to={redirectRoute} />}
            {children}
        </Provider>
    );
};

export { ProtectedBalancesProvider, Consumer as ProtectedBalancesConsumer, ProtectedBalancesContext };
