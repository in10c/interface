import React from 'react';
import ProtectedBalanceForm from './ProtectedBalanceForm';
import ProtectedBalanceList from './ProtectedBalanceList';
import { ProtectedBalancesProvider } from './ProtectedBalancesContext/ProtectedBalancesContext';

import './ProtectedBalances.css';

export default function ProtectedBalances() {
    return (
        <div id="protectedBalances">
            <h1>Balances protegidos</h1>
            <div className="box">
                <div className="logotype">
                    <img src="/images/logotipo.png"/>
                </div>
                <div className="text">
                    <p>Protege estos balances para que no sean sean comprados (bitcoin) y/o vendidos (altcoins) en el sistema de <b>copytrade</b></p>
                </div>
                <ProtectedBalancesProvider>
                    <ProtectedBalanceList />
                </ProtectedBalancesProvider>
            </div>
        </div>
    );
};
