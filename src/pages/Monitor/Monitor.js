import React from 'react';
import { useToasts } from 'react-toast-notifications'
import Requests from "../../services/Requests"

export default function MyPayments() {
    const { addToast } = useToasts()
    const [robots, setRobots] = React.useState([]) 
    React.useEffect(()=>{
        Requests.getTradingServer("getRobots").then(response=>{
            console.log("traiogop de response", response)
            if(response.data.success=== 1){
                setRobots(response.data.robots)
            }
        })
    }, [])
    return(
        <div id="myPayments">
            <h1>Monitor de Robots</h1>
            <di className="box">
                <table style={{width: "100%"}}>
                    <thead>
                        <tr>
                            <th>Usuario</th>
                            <th>Estado</th>
                            <th>Logs</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {robots.map(val=><tr>
                        <td>{val.userID}</td>
                        <td>{val.status}</td>
                        <td>
                            <ul>
                                {val.logs && val.logs.length > 0 && val.logs.map(lg=><li>
                                    {lg.status+ " - "+ lg.symbol}
                                </li>)}
                            </ul>
                        </td>
                        <td>
                            <button>Matar bot</button>
                        </td>
                        </tr>)}
                    </tbody>
                </table>
            </di>
        </div>
    )

}