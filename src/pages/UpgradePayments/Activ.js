import React from 'react';
import { useToasts } from 'react-toast-notifications'
import Constants from '../../services/Constants';
import Requests from "../../services/Requests"

export default function RefComm({activ}) {
    const { addToast } = useToasts()
    const [log, setLog] = React.useState([])
    const [activation, setActivation] = React.useState({ pendingDispersion : [] })

    const verify = (upgradeID) => {
        Requests.post("verifyUpgradeDispersion", {upgradeID}).then((result)=>{
            console.log("regreso de actv", result)
            if(result.data.success==1){
                setLog(result.data.log)
            }  else {
                addToast(result.data.message, { appearance: 'error', autoDismiss: true })
            }
        })
    }
    const regenerate = (idActiv) => {
        Requests.post("regenerateUpgradeDispersionArray", { upgradeID: idActiv }).then((result)=>{
            console.log("regreso de send ", result)
            if(result.data.success==1){
                addToast("Se ha regenerado correctamente.", { appearance: 'success', autoDismiss: true })
            }  else {
                addToast(result.data.message, { appearance: 'error', autoDismiss: true })
            }
        })
    }
    const forceDistribution = (upgradeID) => {
        Requests.post("forceDispersateUpgrade", { upgradeID }).then((result)=>{
            console.log("regreso de send ", result)
            if(result.data.success==1){
                addToast("Se ha distribuido correctamente.", { appearance: 'success', autoDismiss: true })
            }  else {
                addToast(result.data.message, { appearance: 'error', autoDismiss: true })
            }
        })
    }
    React.useEffect(()=>{
        setActivation(JSON.parse(JSON.stringify(activ)))
    }, [])
    return( <tr>
        <td>
            Usuario: {activation.userID}<br/>
            ({activation.address}) <br/>
            ENVIADO: {activation.quantity}
        </td>
        <td>{activation.oldPackage}</td>
        <td>{activation.newPackage}</td>
        <td>
            Pagado previamente: {activation.payedOld} <br/>
            Descuento?:  {activation.halfDiscount ? "SI" : "NO"} <br/>
            Costo nueva licencia:  {activation.toPayForNewOne} <br/>
            A pagar:  {activation.diffPayment} <br/>
        </td>
        <td>
            {
                activation.pendingDispersion.map(disp=>{
                let btc = parseFloat(disp.quantity / 100000000)
                return <p>
                    Dispersa: {disp.address}<br/>
                    Satoshis: {disp.quantity}<br/>
                    BTC: {btc}<br/>
                    Aprx Euro: €{ parseInt(btc * activation.priceBTCinEuros) }
                </p>})
            }
        </td>
        <td>
            {activation.status}<br/>
            {activation.status === "Dispersada correctamente" && <span><a target="_blank" href={Constants.btcExplorer()+"/"+activation.commissionTxID}>TXID</a></span>}
        </td>
        <td><pre>{log}</pre></td>
        <td>
             <button onClick={ev=>verify(activation._id)}>Verificar</button>
            <button onClick={ev=>regenerate(activation._id)}>Regenerar dispersion</button>
            <button onClick={ev=>forceDistribution(activation._id)}>Forzar distribución</button>
        </td>
    </tr> )

}