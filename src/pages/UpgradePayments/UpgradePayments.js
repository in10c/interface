import React from 'react';
import { useToasts } from 'react-toast-notifications'
import { Redirect } from 'react-router-dom'
import Requests from "../../services/Requests"
import Activ from "./Activ"
import "./RefComm.css"

export default function RefComm() {
    const { addToast } = useToasts()
    const [activations, setActivations] = React.useState([])
    const [redirect, setRedirect] = React.useState(undefined)
    React.useEffect(()=>{
        Requests.get("amIdeveloper").then((result)=>{
            console.log("de amIdeveloper ", result)
            if(result.data.result === true){
                setRedirect(false)
                Requests.get("getupgrades").then((result)=>{
                    console.log("regreso de upgrds", result)
                    if(result.data.success == 1){                
                        setActivations(result.data.upgrades)
                    } else {
                        addToast(result.data.message, { appearance: 'error', autoDismiss: true })
                    }
                })
            }else{
                setRedirect(true)
            }
        })
    }, [])
    return(
        <div>
        {redirect === undefined ? 
        <div></div>
        :
        redirect ?
        <Redirect to="/" />
        :
        <React.Fragment>
            <h1>Dispersión de upgrades</h1>
            <div className="box">
                <table style={{width: "100%"}} id="tabladispersa">
                    <thead>
                        <tr>
                            <th>UserID (Wallet)</th>
                            <th>antes</th>
                            <th>despues</th>
                            <th>finanza</th>
                            <th>A dispersar</th>
                            <th>Respuestas</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {activations.map(val=><Activ activ={val}/>)}
                    </tbody>
                </table>
            </div>
            </React.Fragment>
}
        </div>
    )

}