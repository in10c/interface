import React, {useState} from 'react'
import { Redirect } from "react-router-dom";
import {  useToasts } from 'react-toast-notifications'
import * as md5 from "md5";
import Requests from '../../services/Requests'
import FormPassword from "../../formsComponents/password/formPassword"

import './ChangePassword.css'

export default function ChangePassword() {
    const { addToast } = useToasts()
    const user = JSON.parse(localStorage.getItem("user_bitwabi"))
    const [redirect, setRedirect] = useState(false)
    const [emptyFields, setEmptyFields] = useState({
        isOldPasswordEmpty: false,
        isNewPasswordEmpty: false,
        isConfirmNewPasswordEmpty: false
    })
    const [userData, setUserData] = useState({
        userID: user.id, 
        oldPassword: "", 
        newPassword: "", 
        confirmNewPassword: "", 
        isConfirmedPassword: true,
        isNewPasswordDiferent: true
    })

    const OnSubmit = async (event) => {
        event.preventDefault()
        const {userID, oldPassword, newPassword, confirmNewPassword, isConfirmedPassword, isNewPasswordDiferent} = userData
        if(!oldPassword.trim() || !newPassword.trim() || !confirmNewPassword.trim()){
            addToast('Por favor llena todos los campos', { appearance: 'error', autoDismiss: true})
            setEmptyFields({
                isOldPasswordEmpty: !oldPassword.trim(),
                isNewPasswordEmpty: !newPassword.trim(),
                isConfirmNewPasswordEmpty: !confirmNewPassword.trim()
            })
            return
        }
        if(!isConfirmedPassword){
            addToast('Las contraseñas deben coincidir para continuar', { appearance: 'error', autoDismiss: true})
            setEmptyFields({
                isOldPasswordEmpty: !oldPassword.trim(),
                isNewPasswordEmpty: true,
                isConfirmNewPasswordEmpty: true
            })
            return
        }
        if(!isNewPasswordDiferent){
            addToast('La nueva contraseña no puede ser igual a la anterior', { appearance: 'error', autoDismiss: true})
            setEmptyFields({
                isOldPasswordEmpty: !oldPassword.trim(),
                isNewPasswordEmpty: true,
                isConfirmNewPasswordEmpty: true
            })
            return
        }
        const response = await Requests.put("changePassword", {oldPassword: md5(oldPassword), newPassword: md5(newPassword)})
        if(!!response.data.failAuth){
            localStorage.removeItem("user_bitwabi")
            localStorage.removeItem("access-token")
        }
        addToast(response.data.message, { appearance: response.data.success === 1 ? 'success' : 'error', autoDismiss: true })
        setRedirect(response.data.success === 1)
    }

    const OnChangeText = (event) => {
        const {id, value} = event.target
        const {oldPassword, newPassword, confirmNewPassword, isConfirmedPassword, isNewPasswordDiferent} = userData
        let isDiferent = isNewPasswordDiferent
        let isConfirmed = isConfirmedPassword
        switch (id) {
            case "oldPassword":
                if((newPassword.trim() && confirmNewPassword.trim())){
                    isDiferent = isConfirmed ? newPassword !== value : true
                }else{
                    isDiferent = true
                }
                break;
            default:
                isConfirmed = id === "newPassword" ? value === confirmNewPassword : value === newPassword
                if(isConfirmed){
                    isDiferent = oldPassword !== value
                }else{
                    isDiferent = true
                }
                break;
        }
        setUserData({...userData, ...{
            [id] : value,
            isConfirmedPassword: isConfirmed,
            isNewPasswordDiferent: isDiferent
        }})
        setEmptyFields({
            isOldPasswordEmpty: false,
            isNewPasswordEmpty: false,
            isConfirmNewPasswordEmpty: false
        })
    }

    return (
    <div id="changePassword">
    {redirect && <Redirect to="/" />}
            <h1>Cambiar contraseña</h1>
            <div className="box">
                <form onSubmit={OnSubmit}>
                    <div className="formControl">
                        <FormPassword 
                            id="oldPassword" 
                            placeholder="Anterior contraseña" 
                            value={userData.oldPassword} 
                            onChange={OnChangeText} 
                            className={emptyFields.isOldPasswordEmpty ? "error" : ""}
                        />
                    </div>
                    <div className="formControl">
                        <FormPassword 
                            id="newPassword" 
                            placeholder="Nueva contraseña" 
                            value={userData.newPassword} 
                            onChange={OnChangeText} 
                            className={emptyFields.isOldPasswordEmpty ? "error" : ""}
                        />
                    </div>
                    <div className="formControl">
                        <FormPassword 
                            id="confirmNewPassword" 
                            placeholder="Confirmar nueva contraseña" 
                            value={userData.confirmNewPassword} 
                            onChange={OnChangeText} 
                            className={emptyFields.isOldPasswordEmpty ? "error" : ""}
                        />
                    </div>
                    <label style={userData.isNewPasswordDiferent ? {display: "none"} : {display: "block"}}>La nueva contraseña no puede ser igual a la anterior</label>
                    <label style={userData.isConfirmedPassword ? {display: "none"} : {display: "block"}}>Las contraseñas no coinciden</label>
                    <input type="submit" value="Guardar" className="buttn buttn-primary"/>
                </form>
            </div>
    </div>
    )
}
