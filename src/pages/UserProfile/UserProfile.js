import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import { useToasts } from "react-toast-notifications";
import Requests from "../../services/Requests";
import "./UserProfile.css";

export default function UserProfile({ children }) {
    const { addToast } = useToasts();
    const [userInfo, setUserInfo] = useState({
        email: "",
        name: "",
        lastName: "",
        nickName: "",
        netPaymentsWallet: "",
    });
    const [fieldsErrors, setFieldsErrors] = useState({
        thereIsNetPaymentsWalletError: false,
        thereIsNameError: false,
        thereIsLastNameError: false,
        thereIsNickNameError: false,
    });

    useEffect(() => {
        async function getInfo() {
            const response = await Requests.get("user");
            if (!!response.data.failAuth) {
                addToast(response.data.message, { appearance: "error", autoDismiss: true });
                localStorage.removeItem("user_bitwabi");
                localStorage.removeItem("access-token");
            }
            setUserInfo(response.data.user);
        }
        getInfo();
    }, []);

    const OnSubmit = async (event) => {
        event.preventDefault();
        const { name, lastName, nickName, netPaymentsWallet } = userInfo;
        if (
            !name ||
            !name.trim() ||
            !lastName ||
            !lastName.trim() ||
            !nickName ||
            !nickName.trim() ||
            !netPaymentsWallet ||
            !netPaymentsWallet.trim()
        ) {
            addToast("Por favor llena todos los campos", { appearance: "error", autoDismiss: true });
            setFieldsErrors({
                thereIsNameError: !name || !name.trim(),
                thereIsLastNameError: !lastName || !lastName.trim(),
                thereIsNickNameError: !nickName || !nickName.trim(),
                thereIsNetPaymentsWalletError: !netPaymentsWallet || !netPaymentsWallet.trim(),
            });

            return;
        }

        if (!/^([A-Z]{1}[a-zñáéíóú]+[\s]*)+$/.test(name.trim())) {
            addToast("Por favor escribe tu nombre en un formato correcto", { appearance: "error", autoDismiss: true });
            setFieldsErrors({
                thereIsNameError: true,
            });
            return;
        }

        if (!/^([A-Z]{1}[a-zñáéíóú]+[\s]*)+$/.test(lastName.trim())) {
            addToast("Por favor escribe tu apellido en un formato correcto", { appearance: "error", autoDismiss: true });
            setFieldsErrors({
                thereIsLastNameError: true,
            });
            return;
        }

        const response = await Requests.put("user", userInfo);
        if (!!response.data.notAvailableNickName) {
            setFieldsErrors({
                thereIsNickNameError: true,
            });
        }

        if (!!response.data.failAuth) {
            localStorage.removeItem("user_bitwabi");
            localStorage.removeItem("access-token");
        }
        addToast(response.data.message, { appearance: response.data.success === 1 ? "success" : "error", autoDismiss: true });
    };

    const OnChangeText = (event) => {
        const { id, value } = event.target;
        setUserInfo({
            ...userInfo,
            ...{
                [id]: value,
            },
        });
        setFieldsErrors({
            thereIsNetPaymentsWalletError: false,
            thereIsNameError: false,
            thereIsLastNameError: false,
            thereIsNickNameError: false,
        });
    };

    return (
        <div id="userProfile">
            <h1>Mi perfil</h1>
            <div className="box">
                <form onSubmit={OnSubmit}>
                    <div className="avatar">
                        <img src="/images/avatar.png" />
                    </div>
                    <div className="formControl name">
                        <input
                            type="text"
                            id="name"
                            placeholder="Nombre"
                            onChange={OnChangeText}
                            value={userInfo.name}
                            className={`formControl ${fieldsErrors.thereIsNameError ? "error" : ""}`}
                        />
                    </div>
                    <div className="formControl lastName">
                        <input
                            type="text"
                            id="lastName"
                            placeholder="Apellido"
                            onChange={OnChangeText}
                            value={userInfo.lastName}
                            className={`formControl ${fieldsErrors.thereIsLastNameError ? "error" : ""}`}
                        />
                    </div>
                    <div className="formControl nickName">
                        <input
                            type="text"
                            id="nickName"
                            placeholder="NickName"
                            onChange={OnChangeText}
                            value={userInfo.nickName}
                            className={`formControl ${fieldsErrors.thereIsNickNameError ? "error" : ""}`}
                        />
                    </div>
                    <div className="formControl email">
                        <input
                            type="text"
                            id="email"
                            readOnly="true"
                            value={userInfo.email}
                            className={`formControl readonly`}
                        />
                    </div>
                    <div className="formControl netPaymentsWallet">
                        <input
                            type="text"
                            id="netPaymentsWallet"
                            placeholder="Dirección BTC para recibir comisiones"
                            onChange={OnChangeText}
                            value={userInfo.netPaymentsWallet}
                            className={`formControl ${fieldsErrors.thereIsNetPaymentsWalletError ? "error" : ""}`}
                        />
                    </div>
                    <input type="submit" value="Guardar" className="buttn buttn-primary" />
                    <br />
                    <NavLink to="/">Cancelar</NavLink>
                </form>
            </div>
        </div>
    );
}
