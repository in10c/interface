import React, { useState, useEffect, useContext } from "react";
import { Redirect } from "react-router-dom";
import { useToasts } from "react-toast-notifications";
import Requests from "../../services/Requests";
import "./ChangeEmail.css";
import { LayoutContext } from "../../layout/LayoutContext/LayoutContext";
import Loading from "../../layout/Loading/Loading";

export default function ChangeEmail() {
    const { loadingVisible, setLoadingVisible } = useContext(LayoutContext);
    const { addToast } = useToasts();
    const [fieldsErrors, setFieldsErrors] = useState({
        thereIsEmailError: false,
        thereIsVerificationCodeError: false,
    });
    const [userData, setUserData] = useState({
        email: "",
        newEmail: "",
        emailCode: "",
        isConfirmedEmail: false,
    });
    const [sendedCode, setSendedCode] = useState(false);
    const [redirect, setRedirect] = useState(false);

    useEffect(() => {
        async function getInfo() {
            const response = await Requests.get("user");
            if (!!response.data.failAuth) {
                addToast(response.data.message, { appearance: "error", autoDismiss: true });
                localStorage.removeItem("user_bitwabi");
                localStorage.removeItem("access-token");
            }
            setUserData({ email: response.data.user.email, isConfirmedEmail: response.data.user.confirmedEmail });
        }
        getInfo();
    }, []);

    const OnSubmitEmail = async (event) => {
        setLoadingVisible(true);
        event.preventDefault();
        const { email, newEmail } = userData;
        if (!newEmail || !newEmail.trim()) {
            addToast("Por favor llena todos los campos", { appearance: "error", autoDismiss: true });
            setFieldsErrors({
                thereIsEmailError: !newEmail || !newEmail.trim(),
            });
            setLoadingVisible(false);

            return;
        }
        const response = await Requests.post(
            !sendedCode ? "sendEmailVerificationCode" : "cancelEmailVerificationCode",
            { email, newEmail }
        );
        if (response.data.success === 1) {
            setSendedCode(!sendedCode);
        }

        if (!!response.data.failAuth) {
            localStorage.removeItem("user_bitwabi");
            localStorage.removeItem("access-token");
        }
        addToast(response.data.message, { appearance: response.data.success === 1 ? "success" : "error", autoDismiss: true });
        setLoadingVisible(false);
    };

    const OnSubmitCode = async (event) => {
        event.preventDefault();
        const { newEmail, emailCode } = userData;
        if (!emailCode || !emailCode.trim()) {
            addToast("Por favor llena todos los campos", { appearance: "error", autoDismiss: true });
            setFieldsErrors({
                thereIsEmailError: !emailCode || !emailCode.trim(),
            });

            return;
        }
        const response = await Requests.put("confirmChangeEmail", { newEmail, emailCode });
        if (!!response.data.failAuth) {
            localStorage.removeItem("user_bitwabi");
            localStorage.removeItem("access-token");
        }
        addToast(response.data.message, { appearance: response.data.success === 1 ? "success" : "error", autoDismiss: true });
        if (response.data.success === 1) {
            setRedirect(true);
        }
    };

    const OnChangeText = (event) => {
        const { id, value } = event.target;
        setUserData({
            ...userData,
            ...{
                [id]: value,
            },
        });
        setFieldsErrors({
            thereIsEmailError: false,
            thereIsVerificationCodeError: false,
        });
    };

    return (
        <React.Fragment>
            <Loading visible={loadingVisible} />
            <div id="changeEmail">
                {redirect && <Redirect to="/" />}
                <h1>Cambiar correo electrónico</h1>
                <div className="box">
                    <p>Correo electrónico actual: {userData.email}</p>
                    <div className={`isConfirmedEmail ${userData.isConfirmedEmail ? "confirmed" : ""}`}>
                        <span>{`${userData.isConfirmedEmail ? "Confirmado" : "No confirmado"}`}</span>
                    </div>
                    <form onSubmit={OnSubmitEmail}>
                        <label htmlFor="newEmail">Correo electrónico nuevo:</label>
                        <div className="formControl">
                            <input
                                type="email"
                                id="newEmail"
                                placeholder="Correo electrónico nuevo"
                                onChange={OnChangeText}
                                readOnly={sendedCode}
                                value={userData.newEmail}
                                className={`formControl ${fieldsErrors.thereIsEmailError ? "error" : ""} ${
                                    sendedCode ? "readonly" : ""
                                }`}
                            />
                        </div>
                        <input
                            type="submit"
                            value={sendedCode ? "Cambiar correo" : "Guardar"}
                            className="buttn buttn-primary"
                        />
                    </form>
                    <form onSubmit={OnSubmitCode} style={!sendedCode ? { display: "none" } : {}}>
                        <div className="formControl">
                            <input
                                type="text"
                                id="emailCode"
                                placeholder="Código de verificación"
                                onChange={OnChangeText}
                                value={userData.emailCode}
                                className={`formControl ${fieldsErrors.thereIsVerificationCodeError ? "error" : ""}`}
                            />
                        </div>
                        <input type="submit" value="Verificar" className="buttn buttn-primary" />
                    </form>
                </div>
            </div>
        </React.Fragment>
    );
}
