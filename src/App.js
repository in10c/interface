/*
    Creado desde 0 por Cristian Salvador Orihuela Torres @ Numbers&Colors
*/
import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { ToastProvider } from 'react-toast-notifications'
//pages
import FirstConfig from "./pages/FirstConfig/FirstConfig"
import SignIn from "./pages/Signin/Signin"
import Token from "./pages/Token/Token"
import Reservation from "./pages/Reservation/Reservation"
import Layout from "./layout/Layout"
import PrivateRoute from "./core/PrivateRoute"
import Routes from "./layout/Routes"
import RecoverPasswordEmail from "./pages/RecoverPassword/RecoverPasswordEmail"
import RecoverPassword from "./pages/RecoverPassword/RecoverPassword"
import ExonerationResponsability from "./pages/ExonerationResponsability/ExonerationResponsability"
import LegalWarning from "./pages/LegalWarning/LegalWarning"
import TermsAndConditionsAndPrivacyPolicies from "./pages/TermsAndConditionsAndPrivacyPolicies/TermsAndConditionsAndPrivacyPolicies"

export default function App() {
    return (<ToastProvider>
            <Router>
                <Switch>
                    {Routes.map(val=> <PrivateRoute exact path={val.path} component={Layout} /> )}
                    <Route exact path="/reservation/:refererID?" component={Reservation} />
                    <Route exact path="/signin/:returnUrl?" component={SignIn} />
                    <Route exact path="/signup" component={FirstConfig} />
                    <Route exact path="/recoverPassword" component={RecoverPasswordEmail} />
                    <Route exact path="/token/:tkn" component={Token} />
                    <Route exact path="/recoverPassword/:token" component={RecoverPassword} />
                    <Route exact path="/exonerationResponsability" component={ExonerationResponsability} />
                    <Route exact path="/legalWarning" component={LegalWarning} />
                    <Route exact path="/termsAndConditionsAndPrivacyPolicies" component={TermsAndConditionsAndPrivacyPolicies} />
                </Switch>
            </Router>
        </ToastProvider>)
}
