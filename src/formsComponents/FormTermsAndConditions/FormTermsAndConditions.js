import React, { useState, useContext } from "react";
import { useToasts } from "react-toast-notifications";
import { Redirect } from "react-router-dom";
import { LayoutContext } from "../../layout/LayoutContext/LayoutContext";
import Loading from "../../layout/Loading/Loading";
import Requests from '../../services/Requests';
import "./FormTermsAndConditions.css";

export default function FormTermsAndConditions() {
    const { setModalVisible, loadingVisible, setLoadingVisible } = useContext(LayoutContext);
    const [isAgreeExonerationResponsability, setIsAgreeExonerationResponsability] = useState(false);
    const [isAgreeLegalWarning, setIsAgreeLegalWarning] = useState(false);
    const [isAgreeTermsAndConditionsAndPrivacyPolicies, setIsAgreeTermsAndConditionsAndPrivacyPolicies] = useState(false);
    const [sendToSignin, setSendToSignIn] = useState(false);
    const { addToast } = useToasts();
    const logout = ()=>{
        localStorage.removeItem("user_bitwabi")
        localStorage.removeItem("access-token")
        setSendToSignIn(true)
    }
    const OnSubmit = async (event) => {
        event.preventDefault();
        if(isAgreeExonerationResponsability && isAgreeLegalWarning && isAgreeTermsAndConditionsAndPrivacyPolicies){
            const response = await Requests.post("setTermsAndConditionsAgreement");
            
            if (!!response.data.failAuth) {
                localStorage.removeItem("user_bitwabi");
                localStorage.removeItem("access-token");
            }
            addToast(response.data.message, { appearance: response.data.success === 1 ? "success" : "error", autoDismiss: true });
            if (response.data.success === 1) {
                
                setModalVisible(null);
                logout()
            }
        }
    };

    return (
        <form id="termsAndConditions" onSubmit={OnSubmit}>
            {sendToSignin && <Redirect to="/signin" />}
            <h3>Términos y Condiciones</h3>
            <p>
                Para continuar es necesario que acepte las condiciones expuestas a continuación:
            </p>
            <div style={{textAlign: "left", margin: 20}}>
                <input
                    type="checkbox"
                    checked={isAgreeExonerationResponsability}
                    onChange={(event) => {
                        setIsAgreeExonerationResponsability(event.target.checked);
                    }}
                    id="agreeExonerationResponsability"
                />
                <label htmlFor="agreeExonerationResponsability">Acepto la <a href="./exonerationResponsability" target="_blank">Exoneración de responsabilidad;</a></label>
                <br />
                <br />
                <input
                    type="checkbox"
                    checked={isAgreeLegalWarning}
                    onChange={(event) => {
                        setIsAgreeLegalWarning(event.target.checked);
                    }}
                    id="agreeLegalWarning"
                />
                <label htmlFor="agreeLegalWarning">Acepto el <a href="./legalWarning" target="_blank">Aviso legal;</a></label>
                <br />
                <br />
                <input
                    type="checkbox"
                    checked={isAgreeTermsAndConditionsAndPrivacyPolicies}
                    onChange={(event) => {
                        setIsAgreeTermsAndConditionsAndPrivacyPolicies(event.target.checked);
                    }}
                    id="agreeTermsAndConditionsAndPrivacyPolicies"
                />
                <label htmlFor="agreeTermsAndConditionsAndPrivacyPolicies">Acepto la <a href="./termsAndConditionsAndPrivacyPolicies" target="_blank">Política de privacidad y los Términos de uso.</a></label>
                <br />
                <br /> 
            </div>
            
            <input
                type="submit"
                style={{cursor: "pointer"}}
                className={`buttn buttn-primary ${!isAgreeExonerationResponsability || !isAgreeLegalWarning || !isAgreeTermsAndConditionsAndPrivacyPolicies ? "disabled" : ""}`}
                disabled={!isAgreeExonerationResponsability || !isAgreeLegalWarning || !isAgreeTermsAndConditionsAndPrivacyPolicies}
                value="Aceptar"
            />
        </form>
    );
}
