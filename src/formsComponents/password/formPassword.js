import React, { useState } from "react";
import "./formPassword.css";

export default function FormPassword(props) {
    const [isVisiblePassword, setisVisiblePassword] = useState(false);
    return (
        <div class="formPassword">
            <input
                id={props.id || ""}
                type={isVisiblePassword ? "text" : "password"}
                placeholder={props.placeholder}
                className={`formControl ${props.className || ""}`}
                required
                value={props.value}
                name={props.name? props.name : undefined}
                onChange={props.onChange}
            />
            <i
                className={`fa fa-eye-slash ${isVisiblePassword ? "visiblePassword" : ""}`}
                onClick={() => {
                    setisVisiblePassword(!isVisiblePassword);
                }}
            ></i>
        </div>
    );
}
