import React, { useEffect, useState, useContext } from 'react'
import ReactCodeInput from 'react-verification-code-input'
import { Steps } from 'rsuite'
import { Redirect } from "react-router-dom"
import { useToasts } from "react-toast-notifications"
import { LayoutContext } from "../../layout/LayoutContext/LayoutContext"
import Requests from "../../services/Requests"
import "./FormTwoFactorAuthentication.css"

export default function FormTwoFactorAuthentication() {

    const { setModalVisible } = useContext(LayoutContext)
    const { addToast } = useToasts()
    const [step, setStep] = useState(0)
    const [enabled, setEnabled] = useState(false)
    const [desactivatingVerification, setDesactivatingVerification] = useState(false)
    const [desactivateVerificationCode, setDesactivateVerificationCode] = useState("")
    const [qrCode, setQrCode] = useState()
    const [tempVerificationCode, setTempVerificationCode] = useState("")
    const [redirect, setRedirect] = useState(false)

    const copiar = ()=>{
        navigator.clipboard.writeText(qrCode.key).then(function() {
            addToast('El código se ha copiado correctamente al portapapeles.', { appearance: 'success', autoDismiss: true })
        })
    }

    const onChange = async nextStep => {
        if(nextStep > 0){
            const response = await Requests.get("getTwoFactVerQRCode")
            if (!!response.data.failAuth) {
                addToast(response.data.message, { appearance: "error", autoDismiss: true })
                localStorage.removeItem("user_bitwabi")
                localStorage.removeItem("access-token")
                setRedirect(true)
            }
            if (response.data.success === 1) {
                setQrCode(response.data.qrcode)
                setDesactivatingVerification(false)
            }else{
                addToast(response.data.message, { appearance: "error", autoDismiss: true })
            }
        }
        setStep(nextStep < 0 ? 0 : nextStep > 3 ? 3 : nextStep)
    }
    const onNext = () => onChange(step + 1)
    const onPrevious = () => onChange(step - 1)

    useEffect(() => {
        async function getInfo() {
            const response = await Requests.get("verifyTwoFactorActive")
            if (!!response.data.failAuth) {
                addToast(response.data.message, { appearance: "error", autoDismiss: true })
                localStorage.removeItem("user_bitwabi")
                localStorage.removeItem("access-token")
                setRedirect(true)
            }
            setEnabled(response.data.result)
        }
        getInfo();
    }, [])

    const OnSubmit = async (event) => {
        event.preventDefault()
        const response = await Requests.post("verifyTwoFactorTEMPSecret", {code: tempVerificationCode})
        if (!!response.data.failAuth) {
            addToast(response.data.message, { appearance: "error", autoDismiss: true })
            localStorage.removeItem("user_bitwabi")
            localStorage.removeItem("access-token")
            setRedirect(true)
        }
        if (response.data.success === 1) {
            setEnabled(true)
            setDesactivatingVerification(false)
            setModalVisible(null)
            setStep(0)
            addToast("Activada autenticación en dos pasos", { appearance: "success", autoDismiss: true})
        }else{
            addToast(response.data.message, { appearance: response.data.success === 1 ? "success" : "error", autoDismiss: true })
        }
    }

    const OnSubmitDesactivate = async (event) => {
        event.preventDefault()
        const response = await Requests.post("desactivateTwoFactorAuth", {code: desactivateVerificationCode})
        if (!!response.data.failAuth) {
            addToast(response.data.message, { appearance: "error", autoDismiss: true })
            localStorage.removeItem("user_bitwabi")
            localStorage.removeItem("access-token")
            setRedirect(true)
        }
        if (response.data.success === 1) {
            setEnabled(false)
            setDesactivatingVerification(false)
            setModalVisible(null)
            addToast("Desctivada autenticación en dos pasos", { appearance: "success", autoDismiss: true})
        }else{
            addToast(response.data.message, { appearance: response.data.success === 1 ? "success" : "error", autoDismiss: true })
        }
    }

    return (
        <div id="formTwoFactAuthentication">
            {redirect && <Redirect to="signIn" />}
            <div className="formTwoFactAuthenticationTitle">
                <p><b>{!enabled ? "HABILITANDO" : "INHABILITAR"}</b></p>
                <p>AUTENTICADOR DE GOOGLE</p>
            </div>
            {!enabled ?  
                    <div>
                        <div className="stepsContainer">
                            <div className="titles">
                                <span className={step < 0 ? "wait" : ""}>Descarga la app</span>
                                <span className={step < 1 ? "wait" : ""}>Escanea el código QR</span>
                                <span className={step < 2 ? "wait" : ""}>Clave de respaldo</span>
                                <span className={step < 3 ? "wait" : ""}>Habilitar autenticador de Google</span>
                            </div>
                            <Steps current={step}>
                                <Steps.Item/>
                                <Steps.Item/>
                                <Steps.Item/>
                                <Steps.Item/>
                            </Steps>
                        </div>
                        <div className="stepPanelContainer">
                            <p className="stepTitle">PASO {step + 1}</p>
                            {
                                step === 0 ?
                                    <React.Fragment>
                                        <p className="stepDescription">Descarga e instala la aplicación de Autenticador de Google</p>
                                        <div className="dowloadAppImages">
                                            <a href="https://apps.apple.com/es/app/google-authenticator/id388497605" target="_blank">
                                                <img src="/images/appStore.png" alt="AppStore"/>
                                            </a>
                                            <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2" target="_blank">
                                                <img src="/images/googlePlay.png" alt="GooglePlay"/>
                                            </a>
                                        </div>
                                    </React.Fragment>
                                :
                                step === 1 ?
                                    <React.Fragment>
                                        <p className="stepDescription">Escanea el codigo QR en la aplicación instalada</p>
                                        <div className="qrContainer">
                                            <img className="qrCodeImage" src={qrCode.qrcode}/>
                                            <p className="stepDescription">Si no puedes escanear el código QR, sigue al siguiente paso e inserta de manera manual en la app el código que se te proporcionará</p>
                                        </div>
                                    </React.Fragment>
                                :
                                step === 2 ?
                                    <React.Fragment>
                                        <p className="stepDescription">Guarda esta clave como nota, en papel te ayudará a recuperar tu cuenta de Autenticador de Google si tu teléfono llega a perderse</p>
                                        <div className="key">
                                            <span>{qrCode.key}</span>
                                            <br/>
                                            <i className="fa fa-clipboard" onClick={copiar}></i>
                                        </div>
                                    </React.Fragment>
                                :
                                    <React.Fragment>
                                        <p className="stepDescription">Habilita el autenticador de Google</p>
                                        <form onSubmit={OnSubmit}>
                                            <ReactCodeInput 
                                                title="Código de verificación"
                                                placeholder={["-","-","-","-","-","-"]}
                                                id="tempVerificationCode" 
                                                onChange={(event)=>{
                                                    setTempVerificationCode(event)
                                                }} 
                                                autoFocus={true}
                                                fieldWidth={50}
                                                fieldHeight={50}
                                            />
                                            <input style={{marginTop: "20px"}} type="submit" className="buttn buttn-primary" value="Verificar"/>
                                        </form>
                                    </React.Fragment>
                            }
                        </div>
                        {
                            step < 3 &&
                            <button className="buttn buttn-primary" onClick={onNext}>
                                Siguiente
                            </button>
                        }
                        <br/>
                        {
                            step > 0 &&
                            <a href="" onClick={(event)=>{event.preventDefault();onPrevious();}}>
                                Paso anterior
                            </a>
                        }
                    </div>
                :
                    !desactivatingVerification ?
                        <div>
                            <p>Ha activado la autenticación en dos pasos</p>
                            <button className="buttn buttn-primary" onClick={()=>{setDesactivatingVerification(true)}}>Desactivar autenticación en dos pasos</button>
                        </div>
                    :
                        <div>
                            <p>Introduzca su código de verificación de dos pasos: </p>
                            <form onSubmit={OnSubmitDesactivate}>
                                <ReactCodeInput 
                                    title="Código de verificación"
                                    placeholder={["-","-","-","-","-","-"]}
                                    id="desactivateVerificationCodeError" 
                                    onChange={(event)=>{
                                        setDesactivateVerificationCode(event)
                                    }} 
                                    autoFocus={true}
                                    fieldWidth={50}
                                    fieldHeight={50}
                                />
                                <input style={{marginTop: "20px"}} type="submit" className="buttn buttn-primary" value="Verificar"/>
                            </form>
                        </div>
                }
        </div>
    )
}