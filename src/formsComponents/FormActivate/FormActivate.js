import React, { useContext } from "react";
import DeviceDetector from "device-detector-js";
import { useToasts } from "react-toast-notifications";
import axios from "axios";
import Requests from "../../services/Requests";
import Constants from "../../services/Constants";
import { LayoutContext } from "../../layout/LayoutContext/LayoutContext";
import Loading from "../../layout/Loading/Loading";
import "./FormActivate.css";
import { Redirect } from "react-router-dom";

const deviceDetector = new DeviceDetector();
const device = deviceDetector.parse(window.navigator.userAgent);

export default function Activate() {
    const { setModalVisible, loadingVisible, setLoadingVisible } = useContext(LayoutContext);
    const { addToast } = useToasts();
    const user = JSON.parse(localStorage.getItem("user_bitwabi"));
    const [profile, setProfile] = React.useState({});
    const [infoClient, setInfo] = React.useState({});
    const [timeElap, setTimeElap] = React.useState({ minutes: 0, seconds: 0 });
    const [activ, setActiv] = React.useState(false);
    //downgrade vars
    const [down, setDown] = React.useState(false);
    const [selectedDown, setSelectedDown] = React.useState(false);
    //redirect varis
    const [redirect, setRedirect] = React.useState(false);
    const [redirectToActivate, setRedirectToActivate] = React.useState(false);

    const generateRemainAddress = (ev) => {
        setLoadingVisible(true);
        Requests.post("generateRemainAddress", { userID: user.id, infoClient, newSelectedPackage: selectedDown }).then((result) => {
            console.log("regreso de remain", result.data);
            if (result.data.success == 1) {
                getActivationData().then((activ) => {
                    setInterval(() => getElapsedTime(activ.generatedTime), 1000);
                });
            }
            setLoadingVisible(false);
        });
    };
    const getActivationData = () => {
        return new Promise((resolve, reject) => {
            setLoadingVisible(true);
            Requests.post("getActivationData", { userID: user.id }).then((result) => {
                console.log("regreso de activation", result.data);
                if (result.data.success === 1) {
                    let activ = result.data.activation;
                    setActiv(activ);
                    resolve(activ);
                } else {
                    reject();
                }
                setLoadingVisible(false);
            });
        });
    };
    const confirmPaymentAndActivate = () => {
        setLoadingVisible(true);
        Requests.post("confirmPaymentAndActivate").then((result) => {
            console.log("regreso de confirmar pago");
            if (result.data.success === 1) {
                addToast("Su cuenta ha sido activada correctamente", { appearance: "success", autoDismiss: true});
                getActivationData();
            } else {
                addToast(result.data.message, { appearance: "error", autoDismiss: true });
            }
            setLoadingVisible(false);
        });
    }; 
    const downAndActivate = () => {
        setLoadingVisible(true);
        Requests.post("downAndActivate", { newPackage: selectedDown, infoClient }).then((result) => {
            console.log("regreso de confirmar pago");
            if (result.data.success === 1) {
                addToast("Su cuenta ha sido activada correctamente", { appearance: "success", autoDismiss: true});
                getActivationData();
            } else {
                addToast(result.data.message, { appearance: "error", autoDismiss: true });
            }
            setLoadingVisible(false);
        });
    };
    const getclientdata = () => {
        setLoadingVisible(true);
        axios.get("http://api.ipstack.com/check?access_key=512192071c40a3165c5d296a6edf6ea3").then(
            (data) => {
                console.log("data", data, "el device", device);
                setInfo({
                    ip: data.data.ip,
                    country_name: data.data.country_name,
                    browser: device.client.name,
                    device: device.device.type,
                    os: device.os.name,
                });
                setLoadingVisible(false);
            },
            (err) => {
                console.log("hubo error");
                setLoadingVisible(false);
            }
        );
    };
    const getElapsedTime = (time) => {
        let now = new Date().getTime();
        let created = new Date(time).getTime();
        let elapsed = now - created;
        let div = elapsed / 60000;
        var _minutes = Math.floor(div);
        var _seconds = ((elapsed % 60000) / 1000).toFixed(0);
        //console.log("elapsed", elapsed, div)
        setTimeElap({ minutes: _minutes, seconds: _seconds });
    };
    React.useEffect(() => {
        setLoadingVisible(true);
        getActivationData().then((respActiv) => {
            console.log("de activ init", respActiv);
            if (!respActiv) {
                Requests.post("getProfile", { userID: user.id }).then((result) => {
                    console.log("regreso de profile", result.data);
                    if (!!result.data.failAuth) {
                        setRedirectToActivate(true);
                        addToast(result.data.message, { appearance: "error", autoDismiss: true });
                        localStorage.removeItem("user_bitwabi");
                        localStorage.removeItem("access-token");
                    }
                    if (result.data.success === 1) {
                        setProfile(result.data.user);
                    }
                });
            } else {
                setInterval(() => getElapsedTime(respActiv.generatedTime), 1000);
            }
            setLoadingVisible(false);
        });
        getclientdata();
    }, []);

    const getRemain = () => {
        let priceTaked = profile.halfDiscount ? Constants.packgs[selectedDown].half : Constants.packgs[selectedDown].price
        return priceTaked - profile.reservationPayment
    }
    const takedPrice = () =>{
        return profile.halfDiscount ? Constants.packgs[selectedDown].half : Constants.packgs[selectedDown].price
    }

    return (
        <div id="formActivate">
            {redirectToActivate && <Redirect to="/signin/activateAccount" />}
            {redirect && <Redirect to="/signin" />}
            <Loading visible={loadingVisible} />
            {profile.selectedPackage && !activ && !down && (
                <React.Fragment>
                    <p className="formUpgradeTitle" style={{marginBottom: 10}}>
                        <b>Downgrade</b> opcional
                    </p>
                    <div className="field">
                        <div className="name">Licencia Actual</div>
                        <div className="value">{Constants.packgs[profile.selectedPackage].name}</div>
                    </div>
                    <div className="field" style={{marginBottom: 10}}>
                        <div className="name">Saldo en cuenta</div>
                        <div className="value">€{profile.reservationPayment}</div>
                    </div>
                    {Object.entries(Constants.packgs).map((element) => {
                        const [pkgID, pkg] = element;
                        //console.log("pado por activate", pkgID, pkg)
                        return ( ( user && pkgID > user.selectedPackage ) || ( (profile.halfDiscount ? pkg.half : pkg.price) < profile.reservationPayment ) ? null : 
                            <div className={`package`} key={pkgID}>
                                <label className="labelPackage" htmlFor={pkgID}>
                                    <div className="packageImage">
                                        <img src={pkg.path} />
                                    </div>
                                    <div className="packageName">
                                        <span>Licencia</span>
                                        <span className="name">{pkg.name}</span>
                                    </div>
                                    <div className="packagePrice">
                                        <span>€ {profile.halfDiscount ? pkg.half : pkg.price}</span>
                                    </div>
                                </label>
                                <input type="radio" id={pkgID} onChange={ev=>setSelectedDown(pkgID)} checked={pkgID === selectedDown} />
                                <i id={pkgID} onClick={ev=>setSelectedDown(pkgID)}></i>
                            </div> 
                        );
                    })}
                </React.Fragment>
            )}
            {profile.selectedPackage && !activ && down && (
                <React.Fragment>
                    <p className="formUpgradeTitle">
                        <b>Activación</b> de cuenta
                    </p>
                    <div className="field">
                        <div className="name">Licencia reservada:</div>
                        <div className="value">{Constants.packgs[profile.selectedPackage].name}</div>
                    </div>
                    <div className="field">
                        <div className="name">Descuento:</div>
                        <div className="value">{profile.halfDiscount ? "SI" : "NO"}</div>
                    </div>
                    <div className="field">
                        <div className="name">Pago realizado por concepto de reserva:</div>
                        <div className="value">€{profile.reservationPayment}</div>
                    </div>
                    <div className="field">
                        <div className="name">¿Bajó el nivel de la licencia?</div>
                        <div className="value">{selectedDown < profile.selectedPackage ? "SI" : "NO"}</div>
                    </div>
                    <div className="field">
                        <div className="name">Licencia a obtener (costo):</div>
                        <div className="value">{Constants.packgs[selectedDown].name} (€{ takedPrice() })</div>
                    </div>
                    <div className="field">
                        <div className="name">Pago restante a realizar</div>
                        <div className="value">€{getRemain()}</div>
                    </div>
                </React.Fragment>
            )}
            {activ && activ.active && (
                <div className="layer">
                    <p>Hacer el envío de:</p>
                    <div className="bubble" style={{ backgroundColor: "rgb(99, 239, 169, .3)" }}>
                        {/* Aumentamos el fee */}
                        {(activ.quantity + 0.0004).toFixed(5)}BTC
                    </div>
                    
                    <p>A la siguiente dirección:</p>
                    <div className="bubble" style={{ backgroundColor: "rgb(41, 199, 208, .3)" }}>
                        {activ.address}
                    </div>
                    <p>Tiempo transcurrido:</p>
                    <div className="bubble" style={{ borderColor: "gray", borderWidth: 1, borderStyle: "solid" }}>
                        {timeElap.minutes + ":" + timeElap.seconds}
                    </div>
                    <div className="notes">
                        <p>Por favor da clic en el botón <strong>Confirmar transferencia y activar cuenta</strong> cuando
                        se haya confirmado tu transacción para confirmar los fondos del pago. </p>
                        <p>*Puedes cerrar esta ventana y volver cuando la transferencia haya sido confirmada (no perderás tus avances)</p>
                        <p><b>¡Importante!</b>, la activación no es automática. Debe dar click en el botón "<b>Confirmar transferencia y activar cuenta</b>"</p>
                    </div>
                    <button className="buttn buttn-primary" onClick={(ev) => confirmPaymentAndActivate()}>
                        Confirmar transferencia y activar cuenta
                    </button>
                </div>
            )}
            {activ && !activ.active && activ.payed && (
                <div>
                    <b>¡Muchas gracias por su pago!</b> <br />
                    <br />
                    Su cuenta ha sido activada totalmente y ha pasado de estar en modo de reserva a una licencia
                    completa.
                    <br />
                    <br />
                    Vuelva a iniciar sesión e ingrese a <strong>Configuración - Configuración API's</strong>: <br />
                    <br />
                    <button
                        onClick={(ev) => {
                            localStorage.removeItem("user_bitwabi");
                            localStorage.removeItem("access-token");
                            setRedirect(true);
                        }}
                        className="buttn buttn-primary"
                    >
                        Reiniciar sesión
                    </button>
                </div>
            )}
            {profile.selectedPackage && !activ && !down && (
                <React.Fragment>
                    <button className="buttn buttn-primary" onClick={ev=>{
                            if(selectedDown) { 
                                setDown(true) 
                            } else { 
                                    
                            }
                        }}>
                        Elegir 
                    </button>
                </React.Fragment>
            )}
            {profile.selectedPackage && !activ && down && (
                <React.Fragment>
                    <button className="buttn" onClick={ev=>setDown(false)}>
                        Regresar
                    </button>
                    {getRemain() > 0 ? <button className="buttn buttn-primary" onClick={generateRemainAddress}>
                        Generar dirección para realizar pago
                    </button> : <button className="buttn buttn-primary" onClick={downAndActivate}>
                        Activar cuenta    
                    </button>}
                </React.Fragment>
            )}
        </div>
    );
}
