import React, { useState, useContext, useEffect } from "react";
import Request from "../../services/Requests";
import { LayoutContext } from "../../layout/LayoutContext/LayoutContext";

export default function Newsletter() {
    const { setModalVisible } = React.useContext(LayoutContext);
    const user = JSON.parse(localStorage.getItem("user_bitwabi"))
    const [lnew, setNew] = useState(null)

    const OnSubmit = async (event) => {
        event.preventDefault();
    };
    const getNew = async () => {
        const response = await Request.get("latestNew");
        console.log("respuesta de latest new", response)
        if(response.data.success===1){
            setNew(response.data.lnew.new)
            setModalVisible("newsletter")
        } 
    }
    const setReaded = async (ev) => {
        const response = await Request.post("setReaded", {account: user.id});
        console.log("respuesta de setReaded", response)
        if(response.data.success === 1){
            let auxUser = JSON.parse(localStorage.getItem("user_bitwabi"))
            auxUser.hasReadedNews = true
            localStorage.setItem("user_bitwabi", JSON.stringify(auxUser))
            setModalVisible(false)
        }
    }

    useEffect(()=>{
        console.log("tengo el user en newsletter", user)
        if(!user.hasReadedNews){
            getNew()
        }
    }, [])

    return (
        <form id="newsletter" onSubmit={OnSubmit}>
            <h3>Aviso a la comunidad</h3>
            {lnew && <div style={{ textAlign: "justify", maxHeight: 400, overflowY: "scroll", marginBottom: 20}} dangerouslySetInnerHTML={{__html: lnew}} />}
            <button className="buttn buttn-primary" onClick={setReaded}>Leído</button>
        </form>
    );
}
