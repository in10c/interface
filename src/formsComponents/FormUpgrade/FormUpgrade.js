import React, { useState, useEffect, useContext } from "react";
import {Redirect } from "react-router-dom";
import { useToasts } from "react-toast-notifications";
import Constants from "../../services/Constants";
import Requests from "../../services/Requests";
import { LayoutContext } from "../../layout/LayoutContext/LayoutContext";
import Loading from "../../layout/Loading/Loading";
import "./FormUpgrade.css";

export default function Upgrade() {
    const [redirect, setRedirect] = useState(false)
    const { setModalVisible, loadingVisible, setLoadingVisible } = useContext(LayoutContext);
    const { addToast } = useToasts();
    const [timeElap, setTimeElap] = useState({ minutes: 0, seconds: 0 });
    const user = JSON.parse(localStorage.getItem("user_bitwabi"));
    const [selectedPackage, setSelectedPackage] = useState(undefined);
    const [halfDiscount, setHalfDiscount] = useState(undefined);
    const [paymentPayload, setPayload] = useState(false);

    useEffect(() => {
        if(!user){
            setRedirect(true)
        }
    }, [user])

    useEffect(() => {
        async function getInfo() {
            const response = await Requests.get("verifyHalfDiscount");
            if (!!response.data.failAuth) {
                addToast(response.data.message, { appearance: "error", autoDismiss: true });
                localStorage.removeItem("user_bitwabi");
                localStorage.removeItem("access-token");
            }
            setHalfDiscount(response.data.halfDiscount);
        }
        getInfo();
    }, []);

    const OnClick = (event) => {
        const { id } = event.target;
        setSelectedPackage(id);
    };

    const verifyAndUpgrade = async (event) => {
        setLoadingVisible(true);
        event.preventDefault();
        const response = await Requests.post("confirmPaymentAndUpgrade")
        console.log("traigo de verify", response)
        if (!!response.data.failAuth) {
            localStorage.removeItem("user_bitwabi");
            localStorage.removeItem("access-token");
        }
        addToast(response.data.message, { appearance: response.data.success === 1 ? "success" : "error", autoDismiss: true });
        if (response.data.success === 1) {
            const userUpdated = await Requests.get("user");
            localStorage.setItem("user_bitwabi", JSON.stringify(userUpdated.data.user));
            getUpgradeAddress()
        }
        setLoadingVisible(false);
    }
    const generateUpgradeAddress = async (ev) => {
        ev.preventDefault()
        if (!selectedPackage) {
            addToast("Por favor selecciona un paquete", { appearance: "error", autoDismiss: true });
            return;
        }
        setLoadingVisible(true);
        let response = await Requests.post("upgradeAddress", { newPackage: selectedPackage });
        setLoadingVisible(false);
        if (response.data.success === 1) {
            console.log("trago de upgrade address", response.data.payload)
            setPayload(response.data.payload)
            setInterval(() => getElapsedTime(response.data.payload.generatedTime), 1000);
        } else {
            addToast(response.data.message, { appearance: "error", autoDismiss: true });
        }
    }
    const getElapsedTime = (time) => {
        let now = new Date().getTime();
        let created = new Date(time).getTime();
        let elapsed = now - created;
        let div = elapsed / 60000;
        var _minutes = Math.floor(div);
        var _seconds = ((elapsed % 60000) / 1000).toFixed(0);
        //console.log("elapsed", elapsed, div)
        setTimeElap({ minutes: _minutes, seconds: _seconds });
    };
    const getUpgradeAddress = async () => {
        let response = await Requests.get("upgrade");
        if (response.data.success === 1 ) {
            console.log("trago de upgrade address", response.data)
            if(response.data.upgrade){
                setPayload(response.data.upgrade)
                setInterval(() => getElapsedTime(response.data.upgrade.generatedTime), 1000);
            } else {
                setPayload(false)
            }
            
        } else {
            console.log("errr upgrade ", response.data)
            setPayload(false)
            addToast(response.data.message, { appearance: "error", autoDismiss: true });
        }
    }

    const cancelUpgrade = async (upgradeID) => {
        let response = await Requests.post("cancelUpgrade", {upgradeID})
        console.log("el de cancel upgrd", response)
        getUpgradeAddress()
    }

    useEffect(()=>{
        getUpgradeAddress()
    }, [])

    return (
        <div id="formUpdate">
            {redirect && <Redirect to="/signin" />}
            <Loading visible={loadingVisible} />
            { !paymentPayload ? <React.Fragment><p className="formUpgradeTitle">
                <b>Upgradea</b> tu cuenta
            </p>
            <form onSubmit={generateUpgradeAddress}>
                {Object.entries(Constants.packgs).map((element) => {
                    const [pkgID, pkg] = element;
                    return (
                        <div className={`package ${user && pkgID <=  user.selectedPackage ? "disabled" : ""}`} key={pkgID}>
                            <label className="labelPackage" htmlFor={pkgID}>
                                <div className="packageImage">
                                    <img src={pkg.path} />
                                </div>
                                <div className="packageName">
                                    <span>Licencia</span>
                                    <span className="name">{pkg.name}</span>
                                </div>
                                <div className="packagePrice">
                                    <span>€ {halfDiscount ? pkg.price * 0.6 : pkg.price}</span>
                                </div>
                            </label>
                            <input type="radio" id={pkgID} onChange={OnClick} checked={pkgID === selectedPackage} />
                            <i id={pkgID} onClick={OnClick}></i>
                        </div>
                    );
                })}

                <input type="submit" className="buttn buttn-primary" value="Mejorar licencia" />
            </form></React.Fragment> : !paymentPayload.payed ? <React.Fragment>
                <p className="formUpgradeTitle">
                    <b>Paga</b>tu upgrade
                </p>
                <div className="layer">
                    <p>Cálculo (Nueva licencia - Pagado):</p>
                    <div className="bubble" style={{ backgroundColor: "rgb(99, 239, 169, .3)" }}>
                        €{paymentPayload.toPayForNewOne}{paymentPayload.halfDiscount && "(-40%)"} - €{paymentPayload.payedOld}{paymentPayload.halfDiscount && "(-50%)"} = €{paymentPayload.diffPayment}
                    </div>
                    <p>Hacer el envío de:</p>
                    <div className="bubble" style={{ backgroundColor: "rgb(99, 239, 169, .3)" }}>
                        {/**Se aumenta el fee */}
                        {paymentPayload && paymentPayload.quantity && (parseFloat(paymentPayload.quantity) + 0.0004).toFixed(5)}BTC
                    </div>
                    <p>A la siguiente dirección:</p>
                    <div className="bubble" style={{ backgroundColor: "rgb(41, 199, 208, .3)" }}>
                        {paymentPayload.address}
                    </div>
                    <p>Tiempo transcurrido:</p>
                    <div className="bubble" style={{ borderColor: "gray", borderWidth: 1, borderStyle: "solid" }}>
                        {timeElap.minutes + ":" + timeElap.seconds}
                    </div>
                    <p className="warning">
                        <button className="buttn buttn-primary" style={{width: "100%"}} onClick={verifyAndUpgrade}>
                            Confirmar transferencia y procesar upgrade
                        </button>
                        <button className="buttn" style={{width: "100%", marginTop: 10}} onClick={ev=>cancelUpgrade(paymentPayload._id)}>
                            Cancelar upgrade
                        </button>
                    </p>
                </div>
            </React.Fragment> : <React.Fragment>
                <p className="formUpgradeTitle">
                    <b>Cuenta</b>actualizada
                </p>
                <p style={{marginTop:50}}>
                    Muchas gracias, su cuenta ha sido actualizada correctamente.
                </p>
                <p className="warning">
                    <button className="buttn" style={{width: "100%", marginTop: 10}} onClick={ev=>cancelUpgrade(paymentPayload._id)}>
                        Nuevo upgrade
                    </button>
                </p>
            </React.Fragment>}
        </div>
    );
}
