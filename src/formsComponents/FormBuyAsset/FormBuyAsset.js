import React, { useState, useEffect, useContext } from "react";
import { LayoutContext } from "../../layout/LayoutContext/LayoutContext";
import {Redirect } from "react-router-dom";
import { useToasts } from "react-toast-notifications";
import Loading from "../../layout/Loading/Loading";
import Requests from "../../services/Requests";
import "./FormBuyAsset.css"

export default function FormBuyAsset() {
    const { setModalVisible } = useContext(LayoutContext);
    const [redirect, setRedirect] = useState(false)
    const [buyType, setBuyType] = useState("quantityBuy") // quantityBuy || percentageBuy
    const [asset, setAsset] = useState("")
    const [quant, setQuant] = useState("") 
    const [loadingVisible, setLoadingVisible] = useState(false) 
    const { addToast } = useToasts();

    const buyAsset = (ev) => {
        ev.preventDefault()
        setLoadingVisible(true);
        Requests.postTradingServer("buyAsset", { asset, quantity: quant, buyType }).then((result) => {
            console.log("regreso de buy asset", result.data);
            if (result.data.success == 1) {
                addToast("Se ha comprado el activo correctamente.", { appearance: "success", autoDismiss: true});
            } else {
                addToast(result.data.message, { appearance: "error", autoDismiss: true});
            }
            setLoadingVisible(false);
        });
    };

    return (
        <div style={{position: "relative"}}>
            {redirect && <Redirect to="/signin" />}
            <Loading visible={loadingVisible} />
            <p className="formUpgradeTitle">
                <b>Comprar un activo</b> permanentemente
            </p>
            <form  id="formBuyAssets" onSubmit={buyAsset}>
                <p>
                    <label>ACTIVO: </label>
                    <input type="text" value={asset} onChange={ev=>setAsset(ev.target.value)} placeholder="Ejemplo BNB" className="formControl" />
                </p>
                <p>
                    <label>TIPO DE COMPRA: </label>
                    <div style={{marginTop: 10}}>
                        <button 
                            type="button" 
                            className={"buttn buttn-"+(buyType === "quantityBuy" ? "primary" : "default")}
                            onClick={ev=>setBuyType("quantityBuy")}
                        >Cantidad</button>
                        <button 
                            type="button" 
                            className={"buttn buttn-"+(buyType === "percentageBuy" ? "primary" : "default")}
                            onClick={ev=>setBuyType("percentageBuy")}
                        >Porcentaje</button>
                    </div>
                </p>
                <p>
                    <label>CANTIDAD ({buyType === "quantityBuy" ? "#" : "%" }): </label>
                    <input type="number" value={quant} step={0.1} onChange={ev=>setQuant(ev.target.value)} placeholder="Ejemplo BNB" className="formControl" />
                </p>
                <input type="submit" className="buttn buttn-primary" value="Comprar activo" />
            </form>
        </div>
    );
}
