import React from "react"
import { Redirect, Route } from "react-router-dom";

const PrivateRoute = ({ path, component: Component, ...rest }) => {
    let resp = localStorage.getItem("user_bitwabi")
    console.log("la resp private", resp)
    let user = resp ? JSON.parse(resp) : false 
    console.log("la resp user", user)
    console.log("el path es", path)
    return <Route {...rest} render={(props) => (
          user ? <Component {...props} />
          : <Redirect to={`/signin${path || ""}`} />
    )} />
}

export default PrivateRoute