import React from "react";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";
import "./Loading.css";

export default function Loading(props) {
    return (
        <div id="loading" className={`${props.visible ? "visible" : ""}`}>
            <span>
                <Loader type="Oval" color="#00BFFF" height={100} width={100} visible={props.visible} />
                <br />
                Por favor espere...
            </span>
        </div>
    );
}
