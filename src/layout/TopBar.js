import React, {useState, useEffect} from 'react';
import { useToasts } from 'react-toast-notifications'
import Requests from "../services/Requests"
const pack = {1: "Bronce", 2: "Plata", 3: "Oro", 4: "Diamante", 5: "Premium", 6: "Premium Plus"}
export default function TopBar() {
    const user = JSON.parse(localStorage.getItem("user_bitwabi"))
    return(
        <div id="topBar">
            <div className="user">
                <span>Tipo de licencia: {pack[user.selectedPackage]} ({user.fullAccount ? "Completa" : "Reserva"})</span>
            </div>
            <div className="user">
                <span>Usuario: {user.name}</span>
            </div>
        </div>
    )

}
