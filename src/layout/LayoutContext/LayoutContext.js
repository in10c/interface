import React, { useState } from "react";

const LayoutContext = React.createContext();
const { Provider, Consumer } = LayoutContext;

const LayoutProvider = ({ children }) => {
    const [modalVisible, setModalVisible] = useState(null);
    const [loadingVisible, setLoadingVisible] = useState(false);

    return (
        <Provider value={{ modalVisible, setModalVisible, loadingVisible, setLoadingVisible }}>
            {children}
        </Provider>
    );
};

export { LayoutProvider, Consumer as LayoutConsumer, LayoutContext };