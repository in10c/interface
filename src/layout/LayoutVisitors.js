import React from "react"
import "./LayoutVisitors.css"

export default function LayoutVisitors({className, children}) {
    
    return <div id="LayoutVisitors" className={`${className ? className : ""}`}>
        <div className="content">
            <div className="logo">
                <img src="/images/logotipo.png"/>
            </div>
            <div className="wrappBox">
                <div className="box">{children}</div>
            </div>
        </div>
    </div>
}