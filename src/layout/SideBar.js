import React, { useState, useContext } from 'react';
import { NavLink, Redirect, useLocation } from "react-router-dom";
import * as GlobalApp from '../services/GlobalApp'
import NavSubList from './SideBarComponents/NavSubList';
import Constants from '../services/Constants';
import {LayoutContext} from '../layout/LayoutContext/LayoutContext';

export default function SideBar() {
    const {setModalVisible} = useContext(LayoutContext)
    const [redirect, setRedirect] = useState(false)
    const [menuVisible, setMenuVisible] = useState(false)
    const user = JSON.parse(localStorage.getItem("user_bitwabi"))
    let location = useLocation();
    //console.log("la loc", location)
    const logout = (ev)=>{
        ev.preventDefault()
        if(window.confirm("¿Está seguro que desea cerrar su sesión?")){
            localStorage.removeItem("user_bitwabi")
            localStorage.removeItem("access-token")
            setRedirect(true)
        }
        //idioma promocion soporte balance plan amigo transacciones
    }
    return(
            <div id="sideBar" className={menuVisible ? "visible" : ""}>
                {redirect && <Redirect to="/signin" />}
                <div id="buttonToggleMenu">
                    <i className={`fa fa-${menuVisible ? "times" : "bars"} fa-2x`} onClick={() =>{setMenuVisible(!menuVisible)}}></i>
                </div>
                <div className="logotype">
                    <img src="/images/logotipo.png"/>
                </div>
                {user && <div className="userInfo">
                    <div className="avatar">
                        <img src="/images/avatar.png"/>
                    </div>
                    <div className="info">
                        <div className="name">
                            {user.name.length > 15 ?  user.name.substring(0, 15)+"..." : user.name}
                        </div>
                        <div className="package">
                            <div className="image">
                                <img src={Constants.pckgInfo(user.selectedPackage).path}/>
                            </div>
                            <div className="name">
                                <span>{Constants.pckgInfo(user.selectedPackage).name}</span>
                                {user.reservation ? <span className="reservation">
                                    RESERVA
                                </span> :  <span className="reservation">
                                    FULL ACCOUNT
                                </span>}
                            </div>
                        </div>
                        {user.reservation && <div className={`fullAccount`}>
                            <span>No activa</span>
                        </div>}
                    </div>
                </div>}
                <div className="menu">
                    <ul>
                        <li className={location.pathname == "/" ? "active" : ""}>
                            <NavLink to="/" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-home" aria-hidden="true"></i><span>Inicio</span></NavLink>
                        </li>
                        { user && user.fullAccount ? <NavSubList text="Mi cuenta" logo="user">
                            <NavLink to="/ordersHistory" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Operaciones cerradas</span></NavLink>
                            <NavLink to="/logs" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Registro de acciones</span></NavLink>
                            <NavLink to="/myPayments" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Mis pagos</span></NavLink>
                            <NavLink to="/videogallery" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Galería de videos</span></NavLink>
                            <a href="" onClick={(event) => {event.preventDefault(); setModalVisible("upgrade")}}><i className="fa fa-circle" aria-hidden="true"></i><span>Mejora tu licencia</span></a>
                        </NavSubList> : <NavSubList text="Mi cuenta" logo="user">
                            {/*<NavLink to="/ordersHistory" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Operaciones de trading</span></NavLink>*/}
                            <NavLink to="/myPayments" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Mis pagos</span></NavLink>
                            <NavLink to="/activateAccount" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Activar licencia</span></NavLink>
                        </NavSubList> }
                        <NavSubList text="Referidos" logo="users">
                            <NavLink to="/referred" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Mis referidos</span></NavLink>
                            <NavLink to="/refererlink" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Enlace de referido</span></NavLink>
                        </NavSubList>
                        
                        {user && ( user.fullAccount || Constants.developerMode) ? <NavSubList text="Configuraciones" logo="cogs">
                            <NavLink to="/userProfile" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Mi perfil</span></NavLink>
                            <NavLink to="/changePassword" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Cambiar contraseña</span></NavLink>
                            <NavLink to="/changeEmail" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Cambiar correo</span></NavLink>
                            <NavLink to="/apis" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Configuración API´s</span></NavLink>
                            <a href="" onClick={(event) => {event.preventDefault(); setModalVisible("twoFactorAuthentication")}}><i className="fa fa-circle" aria-hidden="true"></i><span>Autenticación en dos pasos</span></a>
                            <NavLink to="/protectedBalances" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-gg-circle" aria-hidden="true"></i><span>Balances protegidos</span></NavLink>
                        </NavSubList> : <NavSubList text="Configuraciones" logo="cogs">
                            <NavLink to="/userProfile" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Mi perfil</span></NavLink>
                            <NavLink to="/changePassword" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Cambiar contraseña</span></NavLink>
                            <NavLink to="/changeEmail" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Cambiar correo</span></NavLink>
                            <a href="" onClick={(event) => {event.preventDefault(); setModalVisible("twoFactorAuthentication")}}><i className="fa fa-circle" aria-hidden="true"></i><span>Autenticación en dos pasos</span></a>
                        </NavSubList>}
                        {user && user.developer && <NavSubList text="Área privada" logo="user-secret">
                            <NavLink to="/monitor" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Monitor de bots</span></NavLink>
                            <NavLink to="/refcomm" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Pagar comisiones</span></NavLink>
                            <NavLink to="/upgradepayments" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Pagar upgrades</span></NavLink>
                            <NavLink to="/allreff" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Todos los referidos</span></NavLink>
                        </NavSubList>}
                        {user && user.trader && <NavSubList text="Trading" logo="line-chart">
                            <NavLink to="/tradelist"><i class="fa fa-circle" aria-hidden="true"></i><span>Lista trading</span></NavLink>
                            <NavLink to="/trade" onClick={() =>{setMenuVisible(!menuVisible)}}><i className="fa fa-circle" aria-hidden="true"></i><span>Enviar trade</span></NavLink>
                            <NavLink to="/bdebug"><i class="fa fa-circle" aria-hidden="true"></i><span>Debug</span></NavLink>
                        </NavSubList>}
                    </ul>
                </div>
                    <div className="logout">
                        <ul>
                            <li>
                                <a href="" onClick={logout}><i className="fa fa-sign-out" aria-hidden="true"></i><span>Cerrar sesión</span></a>
                            </li>
                        </ul>
                    </div>
            </div>
    )

}