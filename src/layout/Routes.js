//import pages
import Dashboard from "../pages/Dashboard/Dashboard"
import RefererLink from "../pages/RefererLink/RefererLink"
import ChildTree from "../pages/ChildTree/ChildTree"
import Trade from "../pages/Trade/Trade"
import Apis from "../pages/Apis/Apis"
import TradeList from "../pages/Trade/TradeList"
import Signal from "../pages/Trade/Signal"
import Activate from "../pages/Activate/Activate"
import ChangePassword from '../pages/UserProfile/ChangePassword'
import ChangeEmail from '../pages/UserProfile/ChangeEmail'
import UserProfile from '../pages/UserProfile/UserProfile'
import MyPayments from '../pages/MyPayments/MyPayments'
import RefComm from "../pages/RefComm/RefComm"
import OrdersHistory from "../pages/OrdersHistory/OrdersHistory"
import ProtectedBalances from "../pages/ProtectedBalances/ProtectedBalances"
import TwoFactorAuthentication from "../pages/TwoFactorAuthentication/TwoFactorAuthentication"
import Monitor from "../pages/Monitor/Monitor"
import UpgradePayments from "../pages/UpgradePayments/UpgradePayments"
import Account from "../pages/Account/Account"
import Debug from "../pages/Trade/Debug"
import Logs from "../pages/Logs/Logs"
import VideoGallery from "../pages/VideoGallery/VideoGallery"

const Routes = [
    {path: "/", component: Dashboard},
    {path: "/refererlink", component: RefererLink},
    {path: "/referred", component: ChildTree},
    {path: "/allreff", component: ChildTree},
    {path: "/userProfile", component: UserProfile},
    {path: "/trade", component: Trade},
    {path: "/apis", component: Apis},
    {path: "/tradelist", component: TradeList},
    {path: "/signal/:signalID", component: Signal},
    {path: "/activateAccount", component: Activate},
    {path: "/changePassword", component: ChangePassword},
    {path: "/changeEmail", component: ChangeEmail},
    {path: "/myPayments", component: MyPayments},
    {path: "/refcomm", component: RefComm},
    {path: "/ordersHistory", component: OrdersHistory},
    {path: "/protectedBalances", component: ProtectedBalances},
    {path: "/twoFactorAuthentication", component: TwoFactorAuthentication},
    {path: "/monitor", component: Monitor},
    {path: "/upgradepayments", component: UpgradePayments},
    {path: "/account/:userID", component: Account},
    {path: "/bdebug", component: Debug},
    {path: "/logs", component: Logs},
    {path: "/videogallery", component: VideoGallery}
]

export default Routes