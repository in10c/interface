import React, { useState, useEffect } from 'react'

export default function Banner(props) {
    const [hidden, setHidden] = useState(true)
    
    useEffect(() => {
        const bannerVisible = localStorage.getItem("banner_closed")
        setHidden(!!bannerVisible)
    }, [])

    const OnClick = () => {
        localStorage.setItem("banner_closed", "true")
        setHidden(true)
    }

    return (
        <div id="banner" className={hidden ? "bannerHidden" : ""}>
            <div className="imageBinance">
                <img src="/images/binance-logo.svg" alt="Binance"/>
            </div>
            <div className="bannerText">
                <a href="https://www.binance.com/en/register?ref=KMYWW4M7" target="_blank" rel="noopener noreferrer">Registrate en Binance</a>
                , da click&nbsp;
                <a href="https://www.binance.com/en/register?ref=KMYWW4M7" target="_blank" rel="noopener noreferrer">aqui.</a>
            </div>
            <div className="closeBannerIcon">
                <i className="fa fa-times" onClick={OnClick}></i>
            </div>
        </div>
    )
}
