import React, { useState } from 'react'
import { useLocation } from "react-router-dom";
import './NavSubList.css'

export default function NavSubList(props) {
    let location = useLocation()
    const [open, setOpen] = useState(false)
    return (
        <div className={`navSubList ${open ? "open" : ""}`}>
            
            <li onClick={() => {setOpen(!open)}}>
                <a>
                    <div className="icon">
                        <i className={`fa fa-${props.logo || "minus"}`} aria-hidden="true"></i>
                    </div>
                    <div className="pageName">
                        <span>{props.text}</span>
                    </div>
                    <div className="dropDownButton">
                        <i className="fa fa-angle-down fa-2x dropdown" aria-hidden="true"></i>
                    </div>
                </a>
            </li>
            <ul>
                {React.Children.map(props.children, child => {
                    return props.hidden ? null : <li className={`child ${location.pathname === child.props.to ? "active" : ""}`}>{child}</li>
                })}
            </ul>
        </div>
    )
}
