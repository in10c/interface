import React, {useEffect, useState} from 'react';
import {Switch, Route, Redirect } from "react-router-dom";
//import layout
import Banner from "./Banner"
import SideBar from "./SideBar"
import TopBar from "./TopBar"
import ChildrenCounter from '../pages/ChildrenCounter/ChildrenCounter';
import TwoFactorAuthAlert from '../pages/TwoFactorAuthAlert/TwoFactorAuthAlert';
//routes
import Routes from "./Routes"
//import style
import "./Dashboard.css"
import {LayoutProvider} from "./LayoutContext/LayoutContext"
import Modal from './Modal/Modal'
import FormUpgrade from '../formsComponents/FormUpgrade/FormUpgrade'
import FormActivate from '../formsComponents/FormActivate/FormActivate'
import FormTermsAndConditions from '../formsComponents/FormTermsAndConditions/FormTermsAndConditions'
import FormTwoFactorAuthentication from '../formsComponents/FormTwoFactAuthentication/FormTwoFactorAuthentication'
import FormBuyAsset from "../formsComponents/FormBuyAsset/FormBuyAsset"
import Newsletter from "../formsComponents/Newsletter/Newsletter"

export default function Layout() {

    const [redirect, setRedirect] = useState(false)
    const user = JSON.parse(localStorage.getItem("user_bitwabi"))
    console.log("El user en layout es:", user)

    useEffect(() => {
        if(!user){
            setRedirect(true)
        }
    }, [user])

    return(
        <LayoutProvider>
            {redirect && <Redirect to="/signin" />}
            <Modal>
                <FormUpgrade form="upgrade" />
            </Modal>
            <Modal>
                <FormActivate form="activate" />
            </Modal>
            <Modal>
                <FormBuyAsset form="formBuyAssets" />
            </Modal>
            <Modal>
                <FormTermsAndConditions form="termsAndConditions" />
            </Modal>
            <Modal>
                <Newsletter form="newsletter" />
            </Modal>
            <Modal>
                <FormTwoFactorAuthentication form="twoFactorAuthentication" />
            </Modal>
            <div id="mainGrid">
                <SideBar/>
                <div id="mainContent">
                    <div id="logotypeOnResponsive">
                        <img src="/images/logotipo.png"/>
                    </div>
                    <div id="contentWrapper">
                        <span id="childrenCounterAtTop">
                            <ChildrenCounter />
                            <TwoFactorAuthAlert />
                        </span>
                        <Switch>
                            {Routes.map(val=><Route exact path={val.path} component={val.component} />)}
                        </Switch>
                    </div>
                </div>
            </div>
        </LayoutProvider>
    )

}